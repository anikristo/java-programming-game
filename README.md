#ReadMe#
This is the read me file for the project Java Programming Game 
##Authors: *g1h* (Ani Kristo, Anisa Llaveshi, Ayse Berceste Dincer, Burcu Canakci)##
 
Software needed to compile and run the code: 

*Eclipse Luna version 4.4.0*
*Java SE Development Kit 1.8*

##How to setup the code for compilation and running:##

### First way: ###
1.	Extract the folder JavaProgrammingGame from the zip file uploaded on Moodle.
2.	Open Eclipse.
3.	Create a new Java Project.
4.	Go to the folder JavaProgrammingGame that you have extracted and copy the src      foder and the other files present there except bin and .settings folder and .classpath and .project files.
5.	Go to the folder where you opened the new Java Project, open it and paste the files you have copied.
6.	Refresh Eclipse by pressing F5 or by right-clicking and clicking Refresh.

### Second way: ###
1.	Extract the folder JavaProgrammingGame from the zip file uploaded on Moodle.
2.	Open Eclipse
3.	Import the project JavaProgrammingGame that you previously extracted.
4.	Refresh Eclipse by pressing F5 or by right-clicking and clicking Refresh.

## Description of the project: ##
Our project named Java Programming Game is an educational game aimed to enhance the programming abilities of individuals who have a certain programming background. By enabling students, assistants, instructors to develop the game and insert sets of puzzles, Java Programming Game serves the students an interactive, flourishing, entertaining opportunity to improve their programming skills. The game will consist of several storylines among which the user can choose to play. The storylines will contain thought-provoking puzzles that the user will have to solve by programming in Java. In these puzzles other features will be present during the game such as clues, hints and a journal. The game has a point-based system. The score of each player is calculated according to the difficulty level of the game, the time that s/he has spent on it and errors s/he may make. 
Furthermore this project is like a container for many storylines and Java programming puzzles. Most of our model classes are abstract and they have to be extended. By extending our classes, filling up the properties and related methods and reasonably changing the structure of the classes, one can make any sort of changes to the example we provided. Contributions to the project can be made in form of Storylines; therefore, typically, a person should extend the storyline, partner, puzzle and setup classes, and then a package containing this information should be imported to the project, and the related game subclass should be updated. 

## Project’s status: ##

### Completed:
* The general structure of the game is finished and the game is functioning properly. It has a set of storylines and each of the storylines consist of a set of puzzles. The partner, hints, clues and journal are functional and being updated and saved properly. The compilation of the code written by the user in the puzzles is working accurately. When compiled, it displays all the compilation errors. When submitted, it runs the code and correctly checks the output. It also handles runtime exceptions like infinite loops or arithmetic exceptions like division by zero.  The number of errors and submissions and the total score both of a puzzle and a storyline are calculated correctly. 
* All the views in the game are being updated and the information is being saved and thus, reloaded when a user opens the game again in the same computer.
* By extending the classes, both model and view classes and making necessary changes, one can create new storylines and puzzles. Thus, additional features can easily be added to the game.


### Not Completed:
* The website dedicated to keeping and displaying data gathered from the program is not completed.

* It is not currently possible for multiple users to play in the same computer because no Log in is provided yet.