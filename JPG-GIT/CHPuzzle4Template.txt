import java.io.*;
public class CHPuzzle4Input {
public static void main( String[] args) throws IOException {
String x = "GoodMorningGoodAfternoonGoodEveningGoodNight";
String y = "HaveYouHeardThatg1HisDoingTheJavaProgrammingProject";
String z = "YesIndeed";

File output = new File( "CHPuzzle4ResOutput.txt");
FileWriter writer = new FileWriter( output);
writer.write( "" + decrypt( x));
writer.write( "\n");
writer.write( "" + decrypt( y));
writer.write( "\n");
writer.write( "" + decrypt( z));
writer.write( "\n");
writer.close();
}