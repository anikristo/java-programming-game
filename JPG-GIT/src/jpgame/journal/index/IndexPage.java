package jpgame.journal.index;

import java.util.*;
import java.io.Serializable;

import jpgame.JPGComponent;
import jpgame.game.Game;
import jpgame.journal.Journal;
import jpgame.storyline.Storyline;

/**
 * IndexPage.java
 * 
 * IndexPage application at JavaProgrammingGame
 * 
 * The class IndexPage that creates a specific journal page IndexPage is used to
 * create the index of the journal page which leads users to specific parts of
 * the journal IndexPage is categorized as storyline-based journal pages,
 * Chronologically-ordered journal pages and User-portfolio journal pages
 * 
 * Author: Ayse Berceste Dincer version 1.00 2014/17/4
 **/

public class IndexPage implements Serializable, JPGComponent

{
	// Serial NO
	private static final long serialVersionUID = -8647616754424518523L;

	// PROPERTIES
	private ArrayList<Storyline> storylinesIndexPageContent;
	private ArrayList<String> chronologicalPuzzleListIndexPageContent;
	private ArrayList<String> portfolioIndexPageContent;
	transient private JPGIndexPageView indexPageView;

	// CONSTRUCTOR
	public IndexPage(Game game) {
		storylinesIndexPageContent = new ArrayList<Storyline>();
		chronologicalPuzzleListIndexPageContent = new ArrayList<String>();
		portfolioIndexPageContent = new ArrayList<String>();

		// Adds the name of each storyline to content of the IndexPage
		if (game.getStorylines() != null) {
			for (int i = 0; i < game.getStorylines().size(); i++) {
				storylinesIndexPageContent.add(game.getStorylines().get(i));
			}
		}

		// Adds the name of each Chronologically Ordered Puzzle to content of
		// the IndexPage
		if (Journal.getChronologicalList() != null)
			for (int i = 0; i < Journal.getChronologicalList().size(); i++) {
				String str = "<html><b>"
						+ Journal.getChronologicalList().get(i)
								.getStorylineName().toUpperCase() + "</b> - "
						+ Journal.getChronologicalList().get(i).getPuzzleName()
						+ "</html>";
				chronologicalPuzzleListIndexPageContent.add(str);
			}

		// Adds the name of each Portfolio Puzzle to content of the IndexPage
		for (int i = 0; i < Journal.getPortfolio().getSize(); i++) {
			String str = "<html><b>"
					+ Journal.getPortfolio().getPortfolio().get(i)
							.getStorylineName().toUpperCase() + "</b> - "
					+ Journal.getPortfolio().getPuzzleNames().get(i)
					+ "</html>";
			portfolioIndexPageContent.add(str);
		}

		indexPageView = new IndexPageView(this); // Creates index page view
	}

	// METHODS

	// Method for returning the content of the first IndexPage
	public ArrayList<Storyline> getStorylinesIndexPageContent() {
		return storylinesIndexPageContent;
	}

	// Method for returning the content of the second IndexPage
	public ArrayList<String> getChronologicalPuzzleListIndexPageContent() {
		return chronologicalPuzzleListIndexPageContent;
	}

	// Method for returning the content of the third IndexPage
	public ArrayList<String> getPortfolioIndexPageContent() {
		return portfolioIndexPageContent;
	}

	// Method for returning the index page view
	public JPGIndexPageView getIndexPageView() {
		indexPageView.updateView(this);
		return indexPageView;
	}

	// Method for modifying the index page view
	public void setIndexPageView(JPGIndexPageView indexPageView) {
		this.indexPageView = indexPageView;
		notifyViews();
	}

	// Method to notify views
	public void notifyViews() {
		if (indexPageView != null) {
			indexPageView.updateView(this);
		}
	}

} // End of IndexPage class
