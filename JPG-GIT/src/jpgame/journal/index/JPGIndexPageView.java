package jpgame.journal.index;

import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;

import jpgame.JPGView;

/**
*JPGIndexPageView.java
*
*JPGIndexPageView application at JavaProgrammingGame
*
* Interface for creating index page views
* Contains methods that are required to be implemented in index page view instances
*
*Author: Ayse Berceste Dincer
*version 1.00 2014/21/7 
**/

public interface JPGIndexPageView extends JPGView
{
	public JButton getBackButton();
	public JLabel getFirstTitleLabel(); //Pages sorted by storylines
	public JLabel getSecondTitleLabel(); //Pages sorted by chronological order
	public JLabel getThirdTitleLabel(); //Pages added to portfolio
	public ArrayList<JButton> getStorylineButtons();
	public ArrayList<JButton> getChronologicalPuzzlesButtons();
	public ArrayList<JButton> getPortfolioPuzzlesButtons();
	public void addActionListeners( ActionListener actionListener );

} //End of JPGIndexPageView	
