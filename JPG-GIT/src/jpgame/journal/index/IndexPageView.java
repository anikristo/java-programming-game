package jpgame.journal.index;

import java.util.*;
import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.*;

import jpgame.JPGColorLibrary;
import jpgame.JPGComponent;
import jpgame.journal.Journal;
import jpgame.storyline.Storyline;

/**
 * IndexPageView.java
 *
 * IndexPageView application at JavaProgrammingGame
 *
 * Creates the panel for displaying the index page of the journal IndexPage is
 * the first page of the journal Contains buttons which will direct the players
 * to specific journal pages Three tabs are presented for accessing to
 * storyline-based chronologically ordered and portfolio-based journal pages
 * 
 * Author: Ayse Berceste Dincer version 1.00 2014/17/4
 **/

public class IndexPageView extends JPanel implements JPGIndexPageView {

	// Serial NO
	private static final long serialVersionUID = -8152879317600838903L;

	// PROPERTIES
	private ArrayList<JButton> storylineButtons;
	private ArrayList<JButton> chronologicalPuzzlesButtons;
	private ArrayList<JButton> portfolioPuzzlesButtons;

	private JPanel firstButtonsPanel;
	private JPanel secondButtonsPanel;
	private JPanel thirdButtonsPanel;

	private JPanel firstPagePanel;
	private JPanel secondPagePanel;
	private JPanel thirdPagePanel;
	
	private JScrollPane firstScrollPane;
	private JScrollPane secondScrollPane;
	private JScrollPane thirdScrollPane;
	
	private JButton backButton;
	private JPanel backButtonPanel;

	private JLabel firstTitleLabel;
	private JLabel secondTitleLabel;
	private JLabel thirdTitleLabel;

	private JTabbedPane tabbedPane;

	// CONSTRUCTOR
	public IndexPageView(IndexPage indexPage) {
		super(new BorderLayout());

		// Back button panel
		backButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		backButton = new JButton("Back");
		backButton.setForeground(JPGColorLibrary.PURE_WHITE);
		backButton.setFont(new Font("Segoe UI", Font.BOLD, 20));
		backButton.setBackground(JPGColorLibrary.JPG_CRIMSON);
		backButtonPanel.add(backButton);
		add(backButtonPanel, "South");

		tabbedPane = new JTabbedPane();
		// A new JPanel is created containing the information of the first tab
		// of the Index Page
		firstPagePanel = new JPanel(new BorderLayout());
		// Creates and adds the title Label
		firstTitleLabel = new JLabel("Index Page", JLabel.CENTER);
		firstTitleLabel.setFont(new Font("Segoe UI", Font.BOLD | Font.ITALIC,
				30));
		firstTitleLabel.setForeground(JPGColorLibrary.JPG_CRIMSON);
		firstPagePanel.add(firstTitleLabel, "North");
		// Creates the buttons panel and add a button for each storyline
		storylineButtons = new ArrayList<JButton>();
		firstButtonsPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 500,
				20));
		// Creates and formats buttons for each of the storylines
		for (int i = 0; i < indexPage.getStorylinesIndexPageContent().size(); i++) {
			JButton button = new JButton(indexPage
					.getStorylinesIndexPageContent().get(i).getName().toUpperCase());
			button.setBackground(JPGColorLibrary.BUTTON_BLUE);
			button.setPreferredSize(new Dimension(500, 50));
			button.setForeground(JPGColorLibrary.PURE_WHITE);
			button.setFont(new Font("Segoe UI", Font.BOLD, 24));
			button.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5,
					JPGColorLibrary.PURE_WHITE));
			if (indexPage.getStorylinesIndexPageContent().get(i).getStatus() == Storyline.NOT_STARTED)
				button.setEnabled(false);

			storylineButtons.add(button);
			firstButtonsPanel.add(button);
		}
		// Adds the buttons of the storylines in the firstPagePanel
		firstPagePanel.add(firstButtonsPanel);
		// Creates the back button which will direct users to homePage
		firstPagePanel.setPreferredSize(new Dimension(Toolkit
				.getDefaultToolkit().getScreenSize()));
		firstPagePanel.setVisible(true);
		// FirstPagePanel is added to the tabbedPane
		firstScrollPane = new JScrollPane(firstPagePanel);
		tabbedPane.addTab("General Index Page", firstScrollPane);
		// A new JPanel is created containing the information of the second tab
		// of the Index Page
		secondPagePanel = new JPanel(new BorderLayout());
		// Creates and adds the title Label
		secondTitleLabel = new JLabel("Puzzles played in chronological order",
				JLabel.CENTER);
		secondTitleLabel.setFont(new Font("Segoe UI", Font.BOLD | Font.ITALIC,
				30));
		secondTitleLabel.setForeground(JPGColorLibrary.JPG_CRIMSON);
		secondPagePanel.add(secondTitleLabel, "North");
		// Creates the buttons panel and add a button for each puzzle
		chronologicalPuzzlesButtons = new ArrayList<JButton>();
		secondButtonsPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 500,
				20));
		// Creates and formats buttons for each of the Chronologically Ordered
		// Puzzles
		for (int i = 0; i < indexPage
				.getChronologicalPuzzleListIndexPageContent().size(); i++) {
			JButton button = new JButton(indexPage
					.getChronologicalPuzzleListIndexPageContent().get(i));
			button.setBackground(JPGColorLibrary.BUTTON_BLUE);
			button.setPreferredSize(new Dimension(500, 50));
			button.setForeground(JPGColorLibrary.PURE_WHITE);
			button.setFont(new Font("Segoe UI", Font.BOLD, 24));
			button.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5,
					JPGColorLibrary.PURE_WHITE));

			chronologicalPuzzlesButtons.add(button);
			secondButtonsPanel.add(button);
		}
		// Adds the buttons of the puzzles in the secondPagePanel
		secondPagePanel.add(secondButtonsPanel);
		// Creates the back button which will direct users to homePage
		secondPagePanel.setPreferredSize(new Dimension(Toolkit
				.getDefaultToolkit().getScreenSize()));
		secondPagePanel.setVisible(true);
		// SecondPagePanel is added to the tabbedPane
		secondScrollPane = new JScrollPane( secondPagePanel );
		tabbedPane.addTab("Chronological ordered puzzles", secondScrollPane );
		// A new JPanel is created containing the information of the third tab
		// of the Index Page
		thirdPagePanel = new JPanel(new BorderLayout());
		// Creates and adds the title Label
		thirdTitleLabel = new JLabel("Favourite puzzles", JLabel.CENTER);
		thirdTitleLabel.setFont(new Font("Segoe UI", Font.BOLD | Font.ITALIC,
				30));
		thirdTitleLabel.setForeground(JPGColorLibrary.JPG_CRIMSON);
		thirdPagePanel.add(thirdTitleLabel, "North");
		// Creates the buttons panel and add a button for each puzzle
		portfolioPuzzlesButtons = new ArrayList<JButton>();
		thirdButtonsPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 500,
				20));
		// Creates and formats buttons for each of the Portfolio puzzles
		for (int i = 0; i < indexPage.getPortfolioIndexPageContent().size(); i++) {
			JButton button = new JButton(indexPage
					.getPortfolioIndexPageContent().get(i));
			button.setBackground(JPGColorLibrary.BUTTON_BLUE);
			button.setPreferredSize(new Dimension(500, 50));
			button.setForeground(JPGColorLibrary.PURE_WHITE);
			button.setFont(new Font("Segoe UI", Font.BOLD, 24));
			button.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5,
					JPGColorLibrary.PURE_WHITE));

			portfolioPuzzlesButtons.add(button);
			thirdButtonsPanel.add(button);
		}
		// Adds the buttons of the Portfolio puzzles in the thirdPagePanel
		thirdPagePanel.add(thirdButtonsPanel);
		// Creates the back button which will direct users to homePage
		thirdPagePanel.setPreferredSize(new Dimension(Toolkit
				.getDefaultToolkit().getScreenSize()));
		thirdPagePanel.setVisible(true);
		// ThirdPagePanel is added to the tabbedPane
		thirdScrollPane = new JScrollPane( thirdPagePanel );
		tabbedPane.addTab("Portfolio", thirdScrollPane);
		// the tabbedPane is added to the primary JPanel
		add(tabbedPane, "North");

		setPreferredSize(new Dimension(Toolkit.getDefaultToolkit()
				.getScreenSize()));
		setVisible(true);
	}

	// METHODS

	// Method for returning the back button
	public JButton getBackButton() {
		return backButton;
	}

	// Method for returning the first title label
	public JLabel getFirstTitleLabel() {
		return firstTitleLabel;
	}

	// Method for returning the second title label
	public JLabel getSecondTitleLabel() {
		return secondTitleLabel;
	}

	// Method for returning the third title label
	public JLabel getThirdTitleLabel() {
		return thirdTitleLabel;
	}

	// Method for returning the storyline buttons
	public ArrayList<JButton> getStorylineButtons() {
		return storylineButtons;
	}

	// Method for returning the Chronologically Ordered Puzzles buttons
	public ArrayList<JButton> getChronologicalPuzzlesButtons() {
		return chronologicalPuzzlesButtons;
	}

	// Method for returning the Portfolio Puzzles buttons
	public ArrayList<JButton> getPortfolioPuzzlesButtons() {
		return portfolioPuzzlesButtons;
	}

	// Method for returning the Portfolio buttons
	public void setPortfolioPuzzlesButtons(
			ArrayList<JButton> portfolioPuzzlesButtons) {
		this.portfolioPuzzlesButtons = portfolioPuzzlesButtons;
	}

	// Overrides the paintComponent method
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
	}

	// Overrides the update method to update the view
	public void updateView(JPGComponent indexPage) {

		for (int i = 0; i < ((IndexPage) indexPage)
				.getStorylinesIndexPageContent().size(); i++) {

			if (((IndexPage) indexPage).getStorylinesIndexPageContent().get(i)
					.getStatus() != Storyline.NOT_STARTED)
				storylineButtons.get(i).setEnabled(true);
		}

		secondPagePanel.removeAll();
		// Creates and adds the title Label
		secondTitleLabel = new JLabel("Puzzles played in chronological order",
				JLabel.CENTER);
		secondTitleLabel.setFont(new Font("Segoe UI", Font.BOLD | Font.ITALIC,
				30));
		secondTitleLabel.setForeground(JPGColorLibrary.JPG_CRIMSON);
		secondPagePanel.add(secondTitleLabel, "North");
		// Creates the buttons panel and add a button for each puzzle
		chronologicalPuzzlesButtons = new ArrayList<JButton>();
		secondButtonsPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 500,
				20));
		// Creates and formats buttons for each of the Chronologically Ordered
		// Puzzles
		if (Journal.getChronologicalList() != null)
			for (int i = 0; i < Journal.getChronologicalList().size(); i++) {
				JButton button = new JButton("<html><b>"
						+ Journal.getChronologicalList().get(i)
								.getStorylineName().toUpperCase() + "</b> - "
						+ Journal.getChronologicalList().get(i).getPuzzleName()
						+ "</html>");
				button.setBackground(JPGColorLibrary.BUTTON_BLUE);
				button.setPreferredSize(new Dimension(500, 50));
				button.setForeground(JPGColorLibrary.PURE_WHITE);
				button.setFont(new Font("Segoe UI", Font.PLAIN, 24));
				button.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5,
						JPGColorLibrary.PURE_WHITE));

				chronologicalPuzzlesButtons.add(button);
				secondButtonsPanel.add(button);
			}
		// Adds the buttons of the puzzles in the secondPagePanel
		secondPagePanel.add(secondButtonsPanel);
		// Creates the back button which will direct users to homePage
		secondPagePanel.setPreferredSize(new Dimension(Toolkit
				.getDefaultToolkit().getScreenSize()));
		secondPagePanel.setVisible(true);
		// SecondPagePanel is added to the tabbedPane
		tabbedPane.addTab("Chronological ordered puzzles", secondScrollPane );
		// A new JPanel is created containing the information of the third tab
		// of the Index Page
		thirdPagePanel.removeAll();
		;
		// Creates and adds the title Label
		thirdTitleLabel = new JLabel("Favourite puzzles", JLabel.CENTER);
		thirdTitleLabel.setFont(new Font("Segoe UI", Font.BOLD | Font.ITALIC,
				30));
		thirdTitleLabel.setForeground(JPGColorLibrary.JPG_CRIMSON);
		thirdPagePanel.add(thirdTitleLabel, "North");
		// Creates the buttons panel and add a button for each puzzle
		portfolioPuzzlesButtons = new ArrayList<JButton>();
		thirdButtonsPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 500,
				20));
		// Creates and formats buttons for each of the Portfolio puzzles
		if (Journal.getPortfolio() != null)
			if (Journal.getPortfolio().getPortfolio() != null)
				for (int i = 0; i < Journal.getPortfolio().getPortfolio()
						.size(); i++) {
					JButton button = new JButton("<html><b>"
							+ Journal.getPortfolio().getPortfolio().get(i)
									.getStorylineName().toUpperCase()
							+ "</b> - "
							+ Journal.getPortfolio().getPuzzleNames().get(i)
							+ "</html>");
					button.setBackground(JPGColorLibrary.BUTTON_BLUE);
					button.setPreferredSize(new Dimension(500, 50));
					button.setForeground(JPGColorLibrary.PURE_WHITE);
					button.setFont(new Font("Segoe UI", Font.PLAIN, 24));
					button.setBorder(BorderFactory.createMatteBorder(5, 5, 5,
							5, JPGColorLibrary.PURE_WHITE));

					portfolioPuzzlesButtons.add(button);
					thirdButtonsPanel.add(button);
				}
		// Adds the buttons of the Portfolio puzzles in the thirdPagePanel
		thirdPagePanel.add(thirdButtonsPanel);
		// Creates the back button which will direct users to homePage
		thirdPagePanel.setPreferredSize(new Dimension(Toolkit
				.getDefaultToolkit().getScreenSize()));
		thirdPagePanel.setVisible(true);
		// ThirdPagePanel is added to the tabbedPane
		tabbedPane.addTab("Portfolio", thirdScrollPane);

		repaint();
	}

	// JPGIndexPageView method
	public void addActionListeners(ActionListener actionListener) {
		if (chronologicalPuzzlesButtons.size() > 0) {
			if (chronologicalPuzzlesButtons.get(0).getActionListeners().length == 0) {
				for (int i = 0; i < chronologicalPuzzlesButtons.size(); i++) {
					chronologicalPuzzlesButtons.get(i).addActionListener(
							actionListener);
				}
				for (int i = 0; i < portfolioPuzzlesButtons.size(); i++) {
					portfolioPuzzlesButtons.get(i).addActionListener(
							actionListener);
				}
			}
		}

		if (backButton.getActionListeners().length == 0) {

			backButton.addActionListener(actionListener);
			for (int i = 0; i < storylineButtons.size(); i++) {
				storylineButtons.get(i).addActionListener(actionListener);
			}
		}

	}

} // End of IndexPageView
