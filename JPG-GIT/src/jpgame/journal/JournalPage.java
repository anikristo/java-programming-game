package jpgame.journal;

import java.io.Serializable;

import jpgame.JPGComponent;
import jpgame.puzzle.Puzzle;
/**
*  JournalPage.java
* 
*  JournalPage application at JavaProgrammingGame
* 
*  The class JournalPage that creates journal pages 
*  Journal page is created automatically for every puzzle of the game
*  It displays the puzzle scene, clues, user code and any features related to puzzle
*  
*  Author: Ayse Berceste Dincer
*  version 1.00 2014/27/4 
**/

public class JournalPage implements JPGComponent, Serializable {
	
	private static final long serialVersionUID = 4544751807500551630L;

	// PROPERTIES
	private Puzzle puzzle;	
	transient private JPGJournalPageView journalPageView;
	
	// Constructor
	// Takes the puzzle reference and initializes the properties accordingly
	public JournalPage(Puzzle puzzle) 
	{
		if (puzzle != null) {
			this.puzzle = puzzle;			
			journalPageView = new JournalPageView( this);
		}
		else 
			throw new NullPointerException( "Puzzle object is null.");
	}

	// Method for returning the puzzle
	public Puzzle getPuzzle() {
		return puzzle;
	}

	// Method for setting the puzzle
	public void setPuzzle(Puzzle puzzle) {
		this.puzzle = puzzle;
		notifyViews(); 
	}
	
	//Method for returning the view
	public  JPGJournalPageView getJournalPageView()
	{
		notifyViews();
		return journalPageView;
	}	
		
	//Method for setting the views
	public void setJournalPageView( JPGJournalPageView view )
	{
		this.journalPageView = view;
		notifyViews(); 
	}
	
			
	//Method for updating the views
	protected void notifyViews()
	{
		if (journalPageView != null) {
			journalPageView.updateView(this);
		}
	}

	public String getAdditionalNotes() {
		return puzzle.getAdditionalNotes();
	}

	public void setAdditionalNotes(String additionalNotes) {
		puzzle.setAdditionalNotes( additionalNotes);
		notifyViews();
	}
	
} // End of JournalPage
