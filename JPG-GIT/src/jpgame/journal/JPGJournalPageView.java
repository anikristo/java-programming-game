package jpgame.journal;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import jpgame.JPGView;

/**
*JPGJournalPageView.java
*
*JPGJournalPageView application at JavaProgrammingGame
*
* Interface for creating journal page views
* Contains methods that are required to be implemented in journal page view instances
*
*Author: Ayse Berceste Dincer
*version 1.00 2014/22/7 
**/

public interface JPGJournalPageView extends JPGView
{
	public JLabel getTitleLabel();
	public JPanel getInfoPanel();
	public JPanel getCodePanel();
	public JLabel getTimeLabel();
	public JLabel getRunLabel();
	public JLabel getRunNoLabel();
	public JLabel getCompileLabel();
	public JLabel getCompileNoLabel();
	public JLabel getScoreLabel();
	public JPanel getAdditionalNotesPanel();
	public JButton getNextButton();
	public JButton getPreviousButton();
	public JButton getEditButton();
	public JButton getExitButton();
	public JButton getPortfolioButton();
	public void addActionListeners( ActionListener actionListener );

} //End of JPGJournalPageView




