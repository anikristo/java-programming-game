package jpgame.journal;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.*;

import jpgame.game.Game;
import jpgame.journal.index.IndexPage;
import jpgame.puzzle.Puzzle;
import jpgame.storyline.Storyline;
/**
* Journal.java
*
* Journal application at JavaProgrammingGame
*
* The class Journal that creates the journal consisting of journal pages
* Journal is a collection of puzzle records which also allows personalization with the additional pages
* 
* Author: Ayse Berceste Din�er, Ani Kristo
* version 1.00 2014/21/4 
**/

public class Journal implements Serializable {

	//Serial NO
	private static final long serialVersionUID = -7535952894710543042L;

	// PROPERTIES
	private static Game game;
	private static Vector<Puzzle> chronologicallyOrderedPuzzles;
	private static Portfolio portfolio;
	private static Vector<Puzzle> allPuzzles;
	private static Vector<Storyline> allStorylines;
	private static Vector<String> allPuzzleNotes;
	private static IndexPage indexPage;
	private static JournalPage journalPage; 
	
	
	// METHODS
	
	//Method for setting game
	public static void setGameReference(Game g) {
		game = g;
		setup();
	}

	//Method for returning the game
	public static Game getGameReference() {
		return game;
	}
	
	//Method for setting the properties
	private static void setup() {
		// fill the lists with puzzles and storylines
		if (game != null) {
			allStorylines = new Vector<Storyline>();
			allPuzzles = new Vector<Puzzle>();

			allStorylines.addAll(game.getStorylines());
			for (Storyline s : allStorylines) {
				allPuzzles.addAll(s.getPuzzles());
			}

			portfolio = new Portfolio();

			allPuzzleNotes = new Vector<String>();
			for (int i = 0; i < allPuzzles.size(); i++) {
				allPuzzleNotes.add("");
			}
		}
	}

	// Method for returning the Index page of the journal
	public static IndexPage getIndexPage(Game game) {
		if (game == null) {
			setGameReference(game);
			setup();
		}
		
		if (indexPage == null)
		{
			indexPage = new IndexPage (game);
		}
		return indexPage;
	}
	
	// Method for returning the Index page of the journal
	public static IndexPage getIndexPage()
	{
		return indexPage;
	}

	//Method for returning the JournalPage
	public static JournalPage getJournalPage(Puzzle puzzle) { // TODO
		if( journalPage == null){
			journalPage = new JournalPage( puzzle);
			return journalPage;
		} else {
			journalPage.setPuzzle( puzzle);
			return journalPage;
		}
	}

	//Method for returning the chronological list
	public static Vector<Puzzle> getChronologicalList() {
		return chronologicallyOrderedPuzzles;
	}

	//Method for returning Portfolio
	public static Portfolio getPortfolio() {
		return portfolio;
	}

	//Method for adding to Portfolio
	public static boolean addToPortfolio(Puzzle puzzle) {
		return Portfolio.addToPortfolio(puzzle);
	}

	//Method for removing from Portfolio
	public static boolean removeFromPortfolio(Puzzle puzzle) {
		return Portfolio.removeFromPortfolio(puzzle);
	}

	//Method for adding to chronological list
	public static boolean addToChronologicalList(Puzzle puzzle) {

		// Checks if a puzzle is already in the chronologicallyOrderedPuzzles
		// list
		if (chronologicallyOrderedPuzzles == null)
			chronologicallyOrderedPuzzles = new Vector<Puzzle>();

		for (int i = 0; i < chronologicallyOrderedPuzzles.size(); i++) {
			if (puzzle.getPuzzleQuestion().equals(
					chronologicallyOrderedPuzzles.get(i).getPuzzleQuestion()))
			// reference checking is not correct, since the reference changes. A
			// unique attribute should distinguish among them.
			{
				return false;
			}
		}
		
		indexPage.notifyViews();
		return chronologicallyOrderedPuzzles.add(puzzle);
	}

	//Method for returning additional notes
	public static String getAdditionalNotes(Puzzle puzzle) {		
		return getJournalPage(puzzle).getAdditionalNotes();
	}

	//Method for setting additional notes
	public static void setAdditionalNotes(Puzzle puzzle, String text) {
		getJournalPage( puzzle).setAdditionalNotes( text);

	}

	//Method for returning the next puzzle in chronological order
	public static Puzzle getFollowingPuzzleInChronology(Puzzle p) {
		for (int i = 0; i < chronologicallyOrderedPuzzles.size(); i++) {
			if (p == chronologicallyOrderedPuzzles.get(i)) {
				if (i == chronologicallyOrderedPuzzles.size() - 1)
					return null;
				else
					return chronologicallyOrderedPuzzles.get(i + 1);
			}
		}
		return null;
	}

	//Method for returning the previous puzzle in chronological order
	public static Puzzle getPreviousPuzzleInChronology(Puzzle p) {
		for (int i = 0; i < chronologicallyOrderedPuzzles.size(); i++) {
			if (p == chronologicallyOrderedPuzzles.get(i)) {
				if (i == 0)
					return null;
				else
					return chronologicallyOrderedPuzzles.get(i - 1);
			}
		}
		return null;
	}

	// Method to save information of the game
	public static void saveContents() {
		try {
			FileOutputStream ostream = new FileOutputStream("ChroList.data");
			ObjectOutputStream p = new ObjectOutputStream(ostream);
			p.writeObject(chronologicallyOrderedPuzzles);
			p.flush();
			p.close();
			ostream.close();
			System.out.println("CList saved");
		} catch (IOException e) {
			System.out.println("Cannot save CList!");
			e.printStackTrace();
		}
		portfolio.saveContents();
	}

	// Method to load the information saved in the game
	@SuppressWarnings("unchecked")
	public static void loadContents() {
		File clistdataFile = new File("ChroList.data");
		File plistdataFile = new File("PortList.data");

		if (!clistdataFile.exists() || !plistdataFile.exists())
			saveContents();

		try {
			FileInputStream istream = new FileInputStream("ChroList.data");
			ObjectInputStream p = new ObjectInputStream(istream);

			try {
				chronologicallyOrderedPuzzles = (Vector<Puzzle>) p.readObject();
			} catch (Exception e) {
				System.out.println("Cannot load Journal");
				e.printStackTrace();
			}
			p.close();
			istream.close();
		} catch (IOException e) {
			System.out.println("Cannot load Journal");
			e.printStackTrace();
		}

		portfolio.loadContents();
	}

	//*************************************************************************************
	//Inner class Portfolio
	public static class Portfolio implements Serializable {

		//Serial NO
		private static final long serialVersionUID = -8910001757268986218L;
		// PROPERTIES
		private static Vector<Puzzle> portfolio;

		// METHODS
		//Method for returning the vector of portfolio puzzles
		public Vector<Puzzle> getPortfolio() {
			return portfolio;
		}

		//Method for saving 
		private void saveContents() {
			try {
				FileOutputStream ostream = new FileOutputStream("PortList.data");
				ObjectOutputStream p = new ObjectOutputStream(ostream);
				p.writeObject(portfolio);
				p.flush();
				ostream.close();
				System.out.println("Plist saved");
			} catch (IOException e) {
				System.out.println("Cannot save Journal contents!");
				e.printStackTrace();
			}

		}

		//Method for loading contents
		@SuppressWarnings("unchecked")
		private void loadContents() {
			try {
				FileInputStream istream = new FileInputStream("PortList.data");
				ObjectInputStream p = new ObjectInputStream(istream);

				try {
					portfolio = (Vector<Puzzle>) p.readObject();
				} catch (Exception e) {
					System.out.println("Cannot load Journal portfolio contents!");
					e.printStackTrace();
				}
				istream.close();
			} catch (IOException e) {
				System.out.println("Cannot load Journal portfolio contents!");
				e.printStackTrace();
			}
		}

		//Method for adding to Portfolio
		private static boolean addToPortfolio(Puzzle p) {
			if (portfolio == null)
				portfolio = new Vector<Puzzle>();

			for (int i = 0; i < portfolio.size(); i++) {
				if (p == portfolio.get(i)) {
					return false;
				}
			}

			indexPage.notifyViews();
			return portfolio.add(p);
		}

		//Method for removing from Portfolio
		private static boolean removeFromPortfolio(Puzzle p) {
			if (portfolio != null)
				return portfolio.remove(p);
			
			indexPage.notifyViews();
			return false;
		}
		
		//Method for returning the size of Portfolio
		public int getSize() {
			if (portfolio == null)
				return 0;
			return portfolio.size();
		}

		//Method for returning the puzzle names
		public ArrayList<String> getPuzzleNames() {
			if (portfolio != null) {
				ArrayList<String> puzzleNames = new ArrayList<String>();
				for (Puzzle p : portfolio) {
					puzzleNames.add(p.getPuzzleName());
				}
				return puzzleNames;
			}
			return null;
		}

		//Method for returning next puzzle from Portfolio
		public Puzzle getFollowingPuzzleInPortfolio(Puzzle p) {
			for (int i = 0; i < portfolio.size(); i++) {
				if (p == portfolio.get(i)) {
					if (i == portfolio.size() - 1)
						return null;
					else
						return portfolio.get(i + 1);
				}
			}
			return null;
		}

		//Method for returning previous puzzle from Portfolio
		public Puzzle getPreviousPuzzleInPortfolio(Puzzle p) {
			for (int i = 0; i < portfolio.size(); i++) {
				if (p == portfolio.get(i)) {
					if (i == 0)
						return null;
					else
						return portfolio.get(i - 1);
				}
			}
			return null;
		}
	}//End of Portfolio Inner class
//****************************************************************************************************	
	
} // End of Journal class
