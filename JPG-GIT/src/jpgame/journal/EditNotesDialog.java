/**
 * @author Ani Kristo
 * @version 1.0
 * 
 * 12 / 05 / 2014
 * 
 * DESCRIPTION: This dialog will contain the text that is already written in the Additional Page of the Journal, and it will allow the 
 * 				user to add, change or remove some notes. At this stage there is no option for formatting the text in terms of the
 * 				font style, color, size etc. 
 */
package jpgame.journal;

import java.awt.BorderLayout;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import jpgame.JPGColorLibrary;
import jpgame.JPGComponent;
import jpgame.puzzle.Puzzle;

public class EditNotesDialog extends JDialog implements ActionListener,
		JPGComponent {

	//Serial NO
	private static final long serialVersionUID = -104202940221275425L;
	
	//PROPERTIES
	private final JPanel contentPanel = new JPanel();
	private JButton okButton;
	private JButton cancelButton;
	private JPanel buttonPane;
	private JEditorPane textField;
	private JLabel title;
	private Puzzle puzzle;

	// CONSTRUCTOR
	// ------------------------------------------------------------------------------------------------------------------------
	public EditNotesDialog( Puzzle puzzle) {

		//this.gameFrame = originatingGameFrame;
		this.puzzle = puzzle;
		okButton = new JButton("OK");
		okButton.setFont(new Font("Segoe UI", Font.BOLD, 16));
		okButton.setBackground(JPGColorLibrary.JPG_CRIMSON);
		okButton.setForeground(JPGColorLibrary.PURE_WHITE);
		okButton.addActionListener(this);

		cancelButton = new JButton("Cancel");
		cancelButton.setFont(new Font("Segoe UI", Font.BOLD, 16));
		cancelButton.setBackground(JPGColorLibrary.JPG_CRIMSON);
		cancelButton.setForeground(JPGColorLibrary.PURE_WHITE);
		cancelButton.addActionListener(this);

		buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPane.add(okButton);
		buttonPane.add(cancelButton);

		textField = new JEditorPane();
		textField.setText( Journal.getAdditionalNotes( puzzle));

		title = new JLabel("Edit my notes on the Journal");
		title.setFont(new Font("Segoe UI", Font.BOLD | Font.ITALIC, 24));
		title.setForeground(JPGColorLibrary.JPG_CRIMSON);

		contentPanel.setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPanel.add(textField, BorderLayout.CENTER);
		contentPanel.add(title, BorderLayout.NORTH);

		JScrollPane scrollpane = new JScrollPane(contentPanel);

		add(buttonPane, BorderLayout.SOUTH);
		add(scrollpane, BorderLayout.CENTER);

		// Get Screen size and determine the dimensions of the window
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension windowSize = new Dimension(screenSize.width / 5 * 2,
				screenSize.height / 5 * 2);

		setBounds((screenSize.width - windowSize.width) / 2,
				(screenSize.height - windowSize.height) / 2, windowSize.width,
				windowSize.height); // centered

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		setTitle("Java Programming Game - Edit Notes");

		setVisible(true);
	}

	// METHODS
	// ---------------------------------------------------------------------------------------------------------------

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == okButton) {
			Journal.setAdditionalNotes(puzzle, textField.getText());
			
			dispose();
		}

		else if (e.getSource() == cancelButton) {
			dispose();
		}
	}

} //End of EditNotesDialog
