package jpgame.journal;

import java.util.*;
import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.*;

import jpgame.JPGColorLibrary;
import jpgame.JPGComponent;

/**
*  JournalPageView.java
* 
*  JournalPageView application at JavaProgrammingGame
* 
*  Creates the journal view to display the information related to a particular puzzle
*  
*  Author: Ayse Berceste Dincer
*  version 1.00 2014/27/4 
**/

public class JournalPageView extends JPanel implements JPGJournalPageView {

	private static final long serialVersionUID = 548882934180100413L;

	// PROPERTIES
	protected JournalPage journalPage;
	private JLabel titleLabel;
	private JPanel infoPanel;
	private JPanel cluesPanel;
	private JPanel scenePanel;
	private JPanel codePanel;
	private JPanel mainPanel;
	private JPanel titlePanel;
	private JPanel buttonsPanel;
	private JPanel additionalNotesPanel;
	private JLabel timeLabel;
	private JLabel runLabel;
	private JLabel runNoLabel;
	private JLabel compileLabel;
	private JLabel compileNoLabel;
	private JLabel scoreLabel;
	private JButton nextButton;
	private JButton previousButton;
	private JButton editButton;
	private JButton exitButton;
	private JButton addToPortfolioButton;
	private JTextArea codeField;
	private JLabel sceneLabel;
	private ArrayList<JLabel> clueContent;
	private JTextArea notes;
	private JEditorPane puzzleQuestion;
	private JTextArea solutionField;
	private JScrollPane mainPane;

	// CONSTRUCTOR
	public JournalPageView(JournalPage journalPage) {
		// Creates the border layout
		super(new BorderLayout());
		// Initializes the properties
		this.journalPage = journalPage;

		// Getting screen size
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

		// Creates and adds the title panel
		titlePanel = new JPanel(new BorderLayout());

		titleLabel = new JLabel();
		titleLabel.setText("  " + journalPage.getPuzzle().getPuzzleName());
		titleLabel.setFont(new Font("Segoe UI", Font.BOLD | Font.BOLD, 30));
		titleLabel.setForeground(JPGColorLibrary.JPG_CRIMSON);
		titleLabel.setAlignmentX(CENTER_ALIGNMENT);

		addToPortfolioButton = new JButton();
		addToPortfolioButton.setFont(new Font("Segoe UI", Font.BOLD
				| Font.BOLD, 20));
		
		if (journalPage.getPuzzle().isInPortfolio()) {
			addToPortfolioButton.setText("Remove From Porfolio");
			addToPortfolioButton.setForeground(JPGColorLibrary.JPG_CRIMSON);
			addToPortfolioButton.setBackground(JPGColorLibrary.PURE_WHITE);
		} else {
			addToPortfolioButton.setText("Add To Portfolio");
			addToPortfolioButton.setBackground(JPGColorLibrary.JPG_CRIMSON);
			addToPortfolioButton.setForeground(JPGColorLibrary.PURE_WHITE);
			}
		
		

		titlePanel.add(titleLabel, "West");
		titlePanel.add(addToPortfolioButton, "East");
		add(titlePanel, "North");
		// Main Panel in Box Layout
		mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		// Creates and the scene panel which holds the picture of the scene
		scenePanel = new JPanel();
		scenePanel.setPreferredSize(new Dimension(screenSize.width - 200, 500));
		sceneLabel = new JLabel();
		sceneLabel.setIcon(journalPage.getPuzzle().getPuzzleBackground());
		scenePanel.add(sceneLabel);
		scenePanel.setBackground(JPGColorLibrary.JOURN_SCENE_BACKGR);
		JLabel sceneTitle = new JLabel("SCENE OF THE PUZZLE");
		sceneTitle.setFont(new Font("Segoe UI", Font.BOLD | Font.BOLD, 20)); 
		sceneTitle.setForeground(JPGColorLibrary.BUTTON_BLUE);
		sceneTitle.setAlignmentX(CENTER_ALIGNMENT);
		mainPanel.add(sceneTitle);
		mainPanel.add(scenePanel);
		// Puzzle Question Panel
		JLabel puzzleQuestionLabel = new JLabel("PUZZLE QUESTION");
		puzzleQuestionLabel.setAlignmentX(CENTER_ALIGNMENT);
		puzzleQuestionLabel.setForeground(JPGColorLibrary.BUTTON_BLUE);
		puzzleQuestionLabel.setFont(new Font("Segoe UI", Font.BOLD | Font.BOLD,
				20));
		puzzleQuestion = new JEditorPane();
		puzzleQuestion.setContentType("text/html");
		puzzleQuestion.setEditable(false);
		puzzleQuestion.setText(journalPage.getPuzzle().getPuzzleQuestion());
		puzzleQuestion.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		puzzleQuestion.setForeground(new Color(25, 25, 40));
		puzzleQuestion.setBackground(JPGColorLibrary.JOURN_QUESTION_BACKGR);
		JScrollPane puzzleQuestionScrollPane = new JScrollPane(puzzleQuestion);
		puzzleQuestionScrollPane.setPreferredSize(new Dimension(screenSize.width - 200, 200));
		mainPanel.add(puzzleQuestionLabel);
		mainPanel.add(puzzleQuestionScrollPane);
		// Creates the clue panel which holds the texts of collected clues
		cluesPanel = new JPanel();
		cluesPanel.setPreferredSize(new Dimension(screenSize.width - 200, 200));
		clueContent = new ArrayList<JLabel>();
		
		for (int i = 0; i < journalPage.getPuzzle().getPreviousClues().size(); i++) {
			clueContent.add( new JLabel());
			clueContent.get(i).setText(
					"<html>" + journalPage.getPuzzle().getPreviousClues().get(i).getText()
							+ "</html>");
		}
		for (int i = 0; i < clueContent.size(); i++) {

			cluesPanel.add(clueContent.get(i));
		}

		JLabel clueTitle = new JLabel("CLUES YOU HAVE FOUND");
		clueTitle.setFont(new Font("Segoe UI", Font.BOLD | Font.BOLD, 20));
		clueTitle.setForeground(JPGColorLibrary.BUTTON_BLUE);
		clueTitle.setAlignmentX(CENTER_ALIGNMENT);
		cluesPanel.setBackground(JPGColorLibrary.JOURN_QUESTION_BACKGR);
		mainPanel.add(clueTitle);
		mainPanel.add(cluesPanel);
		// Creates solution panel
		JPanel solutionPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		solutionField = new JTextArea();
		solutionField.setBackground(JPGColorLibrary.JOURN_SOLN_BACKGR);
		if (journalPage.getPuzzle().isSolved()) {
			solutionField.setText(journalPage.getPuzzle().getUsersSolution());
		} else {
			solutionField.setText("Not solved yet");
		}
		solutionField.setFont(new Font("Segoe UI", Font.PLAIN, 20));
		solutionField.setForeground(JPGColorLibrary.BUTTON_BLUE);
		solutionField.setEditable(false);
		// !!!The following line should not be commented out!!!
		solutionField.setPreferredSize(new Dimension(screenSize.width - 200,
				400));
		solutionField.setLineWrap(true);
		solutionPanel.add(solutionField);
		JScrollPane solutionScroll = new JScrollPane(solutionPanel);
		solutionScroll.setPreferredSize(new Dimension(screenSize.width - 200,
				400));
		solutionPanel.setBackground(JPGColorLibrary.JOURN_SOLN_BACKGR);
		JLabel solutionTitle = new JLabel("YOUR SOLUTION");
		solutionTitle.setHorizontalAlignment(JLabel.CENTER);
		solutionTitle.setFont(new Font("Segoe UI", Font.BOLD, 20));
		solutionTitle.setForeground(JPGColorLibrary.BUTTON_BLUE);
		solutionTitle.setAlignmentX(CENTER_ALIGNMENT);
		mainPanel.add(solutionTitle);
		mainPanel.add(solutionScroll);

		// Creates the code panel which holds the java code of the user
		codePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		codeField = new JTextArea();
		codeField.setBackground(JPGColorLibrary.JOURN_CODES_BACKGR);
		String codeText = "";

		if (journalPage != null && journalPage.getPuzzle().getUserCodes() != null) {
			for (int i = journalPage.getPuzzle().getUserCodes().size() - 1; i >= 0; i--) {
				codeText += "Code " + (i + 1) + ":\n"
						+ journalPage.getPuzzle().getUserCodes().get(i) + "\n\n";
			}
		}
		codeField.setText(codeText);
		codeField.setFont(new Font("Segoe UI", Font.PLAIN, 20));
		codeField.setForeground(JPGColorLibrary.BUTTON_BLUE);
		codeField.setEditable(false);
		codeField.setPreferredSize(new Dimension(screenSize.width - 200, 400));
		codeField.setLineWrap(true);
		codePanel.add(codeField);
		JScrollPane codeScroll = new JScrollPane(codePanel);
		// codeScroll.setPreferredSize(new Dimension(screenSize.width - 200,
		// 200));
		codePanel.setBackground(JPGColorLibrary.JOURN_CODES_BACKGR);
		JLabel codeTitle = new JLabel("THE CODES YOU HAVE WRITTEN");
		codeTitle.setHorizontalAlignment(JLabel.CENTER);
		codeTitle.setFont(new Font("Segoe UI", Font.BOLD, 20));
		codeTitle.setForeground(JPGColorLibrary.BUTTON_BLUE);
		codeTitle.setAlignmentX(CENTER_ALIGNMENT);
		mainPanel.add(codeTitle);
		mainPanel.add(codeScroll);

		// Creates the panel which holds the user informations related to the
		// specific puzzle
		JPanel overall = new JPanel();
		overall.setBackground(JPGColorLibrary.JOURN_BACKGR);
		overall.setPreferredSize(new Dimension(screenSize.width - 200, 200));
		infoPanel = new JPanel(new GridLayout(7, 1));
		infoPanel.setAlignmentX(SwingConstants.RIGHT);
		// Time label
		timeLabel = new JLabel("Time spend on this puzzle: "
				+ journalPage.getPuzzle().getTime());
		timeLabel.setFont(new Font("Segoe UI", Font.PLAIN, 18));
		timeLabel.setForeground(JPGColorLibrary.BUTTON_BLUE);
		infoPanel.add(timeLabel);
		// Compilation label
		compileNoLabel = new JLabel("Number of compilations: "
				+ journalPage.getPuzzle().getNumberOfCompilations());
		compileNoLabel.setFont(new Font("Segoe UI", Font.PLAIN, 18));
		compileNoLabel.setForeground(JPGColorLibrary.BUTTON_BLUE);
		infoPanel.add(compileNoLabel);
		// Compile-time errors label
		compileLabel = new JLabel("Number of compile time errors: "
				+ journalPage.getPuzzle().getNumberOfCompilationErrors());
		compileLabel.setFont(new Font("Segoe UI", Font.PLAIN, 18));
		compileLabel.setForeground(JPGColorLibrary.BUTTON_BLUE);
		infoPanel.add(compileLabel);
		// Submissions label
		runNoLabel = new JLabel("Number of submissions: "
				+ journalPage.getPuzzle().getNumberOfSubmissions());
		runNoLabel.setFont(new Font("Segoe UI", Font.PLAIN, 18));
		runNoLabel.setForeground(JPGColorLibrary.BUTTON_BLUE);
		infoPanel.add(runNoLabel);
		// Run-time errors label
		runLabel = new JLabel("Number of run time errors: "
				+ journalPage.getPuzzle().getNumberOfRuntimeErrors());
		runLabel.setFont(new Font("Segoe UI", Font.PLAIN, 18));
		runLabel.setForeground(JPGColorLibrary.BUTTON_BLUE);
		infoPanel.add(runLabel);
		// Score label
		scoreLabel = new JLabel("Collected score: " + journalPage.getPuzzle().getScore());
		scoreLabel.setFont(new Font("Segoe UI", Font.PLAIN, 18));
		scoreLabel.setForeground(JPGColorLibrary.BUTTON_BLUE);
		infoPanel.add(scoreLabel);
		// Adds the infopanel to main panel
		infoPanel.setPreferredSize(new Dimension(screenSize.width - 200, 200));
		infoPanel.setBackground(JPGColorLibrary.JOURN_BACKGR);
		JLabel infoTitle = new JLabel("YOUR PUZZLE INFORMATION");
		infoTitle.setFont(new Font("Segoe UI", Font.BOLD, 20));
		infoTitle.setForeground(JPGColorLibrary.BUTTON_BLUE);
		infoTitle.setAlignmentX(CENTER_ALIGNMENT);
		overall.add(infoPanel);
		mainPanel.add(infoTitle);
		mainPanel.add(overall);
		// Adds the user notes to the main panel
		additionalNotesPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		notes = new JTextArea();
		notes.setEnabled(false);
		notes.setDisabledTextColor(JPGColorLibrary.PURE_BLACK);
		notes.setForeground(JPGColorLibrary.PURE_BLACK);
		notes.setText(journalPage.getAdditionalNotes());
		notes.setBackground(JPGColorLibrary.JOURN_NOTES_BACKGR);
		notes.setFont(new Font("Segoe UI", Font.BOLD, 20));
		additionalNotesPanel.add(notes);
		additionalNotesPanel.setBackground(JPGColorLibrary.JOURN_NOTES_BACKGR);
		JLabel addTitle = new JLabel("ADDITIONAL NOTES");
		addTitle.setFont(new Font("Segoe UI", Font.BOLD, 18));
		addTitle.setForeground(JPGColorLibrary.BUTTON_BLUE);
		addTitle.setAlignmentX(CENTER_ALIGNMENT);
		JScrollPane notesScrollPane = new JScrollPane(additionalNotesPanel);
		notesScrollPane.setPreferredSize(new Dimension(screenSize.width - 200,
				200));
		mainPanel.add(addTitle);
		mainPanel.add(notesScrollPane);
		// Scroll pane for the main panel
		mainPane = new JScrollPane(mainPanel);
		add(mainPane);
		// Buttons panel
		buttonsPanel = new JPanel(new GridLayout(1, 4));
		// Exit button
		exitButton = new JButton("Exit Journal");
		JPanel exitButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		exitButton.setBackground(JPGColorLibrary.JPG_CRIMSON);
		exitButton.setForeground(JPGColorLibrary.PURE_WHITE);
		exitButton.setFont(new Font("Segoe UI", Font.BOLD | Font.BOLD, 20));
		exitButtonPanel.add(exitButton);
		// Previous page button
		previousButton = new JButton("Previous Page");
		JPanel prevPagePanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		previousButton.setBackground(JPGColorLibrary.JPG_CRIMSON);
		previousButton.setForeground(JPGColorLibrary.PURE_WHITE);
		previousButton.setFont(new Font("Segoe UI", Font.BOLD | Font.BOLD, 20));
		prevPagePanel.add(previousButton);
		// Edit button
		editButton = new JButton("Edit My Notes");
		JPanel editNotesButtonPanel = new JPanel(new FlowLayout(
				FlowLayout.RIGHT));
		editButton.setBackground(JPGColorLibrary.JPG_CRIMSON);
		editButton.setForeground(JPGColorLibrary.PURE_WHITE);
		editButton.setFont(new Font("Segoe UI", Font.BOLD | Font.BOLD, 20));
		editNotesButtonPanel.add(editButton);
		// Next page button
		nextButton = new JButton("Next Page");
		JPanel nextPageButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		nextButton.setBackground(JPGColorLibrary.JPG_CRIMSON);
		nextButton.setForeground(JPGColorLibrary.PURE_WHITE);
		nextButton.setFont(new Font("Segoe UI", Font.BOLD | Font.BOLD, 20));
		nextPageButtonPanel.add(nextButton);
		// Adds buttons panel to south
		buttonsPanel.add(exitButtonPanel);
		buttonsPanel.add(prevPagePanel);
		buttonsPanel.add(nextPageButtonPanel);
		buttonsPanel.add(editNotesButtonPanel);
		add(buttonsPanel, "South");
		// Makes the panel full screen
		setBackground(JPGColorLibrary.BUTTON_BLUE);
		setPreferredSize(screenSize);
		setVisible(true);
	}

	// METHODS
	
	public JPanel getTitlePanel() {
		return titlePanel;
	}

	public JButton getPortfolioButton() {
		return addToPortfolioButton;
	}

	public JournalPage getJournalPage() {
		return journalPage;
	}

	public JLabel getTitleLabel() {
		return titleLabel;
	}

	public JPanel getInfoPanel() {
		return infoPanel;
	}

	public JPanel getCluesPanel() {
		return cluesPanel;
	}

	public JPanel getScenePanel() {
		return scenePanel;
	}

	public JPanel getCodePanel() {
		return codePanel;
	}

	public JPanel getMainPanel() {
		return mainPanel;
	}

	public JPanel getButtonsPanel() {
		return buttonsPanel;
	}

	public JLabel getTimeLabel() {
		return timeLabel;
	}

	public JLabel getRunLabel() {
		return runLabel;
	}

	public JLabel getRunNoLabel() {
		return runNoLabel;
	}

	public JLabel getCompileLabel() {
		return compileLabel;
	}

	public JLabel getCompileNoLabel() {
		return compileNoLabel;
	}

	public JLabel getScoreLabel() {
		return scoreLabel;
	}

	public JButton getNextButton() {
		return nextButton;
	}

	public JButton getPreviousButton() {
		return previousButton;
	}

	public JButton getEditButton() {
		return editButton;
	}

	public JButton getExitButton() {
		return exitButton;
	}

	public JTextArea getCodeField() {
		return codeField;
	}
	
	public JPanel getAdditionalNotesPanel() {
		return additionalNotesPanel;
	}

	public void addActionListeners(ActionListener actionListener) {
		if (addToPortfolioButton.getActionListeners().length == 0) {
			addToPortfolioButton.addActionListener(actionListener);
			nextButton.addActionListener(actionListener);
			previousButton.addActionListener(actionListener);
			editButton.addActionListener(actionListener);
			exitButton.addActionListener(actionListener);
		}
	}

	// Overrides the paintComponent method
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
	}

	@Override
	public void updateView(JPGComponent journalPage) {
		if (journalPage instanceof JournalPage) {
			this.journalPage = (JournalPage) journalPage;
			
			sceneLabel.setIcon( this.journalPage.getPuzzle().getPuzzleBackground());
			titleLabel.setText("  " + this.journalPage.getPuzzle().getPuzzleName());
			puzzleQuestion.setText( this.journalPage.getPuzzle().getPuzzleQuestion());
			timeLabel.setText("Time spend on this puzzle: "
					+ this.journalPage.getPuzzle().getTime());
			runLabel.setText("Number of run time errors: "
					+ this.journalPage.getPuzzle().getNumberOfRuntimeErrors());
			runNoLabel.setText("Number of submissions: "
					+ this.journalPage.getPuzzle().getNumberOfSubmissions());
			compileLabel.setText("Number of compile time errors: "
					+ this.journalPage.getPuzzle().getNumberOfCompilationErrors());
			compileNoLabel.setText("Number of compilations: "
					+ this.journalPage.getPuzzle().getNumberOfCompilations());
			scoreLabel.setText("Collected score: "
					+ this.journalPage.getPuzzle().getScore());

			String codeText = "";
			if ( this.journalPage.getPuzzle().getUserCodes() != null) {
				for (int i = this.journalPage.getPuzzle().getUserCodes().size() - 1; i >= 0; i--) {
					codeText += "Code " + (i + 1) + ":\n"
							+ this.journalPage.getPuzzle().getUserCodes().get(i)
							+ "\n";
				}
			}
			codeField.setText(codeText);
			
			if (this.journalPage.getPuzzle().isSolved()) {
				solutionField.setText(this.journalPage.getPuzzle().getUsersSolution());
			} else {
				solutionField.setText("Not solved yet");
			}

			clueContent = new ArrayList<JLabel>();
		
			for (int i = 0; i < this.journalPage.getPuzzle()
					.getPreviousClues().size(); i++) {
				clueContent.add(new JLabel("<html>"
						+ this.journalPage.getPuzzle()
								.getPreviousClues().get(i).getText()
						+ "</html>"));
			}
			
			cluesPanel.removeAll();
			for (int i = 0; i < clueContent.size(); i++) {

				cluesPanel.add(clueContent.get(i));
			}

			if (this.journalPage.getPuzzle().isInPortfolio()) {
				addToPortfolioButton.setText("Remove From Porfolio");
				addToPortfolioButton.setForeground(JPGColorLibrary.JPG_CRIMSON);
				addToPortfolioButton.setBackground(JPGColorLibrary.PURE_WHITE);
			} else {
				addToPortfolioButton.setText("Add To Portfolio");
				addToPortfolioButton.setBackground(JPGColorLibrary.JPG_CRIMSON);
				addToPortfolioButton.setForeground(JPGColorLibrary.PURE_WHITE);
			}
			
			notes.setText(this.journalPage.getAdditionalNotes());
			
			repaint();
			mainPane.getVerticalScrollBar().setValue( 0); // scrolls to the top
			
		}
	}
} // End of the JournalPageView

