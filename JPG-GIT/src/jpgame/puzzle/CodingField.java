package jpgame.puzzle;

import java.awt.Font;

import javax.swing.JTextArea;

/**
 * CodingField 	The coding field for the PuzzleView.
 *						
 * @author Burcu Canakci
 * @version 1.00 2014/5/1
 */
public class CodingField extends JTextArea
{
	//Serial NO
	private static final long serialVersionUID = 5372964270580777419L;

	//Constructor
	public CodingField (Puzzle puzzle)
	{		
		setText(puzzle.getUsersSolution());
		setFont(new Font("Monospaced", Font.BOLD, 16 ));

	}


}//End of CodingField
