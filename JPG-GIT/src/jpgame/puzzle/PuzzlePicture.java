package jpgame.puzzle;

import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;

import jpgame.puzzle.clues.Clue;
import jpgame.puzzle.clues.ClueFoundDialog;

public class PuzzlePicture extends JPanel implements MouseListener
{

	private static final long serialVersionUID = 3626397686058024069L;
	// PROPERTIES
	private Puzzle puzzle;

	// CONSTRUCTOR
	public PuzzlePicture(Puzzle puzzle)
	{
		this.puzzle = puzzle;
		setOpaque(false);
		addMouseListener(this);
	}

	// METHODS
	public void updateView(Puzzle puzzle)
	{
		repaint();
	}

	@Override
	public void paintComponent(Graphics page)
	{
		super.paintComponent(page);
		// Draw clues if they are not selected yet.
		page.drawImage(puzzle.getPuzzleBackground().getImage(), 0, 0, null);
		for (Clue c : puzzle.getAllClues())
			if (!c.getSelected())
				c.draw(page);
	}

	@Override
	public void mouseClicked(MouseEvent m)
	{
		// See if the clicked point corresponds to a clue point.
		for (Clue c : puzzle.getAllClues())
			if (!c.getSelected() && c.canDisplay() && c.contains(m.getPoint()))
			{
				c.setSelected(true);
				new ClueFoundDialog();
				puzzle.notifyViews();
			}
	}

	@Override
	public void mouseEntered(MouseEvent e){}

	@Override
	public void mouseExited(MouseEvent e){}

	@Override
	public void mousePressed(MouseEvent e){}

	@Override
	public void mouseReleased(MouseEvent e){}
}