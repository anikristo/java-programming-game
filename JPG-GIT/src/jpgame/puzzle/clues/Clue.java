package jpgame.puzzle.clues;

/**
 * Clue	a class to hold information about clues
 *
 * @author Burcu Canakci
 * @version 1.00 2014/4/20
 */

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.ImageObserver;

import javax.swing.ImageIcon;

import jpgame.partner.hints.Hint;
import jpgame.puzzle.Locatable;
import jpgame.puzzle.Puzzle;

public class Clue extends Hint implements Locatable, Selectable
{
	//Serial NO
	private static final long serialVersionUID = -3204841416275516746L;
	// PROPERTIES
	private ImageIcon picture;
	private Rectangle borders;
	private Point location;

	// CONSTRUCTOR
	public Clue(Puzzle puzzle)
	{
		super(puzzle);
		borders = new Rectangle();
		location = new Point();
		picture = new ImageIcon();
	}

	// METHODS

	//Method for returning the picture
	public ImageIcon getPicture()
	{
		return picture;
	}

	//Method for returning the borders
	public Rectangle getBorders()
	{
		return borders;
	}
	
	//Method for returning the location
	public Point getLocation()
	{
		return location;
	}

	//Method for modifying the picture
	public void setImageIcon(ImageIcon picture)
	{
		this.picture = picture;
		setBorders();
	}

	//Method for modifying the locaiton
	public void setLocation(Point location)
	{
		this.location = location;
	}

	//Method for setting the borders
	public void setBorders()
	{
		borders = new Rectangle(location, new Dimension(picture.getIconWidth(),
				picture.getIconHeight()));
	}

	// checks if the borders contains a given point
	public boolean contains(Point point)
	{
		if ((point.getX() - location.getX()) < borders.getWidth()
				&& point.getY() - location.getY() < borders.getHeight())
		{
			return true;
		}
		return false;
	}

	//Method for drawing the picture
	public void draw(Graphics page)
	{
		page.drawImage(picture.getImage(), (int) location.getX(),
				(int) location.getY(), (int) picture.getIconWidth(),
				(int) picture.getIconHeight(), new ImageObserver()
				{
					@Override
					public boolean imageUpdate(Image arg0, int arg1, int arg2,
							int arg3, int arg4, int arg5)
					{
						return false;
					}
				});
	}

	@Override
	public boolean getSelected()
	{
		return isReceived;
	}

	@Override
	public void setSelected(boolean selected)
	{
		isReceived = selected;
	}

} //End of Clue
