package jpgame.puzzle.clues;

/**
 * Selectable an inteface to make the implemented classes selectable.
 *
 * @author Burcu Canakci
 * @version 1.00 2014/4/20
 */

import java.awt.Point;

public interface Selectable
{
	boolean getSelected(); // returns the selected state of the object

	void setSelected(boolean selected);// changes the selected state of
												// the object

	boolean contains(Point p);// returns a shape that contains the given
										// coordinate
}
