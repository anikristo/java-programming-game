/**
 * @author Ani Kristo
 * @version 1.0
 * 
 * 12 / 05 / 2014
 * 
 * DESCRIPTION: This is a JDialog which will communicate that the clicked area in the puzzle background was actually contained
 * 				by the clue icon. It will inform the user that the clue's information is written in the journal. 
 */
package jpgame.puzzle.clues;

import java.awt.BorderLayout;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import jpgame.JPGColorLibrary;
import jpgame.JPGComponent;

public class ClueFoundDialog extends JDialog implements ActionListener,
		JPGComponent {

	//Serial NO
	private static final long serialVersionUID = 6194033556166791268L;
	// PROPERTIES
	private JLabel cluefoundLb;
	private JTextArea content;
	private JButton okButton;
	private JPanel body;
	private JPanel buttonPane;

	// CONSTRUCTOR
	public ClueFoundDialog() {
		cluefoundLb = new JLabel("You just found a Clue!");
		cluefoundLb.setFont(new Font("Segoe UI", Font.BOLD | Font.ITALIC, 24));
		cluefoundLb.setForeground(JPGColorLibrary.JPG_CRIMSON);

		content = new JTextArea();
		content.setText("All the information it contains is automatically saved in the Journal.");
		content.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		content.setLineWrap(true);
		content.setOpaque(false);
		content.setEnabled(false);
		content.setDisabledTextColor(JPGColorLibrary.PURE_BLACK);

		okButton = new JButton("OK");
		okButton.setBackground(JPGColorLibrary.JPG_CRIMSON);
		okButton.setForeground(JPGColorLibrary.PURE_WHITE);
		okButton.addActionListener(this);

		body = new JPanel(new GridLayout(3, 1));
		body.add(cluefoundLb);
		body.add(content);
		body.setBorder(new EmptyBorder(new Insets(20, 20, 20, 20)));

		buttonPane = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		buttonPane.add(okButton);

		setLayout(new BorderLayout());
		add(buttonPane, BorderLayout.SOUTH);
		add(body, BorderLayout.CENTER);
		setTitle("New Clue");
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds((screenSize.width - 540) / 2, (screenSize.height - 180) / 2,
				540, 180);
		setVisible(true);

	}

	// METHODS
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == okButton)
			dispose();
	}

} //End of ClueFoundDialog
