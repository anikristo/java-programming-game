package jpgame.puzzle.successpage;

import java.awt.BorderLayout;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import jpgame.JPGColorLibrary;
import jpgame.JPGComponent;
import jpgame.puzzle.Puzzle;

/**
* SuccessView.java
*
* SuccessView application at JavaProgrammingGame
*
* Creates the panel which is displayed when a puzzle is successfully completed
* The time taken to complete the puzzle, the score and errors will be displayed in the success panel
* 
* Author: Ayse Berceste Dincer
* version 1.00 2014/27/4 
**/

public class SuccessView extends JPanel implements JPGComponent, JPGSuccessView {

	//Serial NO
	private static final long serialVersionUID = 6726822006229561397L;

	// PROPERTIES
	private JPanel titlePanel;
	private JLabel successTitle;
	private JPanel buttonsPanel;
	private JLabel titleLabel;
	private JLabel timeLabel;
	private JLabel runErrLabel;
	private JLabel runNoLabel;
	private JLabel compileErrLabel;
	private JLabel compileNoLabel;
	private JLabel scoreLabel;
	private JPanel descriptionTextsPanels;
	private JPanel valuesPanel;
	private JButton homepageButton;
	private JButton nextButton;
	private JButton notesButton;

	// CONSTRUCTOR
	public SuccessView(Puzzle puzzle) {
		// Creates the border layout
		super(new BorderLayout());;

		// PANEL for the title
		titlePanel = new JPanel(new GridLayout(2, 1));
		titleLabel = new JLabel();
		titleLabel.setText(puzzle.getPuzzleName());
		titleLabel.setFont(new Font("Segoe UI", Font.BOLD | Font.ITALIC, 24));
		titleLabel.setForeground(JPGColorLibrary.TEXT_BLUE);
		titleLabel.setBorder(BorderFactory.createEmptyBorder(50, 0, 0, 0));
		titlePanel.add(titleLabel);
		new JPanel(new GridLayout(20, 1));

		descriptionTextsPanels = new JPanel(new GridLayout(6, 1));
		descriptionTextsPanels.setAlignmentX(JPanel.RIGHT_ALIGNMENT);
		valuesPanel = new JPanel(new GridLayout(6, 1));

		// Creates the heading
		successTitle = new JLabel("Success!");
		successTitle.setFont(new Font("Segoe UI", Font.BOLD | Font.ITALIC, 65));
		successTitle.setForeground(JPGColorLibrary.JPG_CRIMSON);
		JPanel successTitlePanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		successTitlePanel.add(successTitle);
		titlePanel.add(successTitlePanel);

		// Time label
		JLabel descriptionOfTimeLabel = new JLabel(
				"Time taken to complete the puzzle: ");
		descriptionOfTimeLabel.setFont(new Font("Segoe UI", Font.ITALIC, 16));
		descriptionOfTimeLabel.setForeground(JPGColorLibrary.TEXT_BLUE);
		descriptionOfTimeLabel.setHorizontalAlignment(JLabel.RIGHT);
		descriptionTextsPanels.add(descriptionOfTimeLabel);

		timeLabel = new JLabel(puzzle.getTime() + " seconds");
		timeLabel.setFont(new Font("Segoe UI", Font.BOLD | Font.ITALIC, 16));
		timeLabel.setForeground(JPGColorLibrary.JPG_CRIMSON);
		valuesPanel.add(timeLabel);

		// Compilation Label
		JLabel descriptionOfComplNo = new JLabel("Number of compilations: ");
		descriptionOfComplNo.setFont(new Font("Segoe UI", Font.ITALIC, 16));
		descriptionOfComplNo.setForeground(JPGColorLibrary.TEXT_BLUE);
		descriptionOfComplNo.setHorizontalAlignment(JLabel.RIGHT);
		descriptionTextsPanels.add(descriptionOfComplNo);

		compileNoLabel = new JLabel(puzzle.getNumberOfCompilations()
				+ " compilations");
		compileNoLabel
				.setFont(new Font("Segoe UI", Font.BOLD | Font.ITALIC, 16));
		compileNoLabel.setForeground(JPGColorLibrary.JPG_CRIMSON);
		valuesPanel.add(compileNoLabel);

		// Compile-time errors label
		JLabel descriptionOfComplErr = new JLabel(
				"Number of compile time errors: ");
		descriptionOfComplErr.setFont(new Font("Segoe UI", Font.ITALIC, 16));
		descriptionOfComplErr.setForeground(JPGColorLibrary.TEXT_BLUE);
		descriptionOfComplErr.setHorizontalAlignment(JLabel.RIGHT);
		descriptionTextsPanels.add(descriptionOfComplErr);

		compileErrLabel = new JLabel(puzzle.getNumberOfCompilationErrors() + " errors");
		compileErrLabel.setFont(new Font("Segoe UI", Font.BOLD | Font.ITALIC,
				16));
		compileErrLabel.setForeground(JPGColorLibrary.JPG_CRIMSON);
		valuesPanel.add(compileErrLabel);

		// Submission label
		JLabel descriptionOfRunNo = new JLabel("Number of submissions: ");
		descriptionOfRunNo.setFont(new Font("Segoe UI", Font.ITALIC, 16));
		descriptionOfRunNo.setForeground(JPGColorLibrary.TEXT_BLUE);
		descriptionOfRunNo.setHorizontalAlignment(JLabel.RIGHT);
		descriptionTextsPanels.add(descriptionOfRunNo);

		runNoLabel = new JLabel(puzzle.getNumberOfSubmissions()
				+ " submissions");
		runNoLabel.setFont(new Font("Segoe UI", Font.BOLD | Font.ITALIC, 16));
		runNoLabel.setForeground(JPGColorLibrary.JPG_CRIMSON);
		valuesPanel.add(runNoLabel);

		// Run-time errors label
		JLabel descriptionOfRunErr = new JLabel("Number of run time errors: ");
		descriptionOfRunErr.setFont(new Font("Segoe UI", Font.ITALIC, 16));
		descriptionOfRunErr.setForeground(JPGColorLibrary.TEXT_BLUE);
		descriptionOfRunErr.setHorizontalAlignment(JLabel.RIGHT);
		descriptionTextsPanels.add(descriptionOfRunErr);

		runErrLabel = new JLabel(puzzle.getNumberOfRuntimeErrors() + " errors");
		runErrLabel.setFont(new Font("Segoe UI", Font.BOLD | Font.ITALIC, 16));
		runErrLabel.setForeground(JPGColorLibrary.JPG_CRIMSON);
		valuesPanel.add(runErrLabel);

		// Score label
		JLabel descriptionOfScores = new JLabel("Collected score: ");
		descriptionOfScores.setFont(new Font("Segoe UI", Font.ITALIC, 16));
		descriptionOfScores.setForeground(JPGColorLibrary.TEXT_BLUE);
		descriptionOfScores.setHorizontalAlignment(JLabel.RIGHT);
		descriptionTextsPanels.add(descriptionOfScores);

		scoreLabel = new JLabel(puzzle.getScore() + " points");
		scoreLabel.setFont(new Font("Segoe UI", Font.BOLD | Font.ITALIC, 16));
		scoreLabel.setForeground(JPGColorLibrary.JPG_CRIMSON);
		valuesPanel.add(scoreLabel);

		// Body panel
		JPanel bodyPanel = new JPanel(new GridLayout(1, 2, 5, -50));
		bodyPanel.add(descriptionTextsPanels);
		bodyPanel.add(valuesPanel);

		// BUTTONS PANEL
		buttonsPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 30, 10));

		// Homepage button
		homepageButton = new JButton("Go to HomePage");
		homepageButton.setBackground(JPGColorLibrary.JPG_CRIMSON);
		homepageButton.setFont(new Font("Segoe UI", Font.BOLD, 18));
		homepageButton.setForeground(JPGColorLibrary.PURE_WHITE);
		buttonsPanel.add(homepageButton);

		// Edit notes button
		notesButton = new JButton("Add notes to my journal");
		notesButton.setBackground(JPGColorLibrary.JPG_CRIMSON);
		notesButton.setFont(new Font("Segoe UI", Font.BOLD, 18));
		notesButton.setForeground(JPGColorLibrary.PURE_WHITE);
		buttonsPanel.add(notesButton);

		// Next button
		nextButton = new JButton("Go to next puzzle");
		nextButton.setBackground(JPGColorLibrary.JPG_CRIMSON);
		nextButton.setFont(new Font("Segoe UI", Font.BOLD, 18));
		nextButton.setForeground(JPGColorLibrary.PURE_WHITE);
		buttonsPanel.add(nextButton);

		// Adds buttonsPanel to primary panel
		add(titlePanel, "North");
		add(bodyPanel, BorderLayout.CENTER);
		add(buttonsPanel, "South");

		// Makes the primary panel full screen
		setPreferredSize(new Dimension(Toolkit.getDefaultToolkit()
				.getScreenSize()));
		setVisible(true);
	}

	// METHODS
	
	// Update method to update the success page
	public void updateView(Puzzle puzzle) {
		
		timeLabel.setText(puzzle.getTime() + " seconds");
		runErrLabel.setText(puzzle.getNumberOfRuntimeErrors() + " errors");
		runNoLabel.setText(puzzle.getNumberOfSubmissions() + " submissions");
		compileErrLabel.setText(puzzle.getNumberOfCompilationErrors() + " errors");
		compileNoLabel.setText(puzzle.getNumberOfCompilations()
				+ " compilations");
		scoreLabel.setText(puzzle.getScore() + " points");
	}

	//Method for returning title panel
	public JPanel getTitlePanel() {
		return titlePanel;
	}

	//Method for returning success title
	public JLabel getSuccessTitle() {
		return successTitle;
	}

	//Method for returning buttons panel
	public JPanel getButtonsPanel() {
		return buttonsPanel;
	}

	//Method for returning title label
	public JLabel getTitleLabel() {
		return titleLabel;
	}

	//Method for returning time label
	public JLabel getTimeLabel() {
		return timeLabel;
	}

	//Method for returning run-time errors label
	public JLabel getRunLabel() {
		return runErrLabel;
	}
	
	//Method for returning run no label
	public JLabel getRunNoLabel() {
		return runNoLabel;
	}

	//Method for returning compile-time errors label
	public JLabel getCompileLabel() {
		return compileErrLabel;
	}

	//Method for returning compile no label
	public JLabel getCompileNoLabel() {
		return compileNoLabel;
	}

	//Method for returning score label
	public JLabel getScoreLabel() {
		return scoreLabel;
	}

	//Method for returning description texts
	public JPanel getDescriptionTexts() {
		return descriptionTextsPanels;
	}

	//Method for returning the values
	public JPanel getValues() {
		return valuesPanel;
	}

	//Method for returning homepage button
	public JButton getHomepageButton() {
		return homepageButton;
	}

	//Method for returning next button
	public JButton getNextButton() {
		return nextButton;
	}

	//Method for returning notes button
	public JButton getNotesButton() {
		return notesButton;
	}

	// Overrides the paintComponent method
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
	}

	@Override
	//Method for adding action listeners
	public void addActionListeners(ActionListener actionListener) {
		if (homepageButton.getActionListeners().length == 0) {
			homepageButton.addActionListener(actionListener);
			nextButton.addActionListener(actionListener);
			notesButton.addActionListener(actionListener);
		}
	}

} // End of SuccessView

