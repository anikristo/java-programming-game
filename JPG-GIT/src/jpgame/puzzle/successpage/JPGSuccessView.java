package jpgame.puzzle.successpage;

/**
 * JPGSuccessView interface to represent success views
 *
 * @author Burcu Canakci
 * @version 1.00 2014/4/20
 */

import java.awt.event.ActionListener;

import javax.swing.JButton;

import jpgame.puzzle.Puzzle;

public interface JPGSuccessView 
{
	public JButton  getHomepageButton();
	public JButton getNextButton();
	public JButton getNotesButton();
	public void    addActionListeners( ActionListener actionListener );
	public void updateView(Puzzle puzzle);
}
