package jpgame.puzzle;

/**
 * PuzzleView 	Main puzzle view.
 *						
 * @author Burcu Canakci
 * @version 1.00 2014/5/1
 */

//

import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.DecimalFormat;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.Timer;
import javax.swing.JSplitPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

import com.sun.glass.events.KeyEvent;

import jpgame.JPGColorLibrary;


public class PuzzleView extends JPanel implements JPGPuzzleView, DocumentListener
{
	
	//PROPERTIES

	private static final long serialVersionUID = -1734559227506187168L;
	private CodingField					codingField;
	private PuzzlePicture				picture;
	private Timer						timer;	
	private Puzzle						puzzle;
	private JButton 					seeJournal;
	private JButton 					seePartner;
	private JLabel 						time;
	private JButton 					goToHomePage;
	private JButton 					compileButton;
	private JButton 					submitButton;
	private JButton 					undoButton;
	private JButton 					redoButton;
	private JMenuItem					undoItem;
	private JMenuItem					redoItem;
	private UndoManager					manager;
	private JPopupMenu					popupMenu;
	
	// CONSTRUCTOR
	public PuzzleView( Puzzle puzzle) 
	{	
		this.puzzle = puzzle;		
		setLayout(new BorderLayout(0, 0));
		
		//Split Pane
		JSplitPane mainSplitPane = new JSplitPane();
		add(mainSplitPane, BorderLayout.CENTER);
		mainSplitPane.setBackground (JPGColorLibrary.BACKGROUND_DARK_BLUE);
		
		//Coding Field - Mandatory				
		codingField = new CodingField(puzzle);
		codingField.getDocument().addDocumentListener( this);
		JScrollPane codingFieldPane = new JScrollPane (codingField);
		
		//Panel consisting of PuzzleQuestion and Mandatory Labels and Buttons
		JPanel interPanel = new JPanel();
		interPanel.setLayout (new BorderLayout());
		interPanel.setBackground(JPGColorLibrary.BACKGROUND_DARK_BLUE);
		
		JLabel puzzleQuestionLabel = new JLabel ("Puzzle Question");
		puzzleQuestionLabel.setForeground( JPGColorLibrary.PURE_WHITE);
		puzzleQuestionLabel.setFont (new Font ("Monospaced", Font.BOLD, 24));
		
		JEditorPane puzzleQuestion = new JEditorPane();
		puzzleQuestion.setContentType( "text/html");
		puzzleQuestion.setText (puzzle.getPuzzleQuestion());
		puzzleQuestion.setFont (new Font("Arial Black", Font.PLAIN, 16));
		puzzleQuestion.setForeground (JPGColorLibrary.BACKGROUND_DARK_BLUE);
		
		JScrollPane puzzleQuestionPane = new JScrollPane (puzzleQuestion);
		puzzleQuestionPane.setPreferredSize(new Dimension (150,150));
		
		JPanel puzzleQuestionPanel = new JPanel (new BorderLayout());
		puzzleQuestionPanel.add (puzzleQuestionLabel, BorderLayout.NORTH);
		puzzleQuestionPanel.add (puzzleQuestionPane, BorderLayout.CENTER);
		puzzleQuestionPanel.setBackground (JPGColorLibrary.BACKGROUND_DARK_BLUE);

		//Panel for Interaction Buttons and Labels (Mandatory)
		JPanel interactionButtonsPanel = new JPanel();		
		interactionButtonsPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 30, 5));
		interactionButtonsPanel.setBackground(JPGColorLibrary.BACKGROUND_DARK_BLUE);
		
		seeJournal = new JButton("See Journal");
		seeJournal.setBackground(JPGColorLibrary.JPG_CRIMSON);
		seeJournal.setForeground(JPGColorLibrary.PURE_WHITE);
		seeJournal.setFont(new Font("Segoe UI", Font.BOLD | Font.BOLD, 20));

		seePartner = new JButton("See Partner");
		seePartner.setBackground(JPGColorLibrary.JPG_CRIMSON);
		seePartner.setForeground(JPGColorLibrary.PURE_WHITE);
		seePartner.setFont(new Font("Segoe UI", Font.BOLD | Font.BOLD, 20));

		JLabel timeIcon = new JLabel(new ImageIcon(getClass().getResource(
				"/jpgame/img/timeIcon.png")));
		time = new JLabel();
		time.setForeground(JPGColorLibrary.PURE_WHITE);
		time.setFont(new Font("Segoe UI", Font.BOLD | Font.BOLD, 20));
		time.setText("00 : 00");

		JPanel tmpTimePanel = new JPanel(new FlowLayout());
		tmpTimePanel.add(timeIcon);
		tmpTimePanel.add(time);
		tmpTimePanel.setBackground(JPGColorLibrary.BACKGROUND_DARK_BLUE);

		interactionButtonsPanel.add(seeJournal);
		interactionButtonsPanel.add(seePartner);
		interactionButtonsPanel.add(tmpTimePanel);
		
		interPanel.add (puzzleQuestionPanel, BorderLayout.NORTH);
		interPanel.add (interactionButtonsPanel, BorderLayout.CENTER);		
		//End of interPanel
		
		//Panel Consisting of CodingField and Interaction Panels		
		JPanel primaryPanel = new JPanel (new BorderLayout());
		primaryPanel.add (interPanel,  BorderLayout.NORTH);
		primaryPanel.add (codingFieldPane,  BorderLayout.CENTER);
		primaryPanel.setOpaque(false);
		
		//Buttons (mandatory) such as goToHomePage,...		
		goToHomePage = new JButton("Go to Homepage");
		goToHomePage.setBackground(JPGColorLibrary.JPG_CRIMSON);
		goToHomePage.setForeground(JPGColorLibrary.PURE_WHITE);
		goToHomePage.setFont(new Font("Segoe UI", Font.BOLD | Font.BOLD, 20));
		compileButton = new JButton("Compile");
		compileButton.setBackground(JPGColorLibrary.JPG_CRIMSON);
		compileButton.setForeground(JPGColorLibrary.PURE_WHITE);
		compileButton.setFont(new Font("Segoe UI", Font.BOLD | Font.BOLD, 20));
		compileButton.setEnabled( true);
		submitButton = new JButton("Submit");
		submitButton.setBackground(JPGColorLibrary.JPG_CRIMSON);
		submitButton.setForeground(JPGColorLibrary.PURE_WHITE);
		submitButton.setFont(new Font("Segoe UI", Font.BOLD | Font.BOLD, 20));
		submitButton.setEnabled(false);
		submitButton.addMouseListener( new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {}
			
			@Override
			public void mousePressed(MouseEvent e) {}
			
			@Override
			public void mouseExited(MouseEvent e) {
				submitButton.setBackground( JPGColorLibrary.JPG_CRIMSON);				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				if ( submitButton.isEnabled()) {
					submitButton
							.setBackground(JPGColorLibrary.BACKGROUND_DARK_GREEN);
				}
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {}
		});
		
		//Buttons to undo and redo
		undoButton = new JButton("Undo");
		undoButton.setBackground(JPGColorLibrary.PURE_WHITE);
		undoButton.setForeground(JPGColorLibrary.JPG_CRIMSON);
		undoButton.setFont(new Font("Segoe UI", Font.BOLD | Font.BOLD, 10));
		undoButton.setEnabled(false);
		
		redoButton = new JButton("Redo");
		redoButton.setBackground(JPGColorLibrary.PURE_WHITE);
		redoButton.setForeground(JPGColorLibrary.JPG_CRIMSON);
		redoButton.setFont(new Font("Segoe UI", Font.BOLD | Font.BOLD, 10));
		redoButton.setEnabled(false);
		

		JPanel homepageButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		homepageButtonPanel.add(goToHomePage);
		homepageButtonPanel.setBackground( JPGColorLibrary.BACKGROUND_DARK_BLUE);
		JPanel complSubmitPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT, 30, 5));
		complSubmitPanel.add(compileButton);
		complSubmitPanel.add(submitButton);
		complSubmitPanel.setBackground( JPGColorLibrary.BACKGROUND_DARK_BLUE);
		JPanel undoRedoPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT, 30, 5));
		undoRedoPanel.add(undoButton);
		undoRedoPanel.add(redoButton);
		undoRedoPanel.setBackground( JPGColorLibrary.BACKGROUND_DARK_BLUE);

		
		JPanel bottomButtonsPanel = new JPanel( new GridLayout( 1, 3));
		bottomButtonsPanel.add( homepageButtonPanel);
		bottomButtonsPanel.add( undoRedoPanel);
		bottomButtonsPanel.add( complSubmitPanel);
		
		
		add (bottomButtonsPanel, BorderLayout.SOUTH);
		//End of buttomButtons
		
		manager = new UndoManager();
		
		codingField.getDocument().addUndoableEditListener(
		        new MyUndoableEditListener());
		
		UndoActionListener undoAction = new UndoActionListener();
		RedoActionListener redoAction = new RedoActionListener();
		
		undoButton.addActionListener(undoAction); 
		redoButton.addActionListener(redoAction);
	    
	    popupMenu = new JPopupMenu();
	    popupMenu.setPopupSize(new Dimension (100, 80));
	    undoItem = new JMenuItem("Undo");
	    undoItem.addActionListener(undoAction);
	    popupMenu.add(undoItem);
	    popupMenu.addSeparator();
	    redoItem = new JMenuItem("Redo");
	    redoItem.addActionListener(redoAction);
	    popupMenu.add(redoItem);
	    
	    undoItem.setEnabled(false);
	    redoItem.setEnabled(false);
	    
	    undoItem.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				showPopUp(e);
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				showPopUp(e);
			}
			
			@Override
			public void mouseExited(MouseEvent e) {			
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
			}
			@Override
			public void mouseClicked(MouseEvent e) {}
			
			private void showPopUp(MouseEvent e) {
		        if (e.isPopupTrigger()) {
		            popupMenu.show(e.getComponent(),
		                       e.getX(), e.getY());
		        }
			}
	    }   );
	    
	    	redoItem.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				showPopUp(e);
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				showPopUp(e);
			}
			
			@Override
			public void mouseExited(MouseEvent e) {			
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
			}
			@Override
			public void mouseClicked(MouseEvent e) {}
			
			private void showPopUp(MouseEvent e) {
		        if (e.isPopupTrigger()) {
		            popupMenu.show(e.getComponent(),
		                       e.getX(), e.getY());
		        }
			}
	    }   );
		
	    codingField.setComponentPopupMenu(popupMenu);
	    
	    codingField.registerKeyboardAction(undoAction, KeyStroke.getKeyStroke(
	            KeyEvent.VK_Z, InputEvent.CTRL_MASK), JComponent.WHEN_FOCUSED);
	    codingField.registerKeyboardAction(redoAction, KeyStroke.getKeyStroke(
	            KeyEvent.VK_Y, InputEvent.CTRL_MASK), JComponent.WHEN_FOCUSED);
	    
		picture = new PuzzlePicture (puzzle);
		mainSplitPane.setRightComponent(primaryPanel);
		mainSplitPane.setLeftComponent(picture);
		
		mainSplitPane.setDividerLocation(java.awt.Toolkit.getDefaultToolkit().getScreenSize().width/2);		
		setPreferredSize (java.awt.Toolkit.getDefaultToolkit().getScreenSize());
	}

	// METHODS
	
	public CodingField getCodingField() 
	{
		return codingField;
	}

	public JButton getSeeJournalButton() 
	{
		return seeJournal;
	}

	public JButton getSeePartnerButton() 
	{
		return seePartner;
	}

	public JLabel getTimeLabel() 
	{
		return time;
	}

	public JButton getGoToHomePageButton() 
	{
		return goToHomePage;
	}


	public JButton getCompileButton() 
	{
		return compileButton;
	}

	public JButton getSubmitButton() 
	{
		return submitButton;
	}

	public void startTimer()
	{
		timer = new Timer(1000, new TimerActionListener());
		timer.start();
	}
	
	public void stopTimer()
	{
		timer.stop();
	}
	
	public void updateView (Puzzle puzzle)
	{
		this.puzzle = puzzle;
		
		// Repaint the time in decimal format.
		DecimalFormat df = new DecimalFormat("00");
		String seconds = df.format(puzzle.getTime() % 60);
		String minutes = df.format(puzzle.getTime() / 60);
		time.setText(minutes + " : " + seconds);
		
		if (puzzle.isCompiled())
			submitButton.setEnabled(true);
		else
			submitButton.setEnabled( false);
		
		//Picture
		picture.updateView(puzzle);
	}
	
	//PuzzleView's timer's action listener.
	private class TimerActionListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			puzzle.updateTime();
			updateView(puzzle);
		}			
	}
	
	private class MyUndoableEditListener implements UndoableEditListener
	{
		@Override
		public void undoableEditHappened(UndoableEditEvent e) {
            manager.addEdit(e.getEdit());
            updateButtons();
          }
	}
	
	private class UndoActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			
		        try {
		          manager.undo();
		        } catch (CannotUndoException cre) {
		        	Toolkit.getDefaultToolkit().beep();
		        }
			updateButtons();
	      }
	}
	
	private class RedoActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			
		        try {
		          manager.redo();
		        } catch (CannotRedoException cre) {
		        	Toolkit.getDefaultToolkit().beep();
		        }
		        updateButtons();
			}
	}
	

	@Override
	public void addActionListeners(ActionListener actionListener) 
	{
		if (seeJournal.getActionListeners().length == 0)
		{
			seeJournal.addActionListener(actionListener);
			seePartner.addActionListener(actionListener);
			goToHomePage.addActionListener(actionListener);
			compileButton.addActionListener(actionListener);
			submitButton.addActionListener(actionListener);	
		}	
	}
	
	@Override
	public void changedUpdate(DocumentEvent e) {
		 puzzle.setCompiled( false);
		 String temp = codingField.getText().trim();
			if (temp.length() != 0)
				compileButton.setEnabled(true);
			else
				compileButton.setEnabled(false);
	}

	@Override
	public void insertUpdate(DocumentEvent e) {
		puzzle.setCompiled( false);
		String temp = codingField.getText().trim();
		if (temp.length() != 0)
			compileButton.setEnabled(true);
		else
			compileButton.setEnabled(false);
	}

	@Override
	public void removeUpdate(DocumentEvent e) {
		puzzle.setCompiled( false);
		String temp = codingField.getText().trim();
		if (temp.length() != 0)
			compileButton.setEnabled(true);
		else
			compileButton.setEnabled(false);
	}
	
	public void updateButtons() {
		undoItem.setEnabled (manager.canUndo());
		redoItem.setEnabled (manager.canRedo());
	    undoButton.setEnabled(manager.canUndo());
	    redoButton.setEnabled(manager.canRedo());
	  }

}
