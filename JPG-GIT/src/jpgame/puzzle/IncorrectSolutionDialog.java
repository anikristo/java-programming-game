/**
 * @author Ani Kristo
 * @version 1.0
 * 
 * 12 / 05 / 2014
 * 
 * DESCRIPTION: This JDialog will communicate the fact that the correctly compiled code did not generate a correct output. This can be
 * 				either due to a runtime error or simply wrong solution. 
 */
package jpgame.puzzle;

import java.awt.BorderLayout;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import jpgame.JPGColorLibrary;
import jpgame.JPGComponent;

public class IncorrectSolutionDialog extends JDialog implements JPGComponent,
		ActionListener {

	//Serial NO
	private static final long serialVersionUID = -5504809928479907235L;

	// PROPERTIES
	private JLabel msg;
	private JPanel msgPanel;
	private JPanel buttonPane;
	private JButton okButton;

	// CONSTRUCTOR
	public IncorrectSolutionDialog() {
		msg = new JLabel("The output was incorrect!");
		msg.setFont(new Font("Segoe UI", Font.ITALIC, 18));
		msg.setForeground(JPGColorLibrary.JPG_CRIMSON);

		msgPanel = new JPanel();
		msgPanel.add(msg);

		buttonPane = new JPanel(new FlowLayout(FlowLayout.CENTER));
		okButton = new JButton("OK");
		okButton.setBackground( JPGColorLibrary.JPG_CRIMSON);
		okButton.setForeground( JPGColorLibrary.PURE_WHITE);
		okButton.setFont( new Font( "Segoe UI", Font.BOLD, 16));
		okButton.addActionListener(this);
		buttonPane.add(okButton);

		setLayout(new BorderLayout());
		add(msgPanel, BorderLayout.CENTER);
		add(buttonPane, BorderLayout.SOUTH);

		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setTitle("Incorrect Solution");
		pack();

		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit()
				.getScreenSize());
		setLocation((screenSize.width - getWidth()) / 2,
				(screenSize.height - getHeight()) / 2);
		setVisible(true);
	}

	// METHODS
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == okButton) {
			dispose();
		}
	}

}//End of IncorrectSolutionDialog