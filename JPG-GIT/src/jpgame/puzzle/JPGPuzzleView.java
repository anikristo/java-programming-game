package jpgame.puzzle;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;

import jpgame.JPGComponent;

/**
 * JPGPuzzleView interface to represent views added to the puzzle
 *
 * @author Burcu Canakci
 * @version 1.00 2014/4/20
 */

public interface JPGPuzzleView extends JPGComponent
{
	public void updateView(Puzzle puzzle);
	public CodingField getCodingField();
	public JButton getSeeJournalButton(); 
	public JButton getSeePartnerButton(); 
	public JLabel  getTimeLabel();
	public JButton getGoToHomePageButton();
	public JButton getCompileButton(); 
	public JButton getSubmitButton();
	public void    addActionListeners( ActionListener actionListener );
	public void startTimer();
	public void stopTimer();
}
