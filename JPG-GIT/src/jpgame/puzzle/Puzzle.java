/**
 * @author Ani Kristo
 * @version 1.0
 * 
 * STARTED: 19 / 04 / 2014
 * COMPLETED: 12 / 05 / 2014
 * 
 * DESCRIPTION: The Puzzle class is a representation for one puzzle in the game. It is a JPGComponent and a Serializable,
 * 				but abstract class. In order to create an instance of a subclass of Puzzle object, there are three possible
 * 				alternatives: 
 * 					
 * 					+ There is a constructor that takes the puzzle's question and a collection of views and assigns all
 * 				the other unknown variables' values to the default values as described in the respective constants. 
 * 				IMPORTANT: The usage of this constructor is discouraged, therefore it is flagged as @Deprecated.
 * 
 * 					+ Another constructor takes a puzzle setup object { @see BasicPuzzleSetup} and assigns the variables 
 * 				to their respective values that are held in the setup object. For the values that are missing from the 
 * 				setup object, the default constants are used. 
 * 
 * 					+ Lastly, the third constructor takes an AdvancedPuzzleSetup object { @see AdvancedPuzzleSetup} which 
 * 				provides extra information necessary for setting up the values of the variables in the Puzzle class. 
 * 				The usage of the AdvancedPuzzleSetup is strongly suggested as it avoids numerous issues with null objects. 
 * 
 * 
 * 				The main process that is followed by this class is described as below: 
 * 					1.usersSoln variable holds a string that contains the text from the provided textfield where the user
 * 				will be able to write his solution to the particular given task.
 * 					2. When the player requests the compilation of the code, the content of the template file { @see template}
 * 				 is merged with the provided user's solution and written into an Input.java file. ( The filename is determined
 * 				by the setup).
 * 					3. JPGCompiler class's compile() method is called and it compiles the Input.java file, thus creating an
 * 				Input.class file.
 * 					4. If the Input.class file is available and the user requests the solution to run, the puzzle class's
 * 				checkSolution() method should be called. It runs both the Input.class and KeySoln.class { @see keySoln}
 * 				with the same inputs, and generates two output files for the respective sources. 
 * 					5. Finally, the two output files are read and the checkSolution() determines whether their content is 
 * 				the same, hence marking the puzzle as solved or not. { @see isSolved}
 * 
 * 				Other methods include:
 * 				+ Accessor and mutator methods for the variables that require such methods.
 * 				+ getPrevious - Hints / Clues - () which returns the collected Hints / Clues
 * 				+ getAll - Hints / Clues - () which provides the entire collection of Hints / Clues for this puzzle.
 * 				+ getNext - Hint / Clue - () which provides the next Hint / Clue to be received from the player.
 *				# updateScores() (abstract) - responsible for calculating the scores by means of different formulas, 
 *					as the game developer decides
 *				+ updateTime() - keeps the time variable updated by counting the seconds passed for solving the puzzle. 
 *				etc.
 *
 *				NOTE: 	This class holds a reference to the Storyline object that it belongs to and an AdditionalPage
 *						object from the Journal. { @see Journal, @see AdditionalPage}
 * 
 * 
 */

package jpgame.puzzle;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.swing.ImageIcon;

import jpgame.JPGComponent;
import jpgame.partner.hints.Hint;
import jpgame.puzzle.clues.Clue;
import jpgame.puzzle.setup.AdvancedPuzzleSetup;
import jpgame.puzzle.setup.BasicPuzzleSetup;
import jpgame.puzzle.successpage.JPGSuccessView;
import jpgame.puzzle.successpage.SuccessView;
import jpgame.storyline.Storyline;

public abstract class Puzzle implements JPGComponent, Serializable {

	// CONSTANTS
	// -------------------------------------------------------------------------------------------------------------------------------

	/**
	 * Generated serial version UID because of the implementation of
	 * {@link Serializable} interface
	 */
	private static final long serialVersionUID = 4456385982730732977L;

	public final ImageIcon DEFAULT_BACKGROUND = new ImageIcon( getClass().getResource(
			"/jpgame/img/DefaultBackground.jpg"));
	public static final int DEFAULT_DIFFICULTY_COEFFICIENT = 1;

	// for run() method
	public static final int NOT_COMPILED_YET = -1;
	public static final int RUNNING_SUCCESSFUL = 1;
	public static final int CANNOT_CALL_COMMAND = -2; // exception caught

	// for writeInputFile() method
	private static final int CANNOT_CLOSE_READER = -3;
	private static final int CANNOT_CREATE_OUTPUTSTREAM = -4;
	private static final int CANNOT_CLOSE_OUTPUT = -5;

	// for runKeySoln() method
	private static final int KEYSOLN_RUN_SUCCESS = 2;
	private static final int KEYSOLN_RUN_NOT_SUCCESS = -6;

	// VARIABLES
	// -----------------------------------------------------------------------------------------------------------------------------
	private String storylineName;
	private String name;
	private ImageIcon backgroundPic;

	private String puzzleQuestion;
	private String usersSoln;
	private StringBuffer complErrMsg; // Error log from compilation diagnostics

	private Vector<Clue> clues;
	private Vector<Hint> hints;

	private ArrayList<String> userCodes = new ArrayList<String>();
	private double difficultyCoeff;

	private int score;
	private int time; // seconds
	private int compileErrNr;

	private int compilationNr; // number of times the code is compiled
	private int submissionNr; // number of time the code is submitted

	/*
	 * The previously generated file to include the code fragment from the user
	 * and to be compiled
	 */
	private File inputFile;

	/*
	 * The key solution for the problem. It is used to compare the outputs
	 * provided they get the same input.
	 * 
	 * NOTE! Should be contained as a '.class' file,
	 */
	private File keySoln;

	/*
	 * Writes the error log that is generated from the compilation of the code.
	 */
	private File complErrorLog;

	/*
	 * Contains a predefined template of a Class that is only lacking the code
	 * fragment that the player will need to write.
	 */
	private File template;

	// files that are going to be read / generated by the application

	transient private JPGPuzzleView  	puzzleView;
	transient private JPGSuccessView	successView;
	
	private boolean isCompiled;
	private boolean isErrorLogCreated;
	private boolean isSubmitted;
	private boolean isSolved; // true only if correct solution
	private boolean isInPortfolio;

	private String additionalNotes;


	// CONSTRUCTORS
	// --------------------------------------------------------------------------------------------------------------------------

	protected Puzzle(BasicPuzzleSetup setup, Storyline storyline) {

		name = setup.getPuzzleName();
		storylineName = setup.getStorylineName();
		backgroundPic = DEFAULT_BACKGROUND;
		puzzleQuestion = setup.getPuzzleQuestion();

		setClues(new Vector<Clue>());

		setHints(new Vector<Hint>());

		usersSoln = null;
		complErrMsg = null;

		difficultyCoeff = DEFAULT_DIFFICULTY_COEFFICIENT;
		score = 0;
		
		compileErrNr = 0;
		compilationNr = 0;
		submissionNr = 0;

		inputFile = setup.getInputFile();
		keySoln = setup.getKeySolutionFile();
		complErrorLog = setup.getErrorLogFile();
		template = setup.getTemplateFile();
		
		createViews( storyline);

		isCompiled = false;
		isErrorLogCreated = false;
		isSubmitted = false;
		isSolved = false;
		isInPortfolio = false;
		
		additionalNotes = "";
	}

	protected Puzzle(AdvancedPuzzleSetup setup, Storyline storyline) {
		
		storylineName = setup.getStorylineName();
		name = setup.getPuzzleName();
		backgroundPic = setup.getBackgroundPicture();
		puzzleQuestion = setup.getPuzzleQuestion();

		setClues(new Vector<Clue>());

		setHints(new Vector<Hint>());

		usersSoln = setup.getPreliminaryCode();
		complErrMsg = null;

		difficultyCoeff = setup.getDifficultyCoefficient();
		score = 0;

		compileErrNr = 0;

		compilationNr = 0;
		submissionNr = 0;

		inputFile = setup.getInputFile();
		keySoln = setup.getKeySolutionFile();
		complErrorLog = setup.getErrorLogFile();
		template = setup.getTemplateFile();
		
		createViews( storyline);
		
		isCompiled = false;
		isErrorLogCreated = false;
		isSubmitted = false;
		isSolved = false;
		
		isInPortfolio = false;
		
		additionalNotes = "";

	}

	// METHODS

	protected void notifyViews() {

		if (successView != null)
			successView.updateView(this);

		if (puzzleView != null)
			puzzleView.updateView(this);
	}

	public String getPuzzleName() {
		return name;
	}
	
	public String getStorylineName() {
		return storylineName;
	}

	public ImageIcon getPuzzleBackground() {
		return backgroundPic;
	}

	public String getPuzzleQuestion() {
		return puzzleQuestion;
	}

	public int getScore() {
		return score;
	}

	protected void setScore(int score) {
		this.score = score;
		notifyViews();
	}

	public int getTime() {
		return time;
	}

	void updateTime() {
		time++;
		notifyViews();
	}

	/*
	 * This method will serve to calculate the score gained by the user, while
	 * considering the time taken, difficulty coefficient, number of errors made
	 * etc.
	 * 
	 * REMINDER: notifyViews();
	 */
	protected abstract void updateScore();

	public int getNumberOfCompilationErrors() {
		return compileErrNr;
	}

	public int getNumberOfRuntimeErrors() {
		return submissionNr - 1 > 0 ? submissionNr - 1 : 0;
	}

	public int getNumberOfCompilations() {
		return compilationNr - submissionNr;
	}

	public int getNumberOfSubmissions() {
		return submissionNr;
	}

	public double getDifficultyCoefficient() {
		return difficultyCoeff;
	}

	public String getUsersSolution() {
		return usersSoln;
	}

	public void setUsersSolution(String usersSoln) {
		this.usersSoln = usersSoln;
		notifyViews();
	}
	
//	protected void setPreliminarySolution( String preCode){
//		this.setUsersSolution(preCode);
//		notifyViews();
//	}

	public boolean isCompiled() {
		return isCompiled;
	}
	
	public void setCompiled( boolean isCompiled){
		this.isCompiled = isCompiled;
	}

	public boolean isSolved() {
		return isSolved;
	}
	
	public void switchPortfolioState()
	{
		isInPortfolio = !isInPortfolio;
		notifyViews();
	}
	
	public boolean isInPortfolio()
	{
		return isInPortfolio;
	}

	protected void setSolved(boolean value) {
		isSolved = value;
		notifyViews();
	}

	public boolean isSubmitted() {
		return isSubmitted;
	}

	public String getComplErrorMsg() {
		if (complErrMsg != null)
			return complErrMsg.toString();
		else
			return "No Compilation Errors!";
	}

	public ArrayList<Hint> getPreviousHints() {
		ArrayList<Hint> previousHints = new ArrayList<Hint>();
		for (int i = 0; i < getAllHints().size(); i++) {
			if (getAllHints().get(i).isReceived())
				previousHints.add(getAllHints().get(i));
		}
		return previousHints;
	}

	public Hint getNextHint() {
		for (int i = 0; i < getAllHints().size(); i++) {
			if (getAllHints().get(i).canDisplay() && !getAllHints().get(i).isReceived()) {
				return getAllHints().get(i);
			}

		}
		return null;
	}

	public ArrayList<Clue> getPreviousClues() {
		ArrayList<Clue> previousClues = new ArrayList<Clue>();
		for (int i = 0; i < getAllClues().size(); i++) {
			if (getAllClues().get(i).getSelected())
				previousClues.add(getAllClues().get(i));
		}
		return previousClues;
	}

	public Clue getNextClue() {
		for (int i = getAllClues().size() - 1; i >= 0; i--) {
			if (getAllClues().get(i).canDisplay())
				return getAllClues().get(i);
		}
		return null;
	}


	/*
	 * This method should determine whether the user's entered code does what is
	 * expected or not. It should be able to run the Key Solution file and the
	 * generated Input file with the same arguments and test for different
	 * values. In case the test fails, it should return the same boolean value
	 * as it will be assigned to the property 'isSolved'. Otherwise, if the
	 * output will be the same for every testing value, the puzzle should be
	 * considered as correctly solved.
	 * 
	 * REMINDER: Should also notifyViews()
	 */
	public abstract boolean checkSolution();

	/*
	 * The compile() method is responsible for creating a new thread which will
	 * serve for compiling the player's given code.
	 * 
	 * Initially it calls writeInputFile() method.
	 * 
	 * After that, it calls the readErrorLogFile() method.
	 */
	public void compile() {
		// create the file with the template and the user's solution
		writeInputFile();

		// compile
		ExecutorService exec = Executors.newCachedThreadPool();
		exec.execute( new CompilationTask());
		exec.shutdown();
		
		try {
			exec.awaitTermination( 10, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			System.out.println("Cannot execute Compilation Task!");
			e.printStackTrace();
		}
		
//		Thread compilationThread = new Thread(new CompilationTask(),
//				"compilationThread");
//		compilationThread.start();
//		compilationThread.setPriority(Thread.MAX_PRIORITY);
//
//		try {
//			compilationThread.join();
//		} catch (InterruptedException e) {
//		}

		readErrorLogFile();
	}
	
	private class CompilationTask implements Runnable {

		/*
		 * The run() method will call Java Compiler and turn the source code of
		 * the Input file (containing the user's code) into Java Bytecode.
		 * 
		 * It relies on the JPCompiler class's compile method to execute the
		 * process. It assumes that the Input.java file is already written, and
		 * it compiles it.
		 */
		@Override
		public void run() {

			int result = 99; // random
			
			
			// compiling
			try {
				result = JPGCompiler.compile(inputFile, complErrorLog);
			} catch (Exception e1) {
				System.out.println("Cannot compile!");
				e1.printStackTrace();
			}

			compilationNr++;

			if (result == JPGCompiler.SUCCESSFUL_COMPILATION) {
				isCompiled = true;
				isErrorLogCreated = false;
			} else if (result == JPGCompiler.UNSUCCESSFUL_COMPILATION) {
				isCompiled = false;
				isErrorLogCreated = true;

			} else
				isErrorLogCreated = false;
		}

	}


	/*
	 * readErrorLogFile() method's purpose is to read the content of the Error
	 * Log file generated when the JPGCompiler.compile() method was called. It
	 * assigns the error messages to the variable responsible for that, and
	 * determines the number of compile time errors.
	 * 
	 * In case the compilation is successful, it assigns "No compilation errors"
	 * to the variable holding the respective information.
	 */

	private int readErrorLogFile() {

		// empty the Error messages string
		complErrMsg = new StringBuffer();

		if (isErrorLogCreated) {

			BufferedReader scan = null;
			try {
				FileReader fileReader = new FileReader(complErrorLog);
				// read error file
				scan = new BufferedReader(fileReader);
				StringBuffer contentOfErrorLog = new StringBuffer();
				String tmpText;

				while ((tmpText = scan.readLine()) != null) {
					contentOfErrorLog.append(tmpText).append(
							System.getProperty("line.separator"));
				}

				try {
					if (scan != null) {
						scan.close();
					} if( fileReader != null){
						fileReader.close();
					}
				} catch (IOException e) {
					System.out.println("Cannot close reader!");
					e.printStackTrace();
					return CANNOT_CLOSE_READER; // -3
				}

				// set the compile error messages
				complErrMsg = contentOfErrorLog;

				// set number of compilation errors
				int nrOfErrors;
				String lineShowingErrorNr = "";

				// find the last line of the error log file
				for (int i = contentOfErrorLog.length() - 1; i >= 0; i--) {
					if (contentOfErrorLog.charAt(i) == '\n') {
						lineShowingErrorNr = contentOfErrorLog.substring(i + 1);

						if (lineShowingErrorNr.indexOf("error") != -1)
							break;
					}
				}

				// create a string out of the number of compilation displayed in
				// the error log
				lineShowingErrorNr = lineShowingErrorNr.substring(0,
						lineShowingErrorNr.indexOf("error") - 1);

				// parse the integer
				nrOfErrors = Integer.parseInt(lineShowingErrorNr);

				// add the number of errors to the total count
				compileErrNr += nrOfErrors;

			} catch (Exception e) {
				System.out.println("Exception caught while trying to read error log!");
				e.printStackTrace();
				return JPGCompiler.EXCEPTION_CAUGHT; // - 1
			}
		}

		else
			complErrMsg.append("No Compilation Errors!");

		notifyViews();
		return 0; // everything is ok
	}

	/*
	 * This method will take the content of the template File, add the player's
	 * fragment of code and eventually build a completed Java Class.
	 * 
	 * By default, this method assumes that the user is only writing a static
	 * method, and adds this method to the 'Input.java' Class. The functionality
	 * of the given fragment of code should be tested in the main method, which
	 * is already provided in the template file.
	 * 
	 * In case of a different testing scheme or for any other code format for
	 * the player, this method should be overridden.
	 */
	protected int writeInputFile() {

		StringBuffer preparedFileContent = new StringBuffer(); // template's
																// content
		StringBuffer totalFileContent = new StringBuffer();

		// read the input file
		BufferedReader scan;
		String tmpText;

		try {
			scan = new BufferedReader(new FileReader(template));
			while ((tmpText = scan.readLine()) != null) {
				preparedFileContent.append(tmpText).append(
						System.getProperty("line.separator"));
			}
			scan.close();

		} catch (Exception e) {
			e.printStackTrace();
			return CANNOT_CLOSE_READER; // -3
		}

		// create the temporary file to be compiled which is, by default, called
		// Input.java
		totalFileContent.append(preparedFileContent);
		totalFileContent.append(System.getProperty("line.separator"))
				.append(usersSoln)
				.append(System.getProperty("line.separator") + "}");

		PrintWriter o = null;
		try {
			o = new PrintWriter(inputFile);
		} catch (Exception e) {
			e.printStackTrace();
			return CANNOT_CREATE_OUTPUTSTREAM; // -4
		}

		try {
			o.append(totalFileContent.toString());
			o.flush();
			o.close();
		} catch (Exception e1) {
			e1.printStackTrace();
			return CANNOT_CLOSE_OUTPUT; // -5
		}
		return JPGCompiler.SUCCESSFUL_COMPILATION; // 1
	}

	/*
	 * This method will call the Command Prompt and execute the 'java' command,
	 * which calls the Java Interpreter and executes the code in the file.
	 */
	protected int runInputFile() {

		if (isCompiled) {

			// creating command to be passed to Command Prompt
			String inputFileName = inputFile.getName();
			inputFileName = inputFileName.substring(0,
					inputFileName.length() - 5); // removing the extension
													// '.java'
			String command = "cmd /c java " + inputFileName;

			// calling java's interpreter from Command Prompt
			try {
				Process executingProcess = Runtime.getRuntime().exec(command);
				executingProcess.waitFor( 15, TimeUnit.SECONDS);

				isSubmitted = true;
				submissionNr++;

				notifyViews();

				return RUNNING_SUCCESSFUL; // 1

			} catch (Exception e) {
				e.printStackTrace();
				return CANNOT_CALL_COMMAND; // -2
			}

		} else
			return NOT_COMPILED_YET; // -1
	}

	/*
	 * This method will serve for calling Java's Interpreter for the Key
	 * Solution file. It resembles to the runInputFile() method defined earlier,
	 * but it simply executes a different file. This assumes that the solution
	 * file is a .class file.
	 */
	protected int runKeySoln() {

		// creating command to be passed to Command Prompt
		String keySolnFileName = keySoln.getName();

		// run the keySoln
		keySolnFileName = keySolnFileName.substring(0,
				keySolnFileName.length() - 6); // removing the extension .class

		String command = "cmd /c java " + keySolnFileName;

		// calling java's interpreter from Command Prompt
		try {
			Process executingProcess = Runtime.getRuntime().exec(command);
			executingProcess.waitFor( 15, TimeUnit.SECONDS); // waits until the process is finished or time is out
			
			return KEYSOLN_RUN_SUCCESS; // 2
			
		} catch (Exception e) {
			e.printStackTrace();
			return KEYSOLN_RUN_NOT_SUCCESS; // -6
		}

	}

	public ArrayList<String> getUserCodes() {
		return userCodes;
	}

	public void setUserCodes(ArrayList<String> userCodes) {
		this.userCodes = userCodes;
		notifyViews();
	}

	public JPGSuccessView getSuccessView() 
	{
		return successView;
	}

	public void setSuccessView(JPGSuccessView successView) 
	{
		this.successView = successView;
		notifyViews();
	}

	public JPGPuzzleView getPuzzleView() 
	{
		return puzzleView;
	}

	public void setPuzzleView(JPGPuzzleView puzzleView) 
	{
		this.puzzleView = puzzleView;
		notifyViews();
	}

	public void createViews(Storyline s) 
	{
		puzzleView = new PuzzleView(this);
		successView = new SuccessView(this);		
	}

	public Vector<Hint> getAllHints() {
		if (hints != null) {
			return hints;
		}
		return new Vector<Hint>();
	}

	protected void setHints(Vector<Hint> hints) {
		this.hints = hints;
		notifyViews();
	}

	protected Vector<Clue> getAllClues() {
		if ( clues!= null) {
			return clues;
		}
		return new Vector<Clue>();
	}

	protected void setClues(Vector<Clue> clues) {
		this.clues = clues;
		notifyViews();
	}

	public String getAdditionalNotes() {
		return additionalNotes;
	}

	public void setAdditionalNotes(String additionalNotes) {
		this.additionalNotes = additionalNotes;
		notifyViews();
	}
}