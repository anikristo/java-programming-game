package jpgame.puzzle;

/**
 * Locatable an interface to make the implemented classes locatable(i.e. with coordinates).
 *
 * @author Burcu Canakci
 * @version 1.00 2014/4/20
 */

import java.awt.Point;

public interface Locatable
{
	public void setLocation(Point p); // sets the location

	public Point getLocation(); // returns the location
}
