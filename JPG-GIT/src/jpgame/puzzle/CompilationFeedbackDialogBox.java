/**
 * @author Ani Kristo
 * @version 1.0
 * 
 * 12 / 05 / 2014
 * 
 * DESCRIPTION: This dialog box will display the error messages obtained from an unsuccessful compilation. In case the compilaton was
 * 				successful, it will display a message informing that. 
 */
package jpgame.puzzle;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import jpgame.JPGColorLibrary;
import jpgame.JPGComponent;

public class CompilationFeedbackDialogBox extends JDialog implements
		JPGComponent, ActionListener {

	//Serial NO
	private static final long serialVersionUID = 461678338349135431L;
	//PROPERTIES
	private JTextPane content;
	private JPanel buttonPane;
	private JButton closeButton;
	
	private static boolean compilerExists = true; 

	//Constructor
	public CompilationFeedbackDialogBox(Puzzle puzzle) {

		if ( compilerExists) {
			content = new JTextPane();
			content.setText(puzzle.getComplErrorMsg());
			content.setFont(new Font("Segoe UI", Font.ITALIC, 16));
			content.setEditable(false);
			content.setOpaque(false);
			JScrollPane scrollPane = new JScrollPane(content);
			closeButton = new JButton("Close");
			closeButton.setFont(new Font("Segoe UI", Font.BOLD, 16));
			closeButton.setBackground(JPGColorLibrary.JPG_CRIMSON);
			closeButton.setForeground(JPGColorLibrary.PURE_WHITE);
			closeButton.addActionListener(this);
			buttonPane = new JPanel(new FlowLayout(FlowLayout.RIGHT));
			buttonPane.add(closeButton);
			setLayout(new BorderLayout());
			add(scrollPane);
			add(buttonPane, BorderLayout.SOUTH);
			setTitle("Compilation Feedback");
			// Get screen size
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			Dimension windowSize = new Dimension(screenSize.width / 3,
					screenSize.height / 3 * 2);
			setBounds(screenSize.width / 3, screenSize.height / 5,
					windowSize.width, windowSize.height);
			setVisible(true);
		}

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == closeButton)
			dispose();
	}
	
	public static void compilerDoesNotExist(){
		compilerExists = false;
	}

}//End of CompilationFeedbackDialogBox
