/**
 * @author Ani Kristo
 * @version 1.0

 * COMPLETED: 26 / 04 /2014
 * 
 * This class provides the compiler tool for the Java Programming Game. It will compile the class passed as a parameter in the 'compile' method, 
 * and save the log of compile time errors in the provided file. 
 * 
 */
package jpgame.puzzle;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

import jpgame.JPGColorLibrary;

public final class JPGCompiler {

	// PROPERTIES
	private static JavaCompiler compiler;

	// CONSTANTS
	public static final int SUCCESSFUL_COMPILATION = 1;
	public static final int UNSUCCESSFUL_COMPILATION = 0;
	public static final int EXCEPTION_CAUGHT = -1;

	private static final File JDK_PATH_CONTAINER = new File("jdk.path");

	private static final String JDK_DEFAULT_PATH = "C:\\Program Files\\Java\\jdk"
			+ System.getProperty("java.version");

	// METHODS
	/*
	 * @param File to be compiled, file to save the error log
	 * 
	 * @return An integer to represent whether the process was successful or
	 * not. In addition, provides a different return value for exceptions.
	 * 
	 * This method compiles the file passed as a parameter and saves the error
	 * log in the destination error-log file.
	 */
	public static int compile(File fileToCompile) throws IOException {

		String javaHomePath = System.getProperty("java.home");

		if (!JDK_PATH_CONTAINER.exists()) {
			System.setProperty("java.home", JDK_DEFAULT_PATH);
		}

		else {
			String java_home = readJDKPath(JDK_PATH_CONTAINER);
			System.setProperty("java.home", java_home);
		}

		compiler = ToolProvider.getSystemJavaCompiler();

		if (compiler == null) {
			displayErrorMessage();
			return EXCEPTION_CAUGHT;
		}

		int compilationResult;

		// Compile the file
		try {
			compilationResult = compiler.run(null, null, null,
					fileToCompile.getPath());

			if (compilationResult == 0) {
				return SUCCESSFUL_COMPILATION;
			} else {
				return UNSUCCESSFUL_COMPILATION;
			}

		} catch (Exception e) {
			System.out.println("Cannot Compile!");
			e.printStackTrace();
			return EXCEPTION_CAUGHT;
		} finally {
			System.setProperty("java.home", javaHomePath);
		}
	}

	/*
	 * This is a method overloading in case it is required to have a error log
	 * for all the occurred errors during execution
	 */
	public static int compile(File fileToCompile, File destinationErrorLog)
			throws IOException {
		String javaHomePath = System.getProperty("java.home");

		if (!JDK_PATH_CONTAINER.exists()) {
			System.setProperty("java.home", JDK_DEFAULT_PATH);
		}

		else {
			String java_home = readJDKPath(JDK_PATH_CONTAINER);
			System.setProperty("java.home", java_home);
		}

		compiler = ToolProvider.getSystemJavaCompiler();

		if (compiler == null) {
			displayErrorMessage();
			return EXCEPTION_CAUGHT;
		}

		int compilationResult;

		if (destinationErrorLog != null && fileToCompile != null) {

			// Compile the file
			try {
				FileOutputStream outStream = new FileOutputStream(destinationErrorLog);
				compilationResult = compiler.run(System.in, System.out,
						outStream,
						fileToCompile.getPath());
				outStream.close();
				if (compilationResult == 0) {
					return SUCCESSFUL_COMPILATION;
				} else {
					return UNSUCCESSFUL_COMPILATION;
				}

			} catch (Exception e) {
				System.out.println("Cannot compile!");
				e.printStackTrace();
				return EXCEPTION_CAUGHT;
			} finally {
				System.setProperty("java.home", javaHomePath);
			}
		} else {
			System.setProperty("java.home", javaHomePath);
			return EXCEPTION_CAUGHT;
		}
	}

	/*
	 * The error message dialog which notifies that the JDK path is not
	 * configured as default and requires the new path
	 */
	private static void displayErrorMessage() {
		CompilationFeedbackDialogBox.compilerDoesNotExist();
		
		final JDialog errorDialog = new JDialog();
		errorDialog.setTitle("ERROR: Complier not available!");
		errorDialog.setLayout(new BorderLayout());

		JLabel labelMessage = new JLabel(
				"<html>Application cannot retrieve System's Compiler. <br>Your JDK directory is not in \"" + readJDKPath( JDK_PATH_CONTAINER) + "\"<html>");
		labelMessage.setFont(new Font("Segoe UI", Font.CENTER_BASELINE, 18));

		JLabel labelEnter = new JLabel(
				"<html>Please enter the JDK directory below: <br> <html>");
		labelEnter.setFont(new Font("Segoe UI", Font.ITALIC, 14));

		final JTextField pathTF = new JTextField();
		pathTF.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		pathTF.setForeground(JPGColorLibrary.JPG_CRIMSON);
		pathTF.setColumns(40);

		JPanel pathPanel = new JPanel(new GridLayout(2, 1));
		pathPanel.add(labelEnter);
		pathPanel.add(pathTF);

		JPanel messagePanel = new JPanel();
		messagePanel.add(labelMessage, "North");
		messagePanel.add(pathPanel, "South");

		JButton okButton = new JButton("Set Directory");
		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.setProperty("java.home", pathTF.getText() != null ? pathTF.getText() : JDK_DEFAULT_PATH);

				try {
					PrintWriter o = new PrintWriter(JDK_PATH_CONTAINER);
					o.append(pathTF.getText());
					o.close();
				} catch (Exception exc) {
					exc.printStackTrace();
					return;
				}

				errorDialog.dispose();
			}
		});
		
		JLabel note = new JLabel( "NOTE: You need to restart the program after you set the directory!");
		note.setFont( new Font( "Segoe UI", Font.ITALIC, 14));
		note.setForeground( JPGColorLibrary.JPG_CRIMSON);
		
		JPanel notePanel = new JPanel( new FlowLayout());
		notePanel.add( note);

		JPanel buttonPanel = new JPanel(new FlowLayout());
		buttonPanel.add(okButton);
		
		JPanel bottomPanel = new JPanel( new GridLayout(2, 1));
		bottomPanel.add( buttonPanel);
		bottomPanel.add( notePanel);

		errorDialog.add(messagePanel, "Center");
		errorDialog.add(bottomPanel, "South");

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

		errorDialog.setBounds((screenSize.width - 600) / 2,
				(screenSize.height - 220) / 2, 600, 220); // centered
		errorDialog.setVisible(true);

	}

	private static String readJDKPath(File file) {

		if (file.exists()) {
			try {
				BufferedReader reader = new BufferedReader(new FileReader(file));
				String path = reader.readLine();
				reader.close();
				return path != null && path.trim() != "" ? path : JDK_DEFAULT_PATH;
			} catch (Exception e) {
				System.out.println("Cannot read JDK path file!");
				e.printStackTrace();
				return JDK_DEFAULT_PATH;
			}
		} else
			return JDK_DEFAULT_PATH;
	}
}
