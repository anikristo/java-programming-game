/**
 * @author Ani Kristo
 * @version 1.0
 * 
 * COMPLETED: 26 / 04 / 2014
 * 
 * DESCRIPTION: This interface should be implemented by a 'setup' class which will contain the necessary information to be 
 * 				passed to the constructor of the Puzzle sub-classes. Differently from BasicPuzzleSetup, this interface should
 * 				provide the methods for providing the difficulty coefficient and background picture for the puzzle's visual representation. 
 */
package jpgame.puzzle.setup;

import javax.swing.ImageIcon;

public interface AdvancedPuzzleSetup extends BasicPuzzleSetup {
	public ImageIcon getBackgroundPicture();

	public double getDifficultyCoefficient();

	public String getPreliminaryCode();
}
