/**
 * @author Ani Kristo
 * @version 1.0
 * 
 * COMPLETED: 27 / 04 / 2014
 * 
 * DESCRIPTION: This interface is implemented by BasicPuzzleSetup and it provides the necessary IO Files for the following:
 * 						Template 	 -the '.txt' file that will contain the predefined code for building the entire functional 
 * 									Java class which will produce the desired results. The only part missing should be the 
 * 									code fragment that the player should provide.
 * 						
 * 						Input		 -the file that will be written using the content from Template file and the player's code.
 * 
 *  					Error Log	 -the file that will be used from the compiler to log the compile - time error messages.
 *  
 *   					Key Solution -the file that will be used as a key to test whether the player's response will have the same
 *   								outputs as what the Key Solution file will provide.
 *   
 *   					Output Results - this file should be used to store the answers from the testing process of the Input file.
 *   								When reading this file, the program should be able to determine whether the Input file produced
 *   								the correct result values.
 */
package jpgame.puzzle.setup;

import java.io.File;

import jpgame.JPGComponent;

public interface IOPuzzleFilesSetup extends JPGComponent {

	public File getTemplateFile();

	public File getInputFile();

	public File getErrorLogFile();

	public File getKeySolutionFile();

	public File getResultsOutputFile();

	public File getSolutionOutputFile();
}
