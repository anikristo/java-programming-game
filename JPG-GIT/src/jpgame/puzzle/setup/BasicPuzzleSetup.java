/**
 * @author Ani Kristo
 * @version 1.0
 * 
 * COMPLETED: 26 / 04 / 2014
 * 
 * DESCRIPTION: This interface will ensure that the Puzzle sub-class's constructor will get the necessary information from the class implementing
 * 				this interface. It will include the Puzzle's name, the puzzle's question and the necessary files for compilation, 
 * 				solution checking and errors. 
 */
package jpgame.puzzle.setup;

import jpgame.JPGComponent;

public interface BasicPuzzleSetup extends IOPuzzleFilesSetup, JPGComponent {

	public String getPuzzleName();

	public String getPuzzleQuestion();
	
	public String getStorylineName();

}
