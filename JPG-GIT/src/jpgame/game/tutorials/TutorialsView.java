package jpgame.game.tutorials;

/**
 * TutorialsView	- This class is a view class showing tutorials for the game. It displays pictures as ImageIcon-s and related descriptions in JLabels. It also has a set of JButtons as bookmarks, bookmarking a position in the JScrollPane according to the function
 *
 * @author Anisa Llaveshi
 * @version 1.00 2014/4/28
 */

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

import jpgame.JPGColorLibrary;

public class TutorialsView extends JPanel implements ActionListener {
	
	//Serial NO
	private static final long serialVersionUID = -1729077623580808040L;
	//PROPERTIES
	private ScrollPane tutorialsPane;
	private JLabel titleLabel;
	private JLabel pictureLabel;
	private JLabel titleOfTutorial;
	private JLabel howTo;
	private JPanel pictureAndDescription;
	private JPanel titlePanel;
	private JPanel indexesPanel;
	private JPanel titleOfTutorialPanel;
	private JButton startPlaying;
	private JButton journal;
	private JButton compile;
	private JButton submit;
	private JButton clues;
	private JButton specificJournal;
	private JButton editNotes;
	private JButton seePartner;
	private JButton hints;
	private JLabel descriptionLabel;
	private JPanel descriptionPanel;
	private JPanel picturePanel;

	// CONSTRUCTOR
	public TutorialsView() {
		setLayout(new BorderLayout());

		// A JLabel writing Tutorials, the title of the page, is created and put
		// in a JPanel
		titleLabel = new JLabel("TUTORIALS", JLabel.CENTER);
		titlePanel = new JPanel();
		titlePanel.setBackground(JPGColorLibrary.BACKGROUND_DARK_BLUE);
		titleLabel.setFont(new Font("Segoe UI", Font.BOLD | Font.ITALIC, 30));
		titleLabel.setForeground(JPGColorLibrary.TUTS_LIGHT_VIOLET);
		titlePanel.add(titleLabel);

		// a JPanel which will contain a picture and the description is
		// initialized
		pictureAndDescription = new JPanel();
		pictureAndDescription.setLayout(new BoxLayout(pictureAndDescription,
				BoxLayout.Y_AXIS));

		// TUTORIAL1, START PLAYING
		// The picture that will be explained is put in a JLabel
		pictureLabel = new JLabel(new ImageIcon(getClass().getResource(
				"/jpgame/img/Tutorial1.jpg")), JLabel.CENTER);
		picturePanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
		picturePanel.add(pictureLabel);
		// The text that will explain the picture is put in a JTextPane
		descriptionLabel = new JLabel("", JLabel.CENTER);
		descriptionLabel
				.setText("<html>"
						+ "Let's start playing! "
						+ "Did you just enter the game and want to play it but you don't know how?"
						+ " Click on the picture of a storyline you wish to play and you will be directed to that storyline's puzzles."
						+ "</html>");
		// descriptionLabel.setText("This is some explanation");
		descriptionLabel.setOpaque(true);
		descriptionLabel.setForeground(JPGColorLibrary.BACKGROUND_DARK_BLUE);
		descriptionLabel.setFont(new Font("Segoue UI", 0, 15));
		// descriptionLabel.setEditable (false);
		descriptionLabel.setBackground(JPGColorLibrary.TUTS_LIGHT_VIOLET);
		descriptionLabel.setPreferredSize(new Dimension(820, 100));
		descriptionPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
		descriptionPanel.setPreferredSize(new Dimension(820, 120));
		descriptionPanel.add(descriptionLabel);

		// A JLabel containing the title of the tutorial is initialized and
		// added to the JPanel pictureAnd Description
		titleOfTutorial = new JLabel("Start playing");
		titleOfTutorial.setFont(new Font("Segoue UI", Font.BOLD, 20));
		titleOfTutorial.setForeground(JPGColorLibrary.BACKGROUND_DARK_BLUE);
		titleOfTutorialPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0,
				0));
		titleOfTutorialPanel.setBackground(JPGColorLibrary.TUTS_DARK_VIOLET);
		titleOfTutorialPanel.add(titleOfTutorial);

		// The picture JLabel and the description JTextPane are put in the
		// pictureAndDescription JPanel;
		pictureAndDescription.add(titleOfTutorialPanel);
		pictureAndDescription.add(picturePanel);
		pictureAndDescription.add(descriptionPanel);

		// TUTORIAL2, JOURNAL
		// The picture that will be explained is put in a JLabel
		pictureLabel = new JLabel(new ImageIcon(getClass().getResource(
				"/jpgame/img/Tutorial2.jpg")), JLabel.CENTER);
		picturePanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
		picturePanel.add(pictureLabel);
		// The text that will explain the picture is put in a JTextPane
		descriptionLabel = new JLabel("", JLabel.CENTER);
		descriptionLabel.setText("<html>"
				+ "Click the journal button if you want to see have  a view"
				+ " of all the journals of the storylines of the game."
				+ "<html>");
		descriptionLabel.setOpaque(true);
		descriptionLabel.setForeground(JPGColorLibrary.BACKGROUND_DARK_BLUE);
		descriptionLabel.setFont(new Font("Segoue UI", 0, 15));
		descriptionLabel.setBackground(JPGColorLibrary.TUTS_LIGHT_VIOLET);
		descriptionLabel.setPreferredSize(new Dimension(820, 100));
		descriptionPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
		descriptionPanel.setPreferredSize(new Dimension(820, 120));
		descriptionPanel.add(descriptionLabel);

		// A JLabel containing the title of the tutorial is initialized and
		// added to the JPanel pictureAnd Description
		titleOfTutorial = new JLabel("Journal");
		titleOfTutorial.setFont(new Font("Segoue UI", Font.BOLD, 20));
		titleOfTutorial.setForeground(JPGColorLibrary.BACKGROUND_DARK_BLUE);
		titleOfTutorialPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0,
				0));
		titleOfTutorialPanel.setBackground(JPGColorLibrary.TUTS_DARK_VIOLET);
		titleOfTutorialPanel.add(titleOfTutorial);

		// The picture JLabel and the description JTextPane are put in the
		// pictureAndDescription JPanel;
		pictureAndDescription.add(titleOfTutorialPanel);
		pictureAndDescription.add(picturePanel);
		pictureAndDescription.add(descriptionPanel);

		// TUTORIAL3, COMPILE
		// The picture that will be explained is put in a JLabel
		pictureLabel = new JLabel(new ImageIcon(getClass().getResource(
				"/jpgame/img/Tutorial3.jpg")), JLabel.CENTER);
		picturePanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
		picturePanel.add(pictureLabel);
		// The text that will explain the picture is put in a JTextPane
		descriptionLabel = new JLabel("", JLabel.CENTER);
		descriptionLabel
				.setText("<html>"
						+ "Write the code in the white space provided an click on"
						+ "compile button to compile it. If your code has no compilation errors a pop up window "
						+ "showing \"No Compilation Errors\" will be displayed. Otherwise you will see the compilation errors. "
						+ "<html>");
		descriptionLabel.setOpaque(true);
		descriptionLabel.setForeground(JPGColorLibrary.BACKGROUND_DARK_BLUE);
		descriptionLabel.setFont(new Font("Segoue UI", 0, 15));
		descriptionLabel.setBackground(JPGColorLibrary.TUTS_LIGHT_VIOLET);
		descriptionLabel.setPreferredSize(new Dimension(820, 100));
		descriptionPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
		descriptionPanel.setPreferredSize(new Dimension(820, 120));
		descriptionPanel.add(descriptionLabel);

		// A JLabel containing the title of the tutorial is initialized and
		// added to the JPanel pictureAnd Description
		titleOfTutorial = new JLabel("Compilation of the code");
		titleOfTutorial.setFont(new Font("Segoue UI", Font.BOLD, 20));
		titleOfTutorial.setForeground(JPGColorLibrary.BACKGROUND_DARK_BLUE);
		titleOfTutorialPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0,
				0));
		titleOfTutorialPanel.setBackground(JPGColorLibrary.TUTS_DARK_VIOLET);
		titleOfTutorialPanel.add(titleOfTutorial);

		// The picture JLabel and the description JTextPane are put in the
		// pictureAndDescription JPanel;
		pictureAndDescription.add(titleOfTutorialPanel);
		pictureAndDescription.add(picturePanel);
		pictureAndDescription.add(descriptionPanel);

		// TUTORIAL4, SUBMIT
		// The picture that will be explained is put in a JLabel
		pictureLabel = new JLabel(new ImageIcon(getClass().getResource(
				"/jpgame/img/Tutorial4.jpg")), JLabel.CENTER);
		picturePanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
		picturePanel.add(pictureLabel);
		// The text that will explain the picture is put in a JTextPane
		descriptionLabel = new JLabel("", JLabel.CENTER);
		descriptionLabel
				.setText("<html>"
						+ "When you have no compilation errors submit button will be enabled."
						+ "Click on it if you want to submit the code you have written. You will be notified if your code is correct."
						+ "If it is incorrect a success view will be shown and you will be able to approach to the next puzzle."
						+ "<html>");
		descriptionLabel.setOpaque(true);
		descriptionLabel.setForeground(JPGColorLibrary.BACKGROUND_DARK_BLUE);
		descriptionLabel.setFont(new Font("Segoue UI", 0, 15));
		descriptionLabel.setBackground(JPGColorLibrary.TUTS_LIGHT_VIOLET);
		descriptionLabel.setPreferredSize(new Dimension(820, 100));
		descriptionPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
		descriptionPanel.setPreferredSize(new Dimension(820, 120));
		descriptionPanel.add(descriptionLabel);

		// A JLabel containing the title of the tutorial is initialized and
		// added to the JPanel pictureAnd Description
		titleOfTutorial = new JLabel("Submitting the code");
		titleOfTutorial.setFont(new Font("Segoue UI", Font.BOLD, 20));
		titleOfTutorial.setForeground(JPGColorLibrary.BACKGROUND_DARK_BLUE);
		titleOfTutorialPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0,
				0));
		titleOfTutorialPanel.setBackground(JPGColorLibrary.TUTS_DARK_VIOLET);
		titleOfTutorialPanel.add(titleOfTutorial);

		// The picture JLabel and the description JTextPane are put in the
		// pictureAndDescription JPanel;
		pictureAndDescription.add(titleOfTutorialPanel);
		pictureAndDescription.add(picturePanel);
		pictureAndDescription.add(descriptionPanel);

		// TUTORIAL5, CLUES
		// The picture that will be explained is put in a JLabel
		pictureLabel = new JLabel(new ImageIcon(getClass().getResource(
				"/jpgame/img/Tutorial5.jpg")), JLabel.CENTER);
		picturePanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
		picturePanel.add(pictureLabel);
		// The text that will explain the picture is put in a JTextPane
		descriptionLabel = new JLabel("", JLabel.CENTER);
		descriptionLabel
				.setText("<html>"
						+ "During your journey through the game you will be provided with clues."
						+ "This clues consist of information that might be necessary regarding upcoming puzzles. "
						+ "These clues are hidden somewhere in the pictures f the puzzles and they will be available"
						+ "to you after a certain time that you have been playing. You will have to find click on them "
						+ "in order to get the information. The information will be saved in the journal."
						+ "<html>");
		descriptionLabel.setOpaque(true);
		descriptionLabel.setForeground(JPGColorLibrary.BACKGROUND_DARK_BLUE);
		descriptionLabel.setFont(new Font("Segoue UI", 0, 15));
		descriptionLabel.setBackground(JPGColorLibrary.TUTS_LIGHT_VIOLET);
		descriptionLabel.setPreferredSize(new Dimension(820, 100));
		descriptionPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
		descriptionPanel.setPreferredSize(new Dimension(820, 120));
		descriptionPanel.add(descriptionLabel);

		// A JLabel containing the title of the tutorial is initialized and
		// added to the JPanel pictureAnd Description
		titleOfTutorial = new JLabel("Finding the clues");
		titleOfTutorial.setFont(new Font("Segoue UI", Font.BOLD, 20));
		titleOfTutorial.setForeground(JPGColorLibrary.BACKGROUND_DARK_BLUE);
		titleOfTutorialPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0,
				0));
		titleOfTutorialPanel.setBackground(JPGColorLibrary.TUTS_DARK_VIOLET);
		titleOfTutorialPanel.add(titleOfTutorial);

		// The picture JLabel and the description JTextPane are put in the
		// pictureAndDescription JPanel;
		pictureAndDescription.add(titleOfTutorialPanel);
		pictureAndDescription.add(picturePanel);
		pictureAndDescription.add(descriptionPanel);

		// TUTORIAL6, JOURNAL OF STORYLINE
		// The picture that will be explained is put in a JLabel
		pictureLabel = new JLabel(new ImageIcon(getClass().getResource(
				"/jpgame/img/Tutorial6.jpg")), JLabel.CENTER);
		picturePanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
		picturePanel.add(pictureLabel);
		// The text that will explain the picture is put in a JTextPane
		descriptionLabel = new JLabel("", JLabel.CENTER);
		descriptionLabel
				.setText("<html>"
						+ "If you want to see the journal of the storyline you are currently playing in"
						+ "click on \"See Journal\". You will be directed to a page where information such as the clues you have found, "
						+ "the code you have written, the progress of your code and notes you have written yourself will be shown. How to write notes? See next! "
						+ "<html>");
		descriptionLabel.setOpaque(true);
		descriptionLabel.setForeground(JPGColorLibrary.BACKGROUND_DARK_BLUE);
		descriptionLabel.setFont(new Font("Segoue UI", 0, 15));
		descriptionLabel.setBackground(JPGColorLibrary.TUTS_LIGHT_VIOLET);
		descriptionLabel.setPreferredSize(new Dimension(820, 100));
		descriptionPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
		descriptionPanel.setPreferredSize(new Dimension(820, 120));
		descriptionPanel.add(descriptionLabel);

		// A JLabel containing the title of the tutorial is initialized and
		// added to the JPanel pictureAnd Description
		titleOfTutorial = new JLabel("Accessing a specific Journal");
		titleOfTutorial.setFont(new Font("Segoue UI", Font.BOLD, 20));
		titleOfTutorial.setForeground(JPGColorLibrary.BACKGROUND_DARK_BLUE);
		titleOfTutorialPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0,
				0));
		titleOfTutorialPanel.setBackground(JPGColorLibrary.TUTS_DARK_VIOLET);
		titleOfTutorialPanel.add(titleOfTutorial);

		// The picture JLabel and the description JTextPane are put in the
		// pictureAndDescription JPanel;
		pictureAndDescription.add(titleOfTutorialPanel);
		pictureAndDescription.add(picturePanel);
		pictureAndDescription.add(descriptionPanel);

		// TUTORIAL7, EDIT NOTES
		// The picture that will be explained is put in a JLabel
		pictureLabel = new JLabel(new ImageIcon(getClass().getResource(
				"/jpgame/img/Tutorial7.jpg")), JLabel.CENTER);
		picturePanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
		picturePanel.add(pictureLabel);
		// The text that will explain the picture is put in a JTextPane
		descriptionLabel = new JLabel("", JLabel.CENTER);
		descriptionLabel
				.setText("<html>"
						+ "Do you want to keep notes regarding the puzzles, the games or anything you wish? "
						+ "Click on the \"Edit My Notes\" button." + "<html>");
		descriptionLabel.setOpaque(true);
		descriptionLabel.setForeground(JPGColorLibrary.BACKGROUND_DARK_BLUE);
		descriptionLabel.setFont(new Font("Segoue UI", 0, 15));
		descriptionLabel.setBackground(JPGColorLibrary.TUTS_LIGHT_VIOLET);
		descriptionLabel.setPreferredSize(new Dimension(820, 100));
		descriptionPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
		descriptionPanel.setPreferredSize(new Dimension(820, 120));
		descriptionPanel.add(descriptionLabel);

		// A JLabel containing the title of the tutorial is initialized and
		// added to the JPanel pictureAnd Description
		titleOfTutorial = new JLabel("Editing your notes");
		titleOfTutorial.setFont(new Font("Segoue UI", Font.BOLD, 20));
		titleOfTutorial.setForeground(JPGColorLibrary.BACKGROUND_DARK_BLUE);
		titleOfTutorialPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0,
				0));
		titleOfTutorialPanel.setBackground(JPGColorLibrary.TUTS_DARK_VIOLET);
		titleOfTutorialPanel.add(titleOfTutorial);

		// The picture JLabel and the description JTextPane are put in the
		// pictureAndDescription JPanel;
		pictureAndDescription.add(titleOfTutorialPanel);
		pictureAndDescription.add(picturePanel);
		pictureAndDescription.add(descriptionPanel);

		// TUTORIAL8, SEE PARTNER
		// The picture that will be explained is put in a JLabel
		pictureLabel = new JLabel(new ImageIcon(getClass().getResource(
				"/jpgame/img/Tutorial8.jpg")), JLabel.CENTER);
		picturePanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
		picturePanel.add(pictureLabel);
		// The text that will explain the picture is put in a JTextPane
		descriptionLabel = new JLabel("", JLabel.CENTER);
		descriptionLabel
				.setText("<html>"
						+ "In each of your adventures you will be accompanied with a partner. "
						+ "If you need help or need to get other neccessary information regarding your mission "
						+ "click \"See Partner\" button." + "<html>");
		descriptionLabel.setOpaque(true);
		descriptionLabel.setForeground(JPGColorLibrary.BACKGROUND_DARK_BLUE);
		descriptionLabel.setFont(new Font("Segoue UI", 0, 15));
		descriptionLabel.setBackground(JPGColorLibrary.TUTS_LIGHT_VIOLET);
		descriptionLabel.setPreferredSize(new Dimension(820, 100));
		descriptionPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
		descriptionPanel.setPreferredSize(new Dimension(820, 120));
		descriptionPanel.add(descriptionLabel);

		// A JLabel containing the title of the tutorial is initialized and
		// added to the JPanel pictureAnd Description
		titleOfTutorial = new JLabel("Seeing the partner");
		titleOfTutorial.setFont(new Font("Segoue UI", Font.BOLD, 20));
		titleOfTutorial.setForeground(JPGColorLibrary.BACKGROUND_DARK_BLUE);
		titleOfTutorialPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0,
				0));
		titleOfTutorialPanel.setBackground(JPGColorLibrary.TUTS_DARK_VIOLET);
		titleOfTutorialPanel.add(titleOfTutorial);

		// The picture JLabel and the description JTextPane are put in the
		// pictureAndDescription JPanel;
		pictureAndDescription.add(titleOfTutorialPanel);
		pictureAndDescription.add(picturePanel);
		pictureAndDescription.add(descriptionPanel);

		// TUTORIAL9, HINTS
		// The picture that will be explained is put in a JLabel
		pictureLabel = new JLabel(new ImageIcon(getClass().getResource(
				"/jpgame/img/Tutorial9.jpg")), JLabel.CENTER);
		picturePanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
		picturePanel.add(pictureLabel);
		// The text that will explain the picture is put in a JTextPane
		descriptionLabel = new JLabel("", JLabel.CENTER);
		descriptionLabel
				.setText("<html>"
						+ "If you can not progress anymore in the puzzle you are palying"
						+ " click on \"I want a hint\". The partner knows when to give you the hints so there may or may not "
						+ "be available hints when you ask for them. If you instead wnat to see hints that you have collected "
						+ "before click on \"View the hints collected\"."
						+ "<html>");
		descriptionLabel.setOpaque(true);
		descriptionLabel.setForeground(JPGColorLibrary.BACKGROUND_DARK_BLUE);
		descriptionLabel.setFont(new Font("Segoue UI", 0, 15));
		descriptionLabel.setBackground(JPGColorLibrary.TUTS_LIGHT_VIOLET);
		descriptionLabel.setPreferredSize(new Dimension(820, 100));
		descriptionPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
		descriptionPanel.setPreferredSize(new Dimension(820, 120));
		descriptionPanel.add(descriptionLabel);

		// A JLabel containing the title of the tutorial is initialized and
		// added to the JPanel pictureAnd Description
		titleOfTutorial = new JLabel("Getting Hints");
		titleOfTutorial.setFont(new Font("Segoue UI", Font.BOLD, 20));
		titleOfTutorial.setForeground(JPGColorLibrary.BACKGROUND_DARK_BLUE);
		titleOfTutorialPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0,
				0));
		titleOfTutorialPanel.setBackground(JPGColorLibrary.TUTS_DARK_VIOLET);
		titleOfTutorialPanel.add(titleOfTutorial);

		// The picture JLabel and the description JTextPane are put in the
		// pictureAndDescription JPanel;
		pictureAndDescription.add(titleOfTutorialPanel);
		pictureAndDescription.add(picturePanel);
		pictureAndDescription.add(descriptionPanel);

		tutorialsPane = new ScrollPane();
		tutorialsPane.add(pictureAndDescription);

		// JButtons are created for each of the parts of the game that are going
		// to be explained in the tutorial and actionListener-s are added to all
		// of them
		startPlaying = new JButton("Start playing");
		startPlaying.addActionListener(this);
		startPlaying.setBackground(JPGColorLibrary.TUTS_LIGHT_VIOLET);
		startPlaying.setBorder(BorderFactory
				.createBevelBorder(BevelBorder.RAISED));
		startPlaying.setPreferredSize(new Dimension(150, 100));
		journal = new JButton("Access the journal");
		journal.addActionListener(this);
		journal.setBackground(JPGColorLibrary.TUTS_LIGHT_VIOLET);
		journal.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
		compile = new JButton("Compile the code");
		compile.addActionListener(this);
		compile.setBackground(JPGColorLibrary.TUTS_LIGHT_VIOLET);
		compile.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
		submit = new JButton("Submit the code");
		submit.addActionListener(this);
		submit.setBackground(JPGColorLibrary.TUTS_LIGHT_VIOLET);
		submit.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
		clues = new JButton("Find clues");
		clues.addActionListener(this);
		clues.setBackground(JPGColorLibrary.TUTS_LIGHT_VIOLET);
		clues.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
		specificJournal = new JButton("Go to specific journal");
		specificJournal.addActionListener(this);
		specificJournal.setBackground(JPGColorLibrary.TUTS_LIGHT_VIOLET);
		specificJournal.setBorder(BorderFactory
				.createBevelBorder(BevelBorder.RAISED));
		editNotes = new JButton("Edit your notes");
		editNotes.addActionListener(this);
		editNotes.setBackground(JPGColorLibrary.TUTS_LIGHT_VIOLET);
		editNotes
				.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
		seePartner = new JButton("See the partner");
		seePartner.addActionListener(this);
		seePartner.setBackground(JPGColorLibrary.TUTS_LIGHT_VIOLET);
		seePartner.setBorder(BorderFactory
				.createBevelBorder(BevelBorder.RAISED));
		hints = new JButton("Get hints");
		hints.addActionListener(this);
		hints.setBackground(JPGColorLibrary.TUTS_LIGHT_VIOLET);
		hints.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));

		howTo = new JLabel("How to: ", JLabel.CENTER);
		howTo.setFont(new Font("Segoe UI", Font.BOLD | Font.ITALIC, 27));
		howTo.setForeground(JPGColorLibrary.TUTS_LIGHT_VIOLET);

		// JPanel containing all the JButtons previously created
		indexesPanel = new JPanel(new GridLayout(10, 1, 0, 20));
		indexesPanel.setBackground(JPGColorLibrary.BACKGROUND_DARK_BLUE);
		indexesPanel.add(howTo);
		indexesPanel.add(startPlaying);
		indexesPanel.add(journal);
		indexesPanel.add(compile);
		indexesPanel.add(submit);
		indexesPanel.add(clues);
		indexesPanel.add(specificJournal);
		indexesPanel.add(editNotes);
		indexesPanel.add(seePartner);
		indexesPanel.add(hints);

		add(titlePanel, BorderLayout.PAGE_START);
		add(tutorialsPane, BorderLayout.CENTER);
		add(indexesPanel, BorderLayout.LINE_END);
	}

	// METHODS

	// This method checks from which JButton the event source comes and sets the
	// scroll position to the related position
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == startPlaying) {
			tutorialsPane.setScrollPosition(0, 0);
		}

		else if (e.getSource() == journal) {
			tutorialsPane.setScrollPosition(0, 570);
		}

		else if (e.getSource() == compile) {
			tutorialsPane.setScrollPosition(0, 1140);
		}

		else if (e.getSource() == submit) {
			tutorialsPane.setScrollPosition(0, 1710);
		}

		else if (e.getSource() == clues) {
			tutorialsPane.setScrollPosition(0, 2280);
		}

		else if (e.getSource() == specificJournal) {
			tutorialsPane.setScrollPosition(0, 2850);
		}

		else if (e.getSource() == editNotes) {
			tutorialsPane.setScrollPosition(0, 3420);
		}

		else if (e.getSource() == seePartner) {
			tutorialsPane.setScrollPosition(0, 3990);
		}

		else if (e.getSource() == hints) {
			tutorialsPane.setScrollPosition(0, 4560);
		}
	}
}//End of TutorialsView
