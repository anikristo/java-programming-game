package jpgame.game.tutorials;

/**
 * TutorialsFrmae	- This class extends JFrame and it creates the JFrame that shows the Tutorials.
 *
 * @author Anisa Llaveshi
 * @version 1.00 2014/5/10
 */

import java.awt.Toolkit;
import javax.swing.JFrame;

public class TutorialsFrame extends JFrame
{
	//Serial NO
	private static final long serialVersionUID = 7686264734067451505L;
	
	// CONSTRUCTOR
	public TutorialsFrame ()
	{
		TutorialsView tutorials;
		tutorials = new TutorialsView();
		
		add(tutorials);
		
		setTitle( "Tutorials");
		setBounds( 0, 0, Toolkit.getDefaultToolkit().getScreenSize().width, Toolkit.getDefaultToolkit().getScreenSize().height - 50);
		setVisible(true);		
	}
} //End of TutorialsFrame
