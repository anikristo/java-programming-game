/**
 * @author Ani Kristo
 * @version 1.0
 * 
 * @date: 26 / 07 / 2014
 * 
 * Description: This JDialog is displayed if there are no more puzzles available to play. This is the case when
 * 				all the storylines are finished and there is no update currently.
 */
package jpgame.game;

import java.awt.BorderLayout;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import jpgame.JPGColorLibrary;
import jpgame.JPGComponent;

public class NoPuzzlesToPlayDialog extends JDialog implements ActionListener,
		JPGComponent {

	/**
	 * GENERATED SERIAL NO
	 */
	private static final long serialVersionUID = 8079314926079351827L;

	// PROPERTIES
	private JLabel message;
	private JLabel title;
	private JButton okButton;

	// CONSTRUCTOR
	public NoPuzzlesToPlayDialog() {

		title  = new JLabel( "<html><b>There are currently no puzzles available to play</b></html>");
		title.setFont( new Font( "Segoe UI", Font.PLAIN, 24));
		title.setForeground( JPGColorLibrary.JPG_CRIMSON);
		
		message = new JLabel();
		message.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		message.setText("<html>You can check your progress, scores and notes of the played storylines in the Journal section."
				+ "<br>We hope the storyline feed will be updated as soon as possible.</html>");

		JPanel messagePanel = new JPanel();
		messagePanel.add( title);
		messagePanel.add(message);

		okButton = new JButton("OK");
		okButton.setForeground(JPGColorLibrary.PURE_WHITE);
		okButton.setBackground(JPGColorLibrary.JPG_CRIMSON);
		okButton.setFont(new Font("Segoe UI", Font.BOLD, 16));
		okButton.addActionListener(this);

		JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		buttonPanel.add(okButton);

		JPanel mainPanel = new JPanel(new BorderLayout());
		mainPanel.add(messagePanel, "Center");
		mainPanel.add(buttonPanel, "South");

		add(mainPanel);

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension dialogSize = new Dimension(screenSize.width / 2,
				screenSize.height / 4);

		setBounds((screenSize.width - dialogSize.width) / 2,
				(screenSize.height - dialogSize.height) / 2, dialogSize.width,
				dialogSize.height); // centered
		setVisible(true);
	}

	// METHODS
	@Override
	public void actionPerformed(ActionEvent event) {
		// Event is generated only from OK button
		dispose();
	}
}
