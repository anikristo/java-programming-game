package jpgame.game;

/**
 * JPGHomePageView - interface representing a HomePageView
 * 
 * @author Ani Kristo
 * @version 1.00 2014/22/7
 */

import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;

import jpgame.JPGView;

public interface JPGHomepageView extends JPGView {
	public JButton getJournalButton();
	public JButton getTutorialsButton();
	public ArrayList<JButton> getStorylinesInProgressButtons();
	public ArrayList<JButton> getStorylinesNotStartedButtons();
	public void addActionListeners( ActionListener actionListener );
}
