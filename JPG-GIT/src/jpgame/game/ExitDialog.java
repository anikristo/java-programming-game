package jpgame.game;

/**
* ExitDialog.java
*
* ExitDialog application at JavaProgrammingGame
*
* Creates the exiting dialog to be displayed whenever the user wants to quit the game
*
* Author: Ayse Berceste Dincer
* version 1.00 2014/17/7 
**/

import java.awt.GridLayout;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import jpgame.JPGColorLibrary;
import jpgame.JPGComponent;
import jpgame.journal.Journal;

public class ExitDialog extends JDialog implements ActionListener, JPGComponent {

	//Serial NO
	private static final long serialVersionUID = 6924422170564807534L;
	//Properties
	private final JPanel contentPanel = new JPanel( new GridLayout(2,1) );
	private JButton yesButton;
	private JButton noButton;
	private JPanel buttonPane;
	private JLabel title;
	private GameFrame gameFrame;

	// CONSTRUCTOR
	public ExitDialog( GameFrame originatingGameFrame) {
 
		//Initializing gameFrame
		this.gameFrame = originatingGameFrame;
	
		//Yes Button
		yesButton = new JButton("YES");
		yesButton.setFont(new Font("Segoe UI", Font.BOLD, 16));
		yesButton.setBackground(JPGColorLibrary.JPG_CRIMSON);
		yesButton.setForeground(JPGColorLibrary.PURE_WHITE);
		yesButton.addActionListener(this);

		//No Button
		noButton = new JButton("NO");
		noButton.setFont(new Font("Segoe UI", Font.BOLD, 16));
		noButton.setBackground(JPGColorLibrary.JPG_CRIMSON);
		noButton.setForeground(JPGColorLibrary.PURE_WHITE);
		noButton.addActionListener(this);

		//Creates the buttons panel
		buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout() );
		buttonPane.add(yesButton);
		buttonPane.add(noButton);

		//Creates the title
		title = new JLabel("Are you sure you want to exit the game?");
		title.setFont(new Font("Segoe UI", Font.BOLD | Font.ITALIC, 20));
		title.setForeground(JPGColorLibrary.JPG_CRIMSON);
		
		JPanel titlePanel = new JPanel( new FlowLayout( FlowLayout.CENTER));
		titlePanel.add( title);

		contentPanel.add( titlePanel );
		contentPanel.add(buttonPane );
		add(contentPanel );
		
		// Get Screen size and determine the dimensions of the window
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension windowSize = new Dimension(screenSize.width / 3,
				screenSize.height / 5 );

		setBounds((screenSize.width - windowSize.width) / 2,
				(screenSize.height - windowSize.height) / 2, windowSize.width,
				windowSize.height); // centered

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("Java Programming Game - Exit");
		setVisible(true);
	}

	// METHODS
	@Override
	public void actionPerformed(ActionEvent e) {
		//Saves and exits if the user wants to quit
		if (e.getSource() == yesButton) {
			this.dispose();
			gameFrame.setVisible( false);
			Journal.saveContents();
			gameFrame.getGame().save("game.data");
			gameFrame.dispose();
		}
		//Does nothing if the user does not quit
		else if (e.getSource() == noButton) {
			setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
			this.dispose();
		}
	}

} //End of ExitDialog
