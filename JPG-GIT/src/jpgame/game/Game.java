package jpgame.game;

import java.io.Serializable;
import java.util.Vector;
import java.io.*;

import jpgame.JPGComponent;
import jpgame.journal.Journal;
import jpgame.storyline.Storyline;

/**
 * Game - This class is an abstract class which represents a game and it is a
 * JPGComponent and is Serializable. It has accessors and other related methods.
 * This class has get methods like getNotStartedStorylines(),
 * getStartedStorylines() and getFinishedStorylines() which are required for
 * progress tracking, each keeping progress of the storylines that have not been
 * started, the ones that have been started and the finished ones. It has also
 * methods to add and remove a storyline from the game. This class is the class
 * which has the save(String), load(String) and loadFiles(Game) methods which
 * make possible the saving of the information of the game and its reloading.
 * 
 * @author Anisa Llaveshi
 * @version 1.00 2014/26/4
 */

public abstract class Game implements JPGComponent, Serializable {

	//Serial NO
	private static final long serialVersionUID = 6033284390361894200L;
	
	//CONSTANTS
	public final int NOT_STARTED = -1;
	public final int IN_PROGRESS = 0;
	public final int COMPLETED = 1;

	// PROPERTIES
	private Vector<Storyline> storylines;
	private transient JPGHomepageView homepageView;

	// CONSTRUCTORS
	// A constructor taking a Vector of Storylines
	public Game(Vector<Storyline> storylines) {
		this.storylines = storylines;
		homepageView = new HomepageView(this);
		Journal.setGameReference(this);
	}

	// A constructor taking no parameters
	public Game() {
		storylines = new Vector<Storyline>();
		homepageView = new HomepageView(this);
		Journal.setGameReference(this);
	}

	//METHODS
	
	// Method that returns the storylines of the game
	public Vector<Storyline> getStorylines() {
		return storylines;
	}

	// Method that adds a storyline to the vector of storylines
	public boolean addStoryline(Storyline storyline) {
		if (storyline == null)
			return false;
		else {
			storylines.add(storyline);
			return true;
		}
	}

	// Method that removes a storyline at a specific index
	public boolean removeStoryline(Storyline storyline) {
		return storylines.remove(storyline);
	}

	// Method that returns a vector with the storylines that have the status in
	// progress
	public Vector<Storyline> getStorylinesInProgress() {
		Vector<Storyline> startedStorylines;
		startedStorylines = new Vector<Storyline>();

		for (int i = 0; i < storylines.size(); i++) {
			if (storylines.get(i).getStatus() == IN_PROGRESS) {
				startedStorylines.add(storylines.get(i));
			}
		}

		return startedStorylines;
	}

	// Method that returns a vector with the storylines that are finished
	public Vector<Storyline> getFinishedStorylines() {
		Vector<Storyline> finishedStorylines;
		finishedStorylines = new Vector<Storyline>();

		for (int i = 0; i < storylines.size(); i++) {
			if (storylines.get(i).getStatus() == COMPLETED) {
				finishedStorylines.addElement(storylines.get(i));
			}
		}

		return finishedStorylines;
	}

	// Method that returns a vector with the storylines that are not started
	public Vector<Storyline> getNotStartedStorylines() {
		Vector<Storyline> notStartedStorylines;
		notStartedStorylines = new Vector<Storyline>();

		for (int i = 0; i < storylines.size(); i++) {
			if (storylines.get(i).getStatus() == NOT_STARTED) {
				notStartedStorylines.addElement(storylines.get(i));
			}
		}

		return notStartedStorylines;
	}

	// Method to save information of the game
	public void save(String filename) {
		try {
			FileOutputStream ostream = new FileOutputStream(filename);
			ObjectOutputStream p = new ObjectOutputStream(ostream);
			p.writeObject(this);
			p.flush();
			p.close();
			ostream.close();
			System.out.println("Game saved successfully");
		} catch (IOException e) {
			System.out.println("Game not saved successfully. Because");
			System.out.println(e.getLocalizedMessage());
			e.printStackTrace();
		}
	}

	// Method to load the information saved in the game
	public static Game load(String filename) {
		Game temp = null;
		try {

			FileInputStream istream = new FileInputStream(filename);
			ObjectInputStream p = new ObjectInputStream(istream);
			System.out.println("Files found successfully.");

			try {
				temp = (Game) p.readObject();
				temp.loadFiles(temp);
				System.out.println("Game loaded successfully.");

			} catch (Exception e) {
				System.out.println("File not read successfully.");
				e.printStackTrace();
			}
			p.close();
			istream.close();
		} catch (IOException e) {
			System.out
					.println("Game not loaded/Files not founded successfully.");
			e.printStackTrace();
		}

		return temp;
	}

	private void loadFiles(Game t) {
		this.storylines = t.storylines;
	}
	
	public void createViews() 
	{
		homepageView = new HomepageView(this);
		for (Storyline s : storylines)
			s.createViews();
	}
	
	public JPGHomepageView getHomePageView() {
		homepageView.updateView( this);
		return homepageView;
	}

	public void setHomepageView(JPGHomepageView homepageView) {
		this.homepageView = homepageView;
		homepageView.updateView( this);
	}

} //End of Game
