package jpgame.game;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.sound.sampled.*;

import jpgame.JPGComponent;
import jpgame.game.tutorials.TutorialsFrame;
import jpgame.journal.EditNotesDialog;
import jpgame.journal.JPGJournalPageView;
import jpgame.journal.Journal;
import jpgame.journal.index.JPGIndexPageView;
import jpgame.partner.JPGPartnerView;
import jpgame.partner.hints.CollectedHints;
import jpgame.partner.hints.NewHintDialog;
import jpgame.puzzle.CompilationFeedbackDialogBox;
import jpgame.puzzle.IncorrectSolutionDialog;
import jpgame.puzzle.JPGPuzzleView;
import jpgame.puzzle.Puzzle;
import jpgame.puzzle.successpage.JPGSuccessView;
import jpgame.storyline.Storyline;
import jpgame.storyline.desc.JPGDescriptionView;
import jpgame.storyline.win.JPGWinningView;
import jpgame.storyline.win.WinningView;

//*****************************************************************************************************************
// GameFrame 
// Main frame of the game which contains all views of the game
// 
// Authors: Ayse Berceste Dincer, Burcu Canakci
// version 1.00 2014/1/5
//*****************************************************************************************************************

public class GameFrame extends JFrame implements JPGComponent, ActionListener,
		WindowListener {
	// CONSTANTS
	private static final long serialVersionUID = -7478897195315920033L;

	// Properties
	private CardLayout cardLayout;
	private JPanel cards;
	private Game game;
	private JPGHomepageView homepageView;
	private JPGPuzzleView puzzleView;
	private JPGSuccessView successView;
	private JPGIndexPageView indexView;
	private JPGDescriptionView descriptionView;
	private JPGJournalPageView journalPageView;
	private JPGPartnerView partnerView;
	private JPGWinningView win;
	private Puzzle currentlyPlayedPuzzle; // This is the reference to the puzzle
											// that is currently being played

	private Puzzle currentPuzzleForJournal; // This is the reference to the
											// puzzle as shown in the journal.
											// For example, when the user goes
											// to another page of the journal,
											// this references the other puzzle,
											// while the variable
											// currentlyPlayedPuzzle is still
											// the same
	private Storyline currentlyPlayedStoryline;
	private Storyline currentStorylineForJournal;
	private String lastSource;
	private AudioFormat audioFormat;
	private AudioInputStream audioInputStream;
	private SourceDataLine sourceDataLine;

	// Constructor
	public GameFrame(Game game) {

		// Initializes the game property
		this.game = game;
		Journal.setGameReference(game);

		// Adds window listener to the frame
		addWindowListener(this);

		// Creates the journal frame
		setTitle("Java Programming Game");
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Creates the card layout
		cardLayout = new CardLayout();
		cards = new JPanel(cardLayout);

		// Create the Win view
		win = new WinningView(null);

		currentlyPlayedStoryline = game.getStorylines().get(0);

		// DESCRIPTION VIEW
		descriptionView = currentlyPlayedStoryline.getDescriptionView();
		cards.add((Component) descriptionView, "Description");
		

		// PARTNERVIEW
		partnerView = currentlyPlayedStoryline.getPartner().getPartnerView();
		if (partnerView != null)
			cards.add((Component) partnerView, "Partner");

		// PUZZLEVIEW
		currentlyPlayedPuzzle = game.getStorylines().get(0).getCurrentPuzzle();
		puzzleView = currentlyPlayedPuzzle.getPuzzleView();
		cards.add((Component) puzzleView, "Puzzle");

		// SUCCESSVIEW
		successView = currentlyPlayedPuzzle.getSuccessView();
		cards.add((Component) successView, "Success");

		// JOURNAL
		// Index Page View
		currentStorylineForJournal = currentlyPlayedStoryline; // Default
		indexView = Journal.getIndexPage(game).getIndexPageView();
		indexView.addActionListeners(this);
		cards.add((Component) indexView, "JournalIndexView");
		// Journal Page Views
		currentPuzzleForJournal = currentlyPlayedPuzzle;
		journalPageView = Journal.getJournalPage(currentPuzzleForJournal)
				.getJournalPageView();
		journalPageView.addActionListeners(this);
		cards.add((Component) journalPageView, "JournalPageView");
		// END OF JOURNAL

		// GAME VIEW
		homepageView = game.getHomePageView();
		homepageView.addActionListeners(this);
		cards.add((Component) homepageView, "Homepage");

		cardLayout.show(cards, "Homepage");
		lastSource = "Homepage";

		// Sets the bounds of the frame
		Dimension d = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		setBounds(0, 0, d.width, d.height - 40);
		add(cards);
		setVisible(true);
	}

	// ActionPerformed method - Acts according to the event sources
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton source = (JButton) e.getSource();

		// HOMEPAGE
		for (int i = 0; i < homepageView.getStorylinesNotStartedButtons()
				.size(); i++) {
			if (source == homepageView.getStorylinesNotStartedButtons().get(i)) {
				currentlyPlayedStoryline = game.getNotStartedStorylines()
						.get(i);

				descriptionView = currentlyPlayedStoryline.getDescriptionView();
				descriptionView.addActionListeners(this);
				cards.add((Component) descriptionView, "Description");
				cardLayout.show(cards, "Description");
				lastSource = "Homepage";
			}
		}

		for (int i = 0; i < homepageView.getStorylinesInProgressButtons()
				.size(); i++) {
			if (source == homepageView.getStorylinesInProgressButtons().get(i)) {
				currentlyPlayedStoryline = game.getStorylinesInProgress()
						.get(i);
				currentlyPlayedPuzzle = currentlyPlayedStoryline
						.getCurrentPuzzle();
				puzzleView = currentlyPlayedPuzzle.getPuzzleView();
				puzzleView.addActionListeners(this);
				Journal.addToChronologicalList(currentlyPlayedPuzzle);
				cards.add((Component) puzzleView, "Puzzle");
				puzzleView.startTimer();
				lastSource = "Puzzle";
				cardLayout.show(cards, "Puzzle");
			}
		}

		if (source == homepageView.getJournalButton()) {
			indexView = Journal.getIndexPage(game).getIndexPageView();
			indexView.addActionListeners(this);
			cards.add((Component) indexView, "JournalIndexView");
			cardLayout.show(cards, "JournalIndexView");
			lastSource = "IndexPage";
		}

		if (source == homepageView.getTutorialsButton()) {
			new TutorialsFrame();
		}

		// DESCRIPTION

		if (source == descriptionView.getNextButton()) {
			if (currentlyPlayedStoryline.getPuzzles().size() > 0) {
				currentlyPlayedPuzzle = currentlyPlayedStoryline.getPuzzles()
						.get(0);
				Journal.addToChronologicalList(currentlyPlayedPuzzle);
				puzzleView = currentlyPlayedPuzzle.getPuzzleView();
				puzzleView.addActionListeners(this);
				puzzleView.startTimer();
				cards.add((Component) puzzleView, "Puzzle");
				cardLayout.show(cards, "Puzzle");
				lastSource = "Puzzle";
				currentlyPlayedStoryline.setStatus();
			} else
				JOptionPane.showMessageDialog(this,
						"There are no puzzles in this storyline");
		}

		if (source == descriptionView.getBackButton()) {
			if (lastSource == "Puzzle")
				cardLayout.show(cards, "Puzzle");
			else {
				homepageView = game.getHomePageView();
				homepageView.addActionListeners(this);
				cards.add((Component) homepageView, "Homepage");
				cardLayout.show(cards, "Homepage");
				lastSource = "Homepage";
			}
		}

		// PARTNER
		if (source == partnerView.getGetHintButton()) {
			playAudio(getClass().getResource("/jpgame/audio/Tone.wav")
					.getPath());
			new NewHintDialog(currentlyPlayedPuzzle);
		}

		if (source == partnerView.getViewHintsButton()) {
			new CollectedHints(currentlyPlayedPuzzle);
		}

		if (source == partnerView.getExplainMissionButton()) {
			descriptionView = currentlyPlayedStoryline.getDescriptionView();
			descriptionView.getNextButton().setEnabled(false);
			descriptionView.addActionListeners(this);
			cards.add((Component) descriptionView, "Description");
			cardLayout.show(cards, "Description");
		}

		if (source == partnerView.getGoBackButton()) {
			cardLayout.show(cards, "Puzzle");
		}

		// PUZZLE
		if (source == puzzleView.getSeePartnerButton()) {
			currentlyPlayedStoryline.updatePartner();
			partnerView = currentlyPlayedStoryline.getPartner()
					.getPartnerView();
			partnerView.addActionListeners(this);
			cards.add((Component) partnerView, "Partner");
			cardLayout.show(cards, "Partner");
		}

		if (source == puzzleView.getSeeJournalButton()) {
			currentPuzzleForJournal = currentlyPlayedPuzzle;
			currentStorylineForJournal = currentlyPlayedStoryline;
			journalPageView = Journal.getJournalPage(currentPuzzleForJournal)
					.getJournalPageView();
			if (currentlyPlayedStoryline.getAttemptedPuzzles().size() == 1)
				journalPageView.getPreviousButton().setEnabled(false);
			else
				journalPageView.getPreviousButton().setEnabled(true);
			if (currentPuzzleForJournal == currentStorylineForJournal
					.getPuzzles().get(
							currentStorylineForJournal.getFinishedPuzzles()
									.size()))
				journalPageView.getNextButton().setEnabled(false);
			else
				journalPageView.getNextButton().setEnabled(true);

			journalPageView.addActionListeners(this);
			cards.add((Component) journalPageView, "JournalPageView");
			cardLayout.show(cards, "JournalPageView");
		}

		if (source == puzzleView.getGoToHomePageButton()) {
			puzzleView.stopTimer();
			homepageView = game.getHomePageView();
			homepageView.addActionListeners(this);
			cards.add((Component) homepageView, "Homepage");
			cardLayout.show(cards, "Homepage");
			lastSource = "Homepage";
		}

		if (source == puzzleView.getSubmitButton()) {
			currentlyPlayedPuzzle.checkSolution();
			game.save("game.data");
			if (!currentlyPlayedPuzzle.isSolved()) {
				new IncorrectSolutionDialog();
				playAudio(getClass().getResource("/jpgame/audio/Error.wav")
						.getPath());
			} else {
				puzzleView.stopTimer();
				if (currentlyPlayedPuzzle.getUserCodes().size() >= 1)
					currentlyPlayedPuzzle.getUserCodes().remove(
							currentlyPlayedPuzzle.getUserCodes().size() - 1);
				currentlyPlayedPuzzle = currentlyPlayedStoryline
						.getNextPuzzle();

				if (currentlyPlayedPuzzle == null) { // end of storyline
					currentlyPlayedPuzzle = currentlyPlayedStoryline
							.getPuzzles().get(
									currentlyPlayedStoryline.getPuzzles()
											.size() - 1);
					win = currentlyPlayedStoryline.getWinningView();
					win.addActionListeners(this);
					currentlyPlayedStoryline.updateFinishedPuzzles();
					currentlyPlayedStoryline.setStatus();
					cards.add((Component) win, "WinningView");
					cardLayout.show(cards, "WinningView");
				} else { // there are more puzzles
					Journal.addToChronologicalList(currentlyPlayedPuzzle);
					currentlyPlayedStoryline.updateFinishedPuzzles();
					currentlyPlayedStoryline.setStatus();
					successView = currentlyPlayedStoryline
							.getPreviousPuzzleInAttempted(currentlyPlayedPuzzle)
							.getSuccessView();
					successView.addActionListeners(this);
					cards.add((Component) successView, "Success");
					cardLayout.show(cards, "Success");
				}
				playAudio(getClass().getResource("/jpgame/audio/Tada.wav")
						.getPath());
			}
		}

		if (source == puzzleView.getCompileButton()) {
			currentlyPlayedPuzzle.setUsersSolution(puzzleView.getCodingField()
					.getText());
			currentlyPlayedPuzzle.getUserCodes().add(
					puzzleView.getCodingField().getText());
			currentlyPlayedPuzzle.compile();
			new CompilationFeedbackDialogBox(currentlyPlayedPuzzle);
			if (currentlyPlayedPuzzle.isCompiled())
				playAudio(getClass().getResource("/jpgame/audio/Tone.wav")
						.getPath());
			else
				playAudio(getClass().getResource("/jpgame/audio/Error.wav")
						.getPath());
		}

		// WIN
		if (source == win.getHomepageButton()) {
			homepageView = game.getHomePageView();
			homepageView.addActionListeners(this);
			cards.add((Component) homepageView, "Homepage");
			cardLayout.show(cards, "Homepage");
			lastSource = "Homepage";
		}

		if (source == win.getNotesButton()) {
			lastSource = "Win";
			if (currentlyPlayedStoryline.getStatus() == Storyline.COMPLETED)
				new EditNotesDialog(currentlyPlayedPuzzle);
			else
				new EditNotesDialog(
						currentlyPlayedStoryline
								.getPreviousPuzzleInAttempted(currentlyPlayedPuzzle));

		}

		// SUCCESS
		if (source == successView.getHomepageButton()) {
			homepageView = game.getHomePageView();
			homepageView.addActionListeners(this);
			cards.add((Component) homepageView, "Homepage");
			cardLayout.show(cards, "Homepage");
			lastSource = "Homepage";
		}

		if (source == successView.getNextButton()) {
			puzzleView = currentlyPlayedPuzzle.getPuzzleView();
			cards.add((Component) puzzleView, "Puzzle");
			puzzleView.addActionListeners(this);
			puzzleView.startTimer();
			lastSource = "Puzzle";
			cardLayout.show(cards, "Puzzle");
		}

		if (source == successView.getNotesButton()) {
			lastSource = "Success";
			if (currentlyPlayedStoryline.getStatus() == 1)
				new EditNotesDialog(currentlyPlayedPuzzle);
			else
				new EditNotesDialog(
						currentlyPlayedStoryline
								.getPreviousPuzzleInAttempted(currentlyPlayedPuzzle));

		}

		// INDEX
		if (source == indexView.getBackButton()) {
			cardLayout.show(cards, "Homepage");
		}

		for (int i = 0; i < indexView.getStorylineButtons().size(); i++) {
			if (source == indexView.getStorylineButtons().get(i)) {
				currentStorylineForJournal = game.getStorylines().get(i);
				currentPuzzleForJournal = currentStorylineForJournal
						.getPuzzles().get(0);
				journalPageView = Journal.getJournalPage(
						currentPuzzleForJournal).getJournalPageView();
				journalPageView.getPreviousButton().setEnabled(false);
				if (currentStorylineForJournal.getAttemptedPuzzles().size() == 1) {
					journalPageView.getNextButton().setEnabled(false);
				} else
					journalPageView.getNextButton().setEnabled(true);

				journalPageView.addActionListeners(this);
				cards.add((Component) journalPageView, "JournalPageView");
				cardLayout.show(cards, "JournalPageView");
				lastSource = "storylineIndex";
			}
		}

		for (int i = 0; i < indexView.getChronologicalPuzzlesButtons().size(); i++) {
			if (source == indexView.getChronologicalPuzzlesButtons().get(i)) {
				currentPuzzleForJournal = Journal.getChronologicalList().get(i);
				journalPageView = Journal.getJournalPage(
						currentPuzzleForJournal).getJournalPageView();
				if (i == 0)
					journalPageView.getPreviousButton().setEnabled(false);
				else
					journalPageView.getPreviousButton().setEnabled(true);
				if (i == indexView.getChronologicalPuzzlesButtons().size() - 1)
					journalPageView.getNextButton().setEnabled(false);
				else
					journalPageView.getNextButton().setEnabled(true);
				journalPageView.addActionListeners(this);
				cards.add((Component) journalPageView, "JournalPageView");
				cardLayout.show(cards, "JournalPageView");
				lastSource = "chrIndex";
			}
		}

		for (int i = 0; i < indexView.getPortfolioPuzzlesButtons().size(); i++) {
			if (source == indexView.getPortfolioPuzzlesButtons().get(i)) {
				currentPuzzleForJournal = Journal.getPortfolio().getPortfolio()
						.get(i);
				journalPageView = Journal.getJournalPage(
						currentPuzzleForJournal).getJournalPageView();
				if (i == 0)
					journalPageView.getPreviousButton().setEnabled(false);
				else
					journalPageView.getPreviousButton().setEnabled(true);
				if (i == indexView.getPortfolioPuzzlesButtons().size() - 1)
					journalPageView.getNextButton().setEnabled(false);
				else
					journalPageView.getNextButton().setEnabled(true);
				journalPageView.addActionListeners(this);
				cards.add((Component) journalPageView, "JournalPageView");
				cardLayout.show(cards, "JournalPageView");
				lastSource = "portIndex";
			}
		}

		// JOURNAL
		if (source == journalPageView.getEditButton()) {
			new EditNotesDialog(currentPuzzleForJournal);
		}

		if (source == journalPageView.getPortfolioButton()) {

			currentPuzzleForJournal.switchPortfolioState();

			if (currentPuzzleForJournal.isInPortfolio()) {
				Journal.addToPortfolio(currentPuzzleForJournal);
			}

			else {
				Journal.removeFromPortfolio(currentPuzzleForJournal);
			}

			journalPageView = Journal.getJournalPage(currentPuzzleForJournal)
					.getJournalPageView();
			journalPageView.addActionListeners(this);
			cards.add((Component) journalPageView, "JournalPageView");
			cardLayout.show(cards, "JournalPageView");
		}

		if (source == journalPageView.getExitButton()) {
			if (lastSource == "Puzzle")
				cardLayout.show(cards, "Puzzle");
			else if (lastSource == "chrIndex" || lastSource == "portIndex"
					|| lastSource == "storylineIndex") {
				indexView = Journal.getIndexPage(game).getIndexPageView();
				indexView.addActionListeners(this);
				cards.add((Component) indexView, "JournalIndexView");
				cardLayout.show(cards, "JournalIndexView");
			} else
				cardLayout.show(cards, "Homepage");
		}

		if (source == journalPageView.getNextButton()) {

			if (lastSource == "storylineIndex") {
				currentPuzzleForJournal = currentStorylineForJournal
						.getFollowingPuzzleInAttempted(currentPuzzleForJournal);
				journalPageView = Journal.getJournalPage(
						currentPuzzleForJournal).getJournalPageView();
				if (currentStorylineForJournal
						.getFollowingPuzzleInAttempted(currentPuzzleForJournal) == null)
					journalPageView.getNextButton().setEnabled(false);
				else
					journalPageView.getNextButton().setEnabled(true);

				journalPageView.getPreviousButton().setEnabled(true);
			}

			else if (lastSource == "chrIndex") {
				currentPuzzleForJournal = Journal
						.getFollowingPuzzleInChronology(currentPuzzleForJournal);
				journalPageView = Journal.getJournalPage(
						currentPuzzleForJournal).getJournalPageView();
				if (Journal
						.getFollowingPuzzleInChronology(currentPuzzleForJournal) == null)
					journalPageView.getNextButton().setEnabled(false);
				else
					journalPageView.getNextButton().setEnabled(true);

				journalPageView.getPreviousButton().setEnabled(true);
			}

			else if (lastSource == "portIndex") {
				if (currentPuzzleForJournal.isInPortfolio()) {
					currentPuzzleForJournal = Journal.getPortfolio()
							.getFollowingPuzzleInPortfolio(
									currentPuzzleForJournal);
					journalPageView = Journal.getJournalPage(
							currentPuzzleForJournal).getJournalPageView();
					if (Journal.getPortfolio().getFollowingPuzzleInPortfolio(
							currentPuzzleForJournal) == null)
						journalPageView.getNextButton().setEnabled(false);
					else
						journalPageView.getNextButton().setEnabled(true);

					journalPageView.getPreviousButton().setEnabled(true);
				}

				else {
					currentPuzzleForJournal = Journal.getPortfolio()
							.getPortfolio().get(0);
					journalPageView = Journal.getJournalPage(
							currentPuzzleForJournal).getJournalPageView();
					journalPageView.getPreviousButton().setEnabled(false);
					if (Journal.getPortfolio().getPortfolio().size() == 1)
						journalPageView.getNextButton().setEnabled(false);
					else
						journalPageView.getNextButton().setEnabled(true);

					journalPageView.getPreviousButton().setEnabled(false);
				}

			}

			else { // / last source is puzzle
				currentPuzzleForJournal = currentStorylineForJournal
						.getFollowingPuzzleInAttempted(currentPuzzleForJournal);
				journalPageView = Journal.getJournalPage(
						currentPuzzleForJournal).getJournalPageView();
				if (currentStorylineForJournal
						.getFollowingPuzzleInAttempted(currentPuzzleForJournal) == null)
					journalPageView.getNextButton().setEnabled(false);
				else
					journalPageView.getNextButton().setEnabled(true);

				journalPageView.getPreviousButton().setEnabled(true);
			}

			journalPageView.addActionListeners(this);
			cards.add((Component) journalPageView, "JournalPageView");
			cardLayout.show(cards, "JournalPageView");
			playAudio(getClass().getResource("/jpgame/audio/Page.wav")
					.getPath());
		}

		if (source == journalPageView.getPreviousButton()) {
			if (lastSource == "storylineIndex") {

				currentPuzzleForJournal = currentStorylineForJournal
						.getPreviousPuzzleInAttempted(currentPuzzleForJournal);
				journalPageView = Journal.getJournalPage(
						currentPuzzleForJournal).getJournalPageView();
				if (currentStorylineForJournal
						.getPreviousPuzzleInAttempted(currentPuzzleForJournal) == null)
					journalPageView.getPreviousButton().setEnabled(false);
				else
					journalPageView.getPreviousButton().setEnabled(true);

				journalPageView.getNextButton().setEnabled(true);
			}

			else if (lastSource == "chrIndex") {
				currentPuzzleForJournal = Journal
						.getPreviousPuzzleInChronology(currentPuzzleForJournal);
				journalPageView = Journal.getJournalPage(
						currentPuzzleForJournal).getJournalPageView();
				if (Journal
						.getPreviousPuzzleInChronology(currentPuzzleForJournal) == null)
					journalPageView.getPreviousButton().setEnabled(false);
				else
					journalPageView.getPreviousButton().setEnabled(true);

				journalPageView.getNextButton().setEnabled(true);
			}

			else if (lastSource == "portIndex") {
				if (currentPuzzleForJournal.isInPortfolio()) {
					currentPuzzleForJournal = Journal.getPortfolio()
							.getPreviousPuzzleInPortfolio(
									currentPuzzleForJournal);
					journalPageView = Journal.getJournalPage(
							currentPuzzleForJournal).getJournalPageView();
					if (Journal.getPortfolio().getPreviousPuzzleInPortfolio(
							currentPuzzleForJournal) == null)
						journalPageView.getPreviousButton().setEnabled(false);
					else
						journalPageView.getPreviousButton().setEnabled(true);
					journalPageView.getNextButton().setEnabled(true);
				} else {
					currentPuzzleForJournal = Journal.getPortfolio()
							.getPortfolio().get(0);
					journalPageView = Journal.getJournalPage(
							currentPuzzleForJournal).getJournalPageView();
					journalPageView.getPreviousButton().setEnabled(false);
					if (Journal.getPortfolio().getPortfolio().size() == 1)
						journalPageView.getNextButton().setEnabled(false);
					else
						journalPageView.getNextButton().setEnabled(true);
				}
			}

			else { // last source is puzzle
				currentPuzzleForJournal = currentStorylineForJournal
						.getPreviousPuzzleInAttempted(currentPuzzleForJournal);
				journalPageView = Journal.getJournalPage(
						currentPuzzleForJournal).getJournalPageView();
				if (currentStorylineForJournal
						.getPreviousPuzzleInAttempted(currentPuzzleForJournal) == null)
					journalPageView.getPreviousButton().setEnabled(false);
				else
					journalPageView.getPreviousButton().setEnabled(true);

				journalPageView.getNextButton().setEnabled(true);

			}
			journalPageView.addActionListeners(this);
			cards.add((Component) journalPageView, "JournalPageView");
			cardLayout.show(cards, "JournalPageView");
			playAudio(getClass().getResource("/jpgame/audio/Page.wav")
					.getPath());
		}
	}

	// Method for updating the Journal view
	// void updateJournalView() {
	// if (currentPuzzleForJournal != null) {
	// journalPageView = Journal
	// .getJournalPage(currentPuzzleForJournal).getJournalPageView();
	// if (currentStorylineForJournal
	// .getPreviousPuzzleInAttempted(currentPuzzleForJournal) == null)
	// journalPageView.getPreviousButton().setEnabled(false);
	// if (currentStorylineForJournal
	// .getFollowingPuzzleInAttempted(currentPuzzleForJournal) == null)
	// journalPageView.getNextButton().setEnabled(false);
	// journalPageView.addActionListeners(this);
	// journalPageView.getAddToPortfolioButton().addActionListener(this);
	// if (lastSource != "Success" && lastSource != "Win") {
	// cards.add((Component) journalPageView, "JournalPageView");
	// cardLayout.show(cards, "JournalPageView");
	// }
	// }
	// }

	// =============================================
	// Inner class to play back the data from the audio file.
//	private class PlayThread extends Thread {
//		byte tempBuffer[] = new byte[10000];
//
//		public void run() {
//			try {
//				sourceDataLine.open(audioFormat);
//				sourceDataLine.start();
//
//				int cnt;
//				while ((cnt = audioInputStream.read(tempBuffer, 0,
//						tempBuffer.length)) != -1) {
//					if (cnt > 0) {
//						sourceDataLine.write(tempBuffer, 0, cnt);
//					}
//				}
//				sourceDataLine.drain();
//				sourceDataLine.close();
//			} catch (Exception e) {
//				System.out.println("Cannot play sound!");
//			}
//		}
//	}
	
	// Method for playing the audio
	private void playAudio(String fileName) {
		ExecutorService exec = Executors.newCachedThreadPool();
		exec.execute( new AudioPlayingTask( fileName));
		exec.shutdown();
	}
	
	private class AudioPlayingTask implements Runnable{

		// PROPERTIES
		private byte[] tempBuffer = new byte[10000];
		
		// CONSTRUCTOR
		public AudioPlayingTask( String fileName) {
			try {
				File soundFile = new File(fileName);
				audioInputStream = AudioSystem.getAudioInputStream(soundFile);
				audioFormat = audioInputStream.getFormat();
				DataLine.Info dataLineInfo = new DataLine.Info(
						SourceDataLine.class, audioFormat);
				sourceDataLine = (SourceDataLine) AudioSystem.getLine(dataLineInfo);
			} catch (Exception e) {
				System.out.println("Cannot play sound!");
				e.printStackTrace();
			}
//			finally {
//				try {
//					audioInputStream.close();
//				} catch (IOException e) {
//					System.out.println("Cannot close audioInputStream!");
//					e.printStackTrace();
//				}
//			}
			
		}
		
		// METHODS	
		@Override
		public void run() {
			try {
				sourceDataLine.open(audioFormat);
				sourceDataLine.start();

				int len;
				while ((len = audioInputStream.read(tempBuffer, 0,
						tempBuffer.length)) != -1) {
					if (len > 0) {
						sourceDataLine.write(tempBuffer, 0, len);
					}
				}
				sourceDataLine.drain();
				//sourceDataLine.close();
			} catch (Exception e) {
				System.out.println("Cannot play sound!");
				e.printStackTrace();
			}
		}
		
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	// Overrides the window closing method
	public void windowClosing(WindowEvent e) {
		setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
		new ExitDialog(this);
	}

	// Empty definitions for WindowListener interface methods
	public void windowDeactivated(WindowEvent e) {
	}

	public void windowActivated(WindowEvent e) {
	}

	public void windowDeiconified(WindowEvent e) {
	}

	public void windowIconified(WindowEvent e) {
	}

	public void windowClosed(WindowEvent e) {
	}

	public void windowOpened(WindowEvent e) {
		
		// All storylines are completed
		if (game.getStorylines().size() == game.getFinishedStorylines().size()) 
			new NoPuzzlesToPlayDialog();
	}
} // End of GameFrame
