package jpgame.game;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import jpgame.JPGColorLibrary;
import jpgame.JPGComponent;

/**
 * GameView - This class is a view class for the game model class. It has a
 * JScrollPane containing set of JPanels with the storylines JPanels and it has
 * JPanel with the title of the game and Tutorials and Journal JButtons
 * 
 * @author Anisa Llaveshi
 * @version 1.00 2014/4/28
 */

public class HomepageView extends JPanel implements JPGHomepageView, Serializable {

	//Serial NO
	private static final long serialVersionUID = -2809472082229889869L;

	//PROPERTIES
	
	// ArrayList of JPanel-s is created to keep each of the JPanels of the not
	// started, in progress and finished storylines
	private ArrayList<JPanel> storylinesInProgress;
	private ArrayList<JPanel> storylinesNotStarted;
	private ArrayList<JPanel> finishedStorylines;

	// JPanels to keep the set of not started, in progress, and finished
	// storylines
	private JPanel storylinesNotStartedPanel;
	private JPanel storylinesInProgressPanel;
	private JPanel finishedStorylinesPanel;

	// JPanel to keep all JPanels of all the sets of storylines
	private JPanel allStorylinesPanel;

	// Buttons for the Storyline, journal and tutorials
	private JButton storylineButton;
	private JButton journalButton;
	private JButton tutorialsButton;

	// ArrayList of JButton-s to keep the set of buttons of each set of
	// storylines, the ones not started, in progress and finished
	private ArrayList<JButton> storylinesInProgressButtons;
	private ArrayList<JButton> storylinesNotStartedButtons;
	private ArrayList<JButton> finishedStorylinesButtons;

	private JLabel welcomeLabel;
	private JLabel storylineTitle;

	// JPanels to keep the set of buttons, and welcomeLabel
	private JPanel buttonsPanel;
	private JPanel welcomePanel;

	private JTextPane description;
	private JProgressBar progressBar;

	// JScrollPane to keep the JPanel of all the set of storylines
	private JScrollPane storylinesPane;

	// Constructor
	public HomepageView(Game game) {

		// The dimension of the panel is set to fit the size of the screen
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setPreferredSize(screenSize);

		// the layout of the panel is set to be BoxLayout
		setLayout(new BorderLayout());

		// The welcome label is created and its font and color are changed
		welcomeLabel = new JLabel("Welcome to Java Programming Game",
				JLabel.CENTER);
		welcomeLabel.setFont(new Font("Segoe UI", Font.BOLD | Font.ITALIC, 48));
		welcomeLabel.setForeground(JPGColorLibrary.BACKGROUND_WIN_VIEW);

		welcomePanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 10));
		welcomePanel.add(welcomeLabel);

		// JPanels in BoxLayout are created for each of the sets of storylines,
		// the ones not started yet, the ones finished and the ones in progress
		// and their background color is changed
		storylinesNotStartedPanel = new JPanel();
		storylinesNotStartedPanel.setLayout(new BoxLayout(
				storylinesNotStartedPanel, BoxLayout.Y_AXIS));
		storylinesNotStartedPanel.setOpaque(true);
		storylinesNotStartedPanel.setBackground(JPGColorLibrary.BACKGROUND_NOT_STARTED_STRL);
		finishedStorylinesPanel = new JPanel();
		finishedStorylinesPanel.setLayout(new BoxLayout(
				finishedStorylinesPanel, BoxLayout.Y_AXIS));
		finishedStorylinesPanel.setOpaque(true);
		finishedStorylinesPanel.setBackground(JPGColorLibrary.BACKGROUND_FINISHED_STRL);
		storylinesInProgressPanel = new JPanel();
		storylinesInProgressPanel.setLayout(new BoxLayout(
				storylinesInProgressPanel, BoxLayout.Y_AXIS));
		storylinesInProgressPanel.setOpaque(true);
		storylinesInProgressPanel.setBackground(JPGColorLibrary.BACKGROUND_STRL);

		// ArrayLists of JButtons for each separate sets of Storylines are
		// created
		storylinesNotStartedButtons = new ArrayList<JButton>();
		storylinesInProgressButtons = new ArrayList<JButton>();
		finishedStorylinesButtons = new ArrayList<JButton>();

		// STORYLINES IN PROGRESS

		// an ArrayList of JPanels is created for started storylines and
		// for each storyline in this set of storylines in progress
		// a JPanel is added to this ArrayList
		storylinesInProgress = new ArrayList<JPanel>();
		for (int i = 0; i < game.getStorylinesInProgress().size(); i++) {
			storylinesInProgress.add(new JPanel(new BorderLayout()));
			storylinesInProgress.get(i).setBackground(JPGColorLibrary.BACKGROUND_FINISHED_STRL);
		}

		// For each storyline, in its respective JPanel there is created and
		// added a button with an image representing that storyline,
		// a label with the name of the storyline
		// and a text area with the description of the storyline.
		for (int i = 0; i < storylinesInProgress.size(); i++) {
			storylineButton = new JButton();
			storylineButton.setPreferredSize(new Dimension(50, 50));
			storylineButton.setToolTipText("Click here to continue playing!");
			getStorylinesInProgressButtons().add(storylineButton);
			storylineButton.setPreferredSize(new Dimension(150, 150));

			// The image is scaled to fit the storylineButton's size
			ImageIcon picture;
			picture = game.getStorylinesInProgress().get(i).getPicture();
			Image img = picture.getImage();
			Image newimg = img.getScaledInstance(150, 150,
					java.awt.Image.SCALE_SMOOTH);
			picture = new ImageIcon(newimg);
			storylineButton.setIcon(picture);

			// JLabel with the title of the storyline
			storylineTitle = new JLabel(game.getStorylinesInProgress().get(i)
					.getName()
					+ " (In Progress)", JLabel.CENTER);
			storylineTitle.setFont(new Font("Segoe UI", Font.BOLD, 20));
			storylineTitle.setForeground(JPGColorLibrary.BACKGROUND_WIN_VIEW);

			// JTextArea for the description of the storyline
			description = new JTextPane();
			description.setText(game.getStorylinesInProgress().get(i)
					.getDescription());
			description.setFont(new Font("Segoue UI", Font.ITALIC, 12));
			description.setEditable(false);
			description.setBackground(JPGColorLibrary.BACKGROUND_DESCRP);
			JScrollPane descriptionPane = new JScrollPane(description);

			// For the storylines in progress a progress bar, shows the staus of
			// the progress
			progressBar = new JProgressBar(0, game.getStorylinesInProgress()
					.get(i).getPuzzles().size());
			progressBar.setValue(game.getStorylinesInProgress().get(i)
					.getFinishedPuzzles().size());
			progressBar.setStringPainted(true);
			progressBar.setForeground(JPGColorLibrary.JPG_CRIMSON);

			// The storylineButton, storylineTitle, and descriptionPane are
			// added to the JPanel of each storyline
			storylinesInProgress.get(i).add(storylineButton,
					BorderLayout.LINE_START);
			storylinesInProgress.get(i).add(storylineTitle,
					BorderLayout.PAGE_START);
			storylinesInProgress.get(i).add(descriptionPane,
					BorderLayout.CENTER);
			storylinesInProgress.get(i).add(progressBar, BorderLayout.PAGE_END);
		}

		// FINISHED STORYLINES

		// an ArrayList of JPanels is created for finished storylines and
		// for each storyline in this set of finished storylines
		// a JPanel is added to this ArrayList
		finishedStorylines = new ArrayList<JPanel>();
		for (int i = 0; i < game.getFinishedStorylines().size(); i++) {
			finishedStorylines.add(new JPanel(new BorderLayout()));
			finishedStorylines.get(i).setBackground(JPGColorLibrary.BACKGROUND_STRL);
		}

		// For each storyline, in its respective panel there is created and
		// added a button with an image
		// representing that storyline, a label with the name of the storyline
		// and a text area with the description of the storyline.
		for (int i = 0; i < game.getFinishedStorylines().size(); i++) {
			storylineButton = new JButton();
			finishedStorylinesButtons.add(storylineButton);
			storylineButton.setPreferredSize(new Dimension(150, 150));
			storylineButton
					.setToolTipText("You have completed this storyline! Check the journal for the records.");

			// The picture of the storyline is scaled to fit the size of the
			// storylineButton
			ImageIcon picture;
			picture = game.getFinishedStorylines().get(i).getPicture();
			Image img = picture.getImage();
			Image newimg = img.getScaledInstance(150, 150,
					java.awt.Image.SCALE_SMOOTH);
			picture = new ImageIcon(newimg);
			storylineButton.setIcon(picture);

			// JLabel with the title of the storyline
			storylineTitle = new JLabel(game.getFinishedStorylines().get(i)
					.getName()
					+ " (Finished)", JLabel.CENTER);
			storylineTitle.setFont(new Font("Segoe UI", Font.BOLD, 20));
			storylineTitle.setForeground(JPGColorLibrary.BACKGROUND_WIN_VIEW);

			// JTextPane with the description of the storyline
			description = new JTextPane();
			description.setText(game.getFinishedStorylines().get(i)
					.getDescription());
			description.setFont(new Font("Segoue UI", Font.ITALIC, 12));
			description.setBackground(JPGColorLibrary.BACKGROUND_DESCRP);
			description.setEditable(false);
			JScrollPane descriptionPane = new JScrollPane(description);

			// The storylineButton, storylineTitle, and descriptionPane are
			// added to the JPanel of each storyline in the set of finished
			// storylines
			finishedStorylines.get(i).add(storylineButton,
					BorderLayout.LINE_START);
			finishedStorylines.get(i).add(storylineTitle,
					BorderLayout.PAGE_START);
			finishedStorylines.get(i).add(descriptionPane, BorderLayout.CENTER);
		}

		// NOT STARTED STORYLINES

		// an ArrayList of JPanels is created for not started storylines and
		// for each storyline in this set of not started storylines
		// a JPanel is added to this ArrayList
		storylinesNotStarted = new ArrayList<JPanel>();

		for (int i = 0; i < game.getNotStartedStorylines().size(); i++) {
			storylinesNotStarted.add(new JPanel(new BorderLayout()));
			storylinesNotStarted.get(i).setBackground(JPGColorLibrary.BACKGROUND_NOT_STARTED_STRL);
		}

		// For each storyline, in its respective panel there is created and
		// added a button with an image
		// representing that storyline, a label with the name of the storyline
		// and a text area with the description of the storyline.
		for (int i = 0; i < game.getNotStartedStorylines().size(); i++) {
			storylineButton = new JButton();
			getStorylinesNotStartedButtons().add(storylineButton);
			storylineButton.setPreferredSize(new Dimension(150, 150));
			storylineButton
					.setToolTipText("Click here to start a new adventure!");

			// The picture of the storyline is scaled to fit storylineButton's
			// size
			ImageIcon picture;
			picture = game.getNotStartedStorylines().get(i).getPicture();
			Image img = picture.getImage();
			Image newimg = img.getScaledInstance(150, 150,
					java.awt.Image.SCALE_SMOOTH);
			picture = new ImageIcon(newimg);
			storylineButton.setIcon(picture);

			// JLabel with the title of the storyline
			storylineTitle = new JLabel(game.getNotStartedStorylines().get(i)
					.getName()
					+ " (Not Started)", JLabel.CENTER);
			storylineTitle.setFont(new Font("Segoe UI", Font.ITALIC, 20));
			storylineTitle.setForeground(JPGColorLibrary.BACKGROUND_WIN_VIEW);

			// JTextPane with the description of the storyline
			description = new JTextPane();
			description.setText(game.getNotStartedStorylines().get(i)
					.getDescription());
			description.setFont(new Font("Segoue UI", Font.ITALIC, 12));
			description.setBackground(JPGColorLibrary.BACKGROUND_DESCRP);
			description.setEditable(false);
			JScrollPane descriptionPane = new JScrollPane(description);

			// The storylineButton, storylineTitle, and descriptionPane are
			// added to the JPanel of each storyline in the set of not started
			// storylines
			storylinesNotStarted.get(i).add(storylineButton,
					BorderLayout.LINE_START);
			storylinesNotStarted.get(i).add(storylineTitle,
					BorderLayout.PAGE_START);
			storylinesNotStarted.get(i).add(descriptionPane,
					BorderLayout.CENTER);
		}

		// The set of the panels of the storylines in progress is added to the
		// storylinesInProgressPanel
		for (int i = 0; i < storylinesInProgress.size(); i++) {
			storylinesInProgressPanel.add(storylinesInProgress.get(i));

		}

		// The set of the panels of the finished storylines is added to the
		// finishedStorylinesPanel
		for (int i = 0; i < finishedStorylines.size(); i++) {
			finishedStorylinesPanel.add(finishedStorylines.get(i));
		}

		// The set of the panels of the storylines not started is added to the
		// storylinesNotStartedPanel
		for (int i = 0; i < storylinesNotStarted.size(); i++) {
			storylinesNotStartedPanel.add(storylinesNotStarted.get(i));
		}

		// A JPanel for the buttons of the journal and tutorial buttons and the
		// respective JButtons are created and their style is formatted
		buttonsPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 150, 10));

		journalButton = new JButton("Journal");
		journalButton.setFont(new Font("Segoe UI", Font.BOLD, 18));
		journalButton.setPreferredSize(new Dimension(150, 50));
		journalButton.setBackground(JPGColorLibrary.JPG_CRIMSON);
		journalButton.setForeground(JPGColorLibrary.PURE_WHITE);

		tutorialsButton = new JButton("Tutorials");
		tutorialsButton.setFont(new Font("Segoe UI", Font.BOLD, 18));
		tutorialsButton.setPreferredSize(new Dimension(150, 50));
		tutorialsButton.setBackground(JPGColorLibrary.JPG_CRIMSON);
		tutorialsButton.setForeground(JPGColorLibrary.PURE_WHITE);

		// The JButtons of the journal and tutorial are added to the
		// buttonsPanel
		buttonsPanel.add(getJournalButton());
		buttonsPanel.add(getTutorialsButton());

		// A JPanel to keep all JPanels of all the sets of storylines is created
		// and these JPanels are added to it
		allStorylinesPanel = new JPanel();
		allStorylinesPanel.setLayout(new BoxLayout(allStorylinesPanel,
				BoxLayout.Y_AXIS));
		allStorylinesPanel.add(storylinesInProgressPanel);
		allStorylinesPanel.add(storylinesNotStartedPanel);
		allStorylinesPanel.add(finishedStorylinesPanel);

		// JScrollPane to keep the allStorylinesPanel
		storylinesPane = new JScrollPane(allStorylinesPanel);

		// The welcomePanel, storylinesPanel and buttonsPanel are added to the
		// general GameView JPanel
		add(welcomePanel, BorderLayout.PAGE_START);
		add(storylinesPane, BorderLayout.CENTER);
		add(buttonsPanel, BorderLayout.PAGE_END);

	}

	// Methods

	// This method overrides JPGView updateView methods and updates the GameView
	@Override
	public void updateView(JPGComponent game) {

		if (game instanceof Game) {
			storylinesNotStartedPanel.removeAll();
			finishedStorylinesPanel.removeAll();
			storylinesInProgressPanel.removeAll();
			
			storylinesNotStartedButtons = new ArrayList<JButton>();
			storylinesInProgressButtons = new ArrayList<JButton>();
			finishedStorylinesButtons = new ArrayList<JButton>();
			
			storylinesInProgress = new ArrayList<JPanel>();
			for (int i = 0; i < ((Game) game).getStorylinesInProgress().size(); i++) {
				storylinesInProgress.add(new JPanel(new BorderLayout()));
				storylinesInProgress.get(i).setBackground(JPGColorLibrary.BACKGROUND_FINISHED_STRL);
			}
			
			for (int i = 0; i < storylinesInProgress.size(); i++) {
				storylineButton = new JButton();
				storylineButton.setPreferredSize(new Dimension(50, 50));
				storylineButton.setToolTipText("Click here to continue playing!");
				getStorylinesInProgressButtons().add(storylineButton);
				storylineButton.setPreferredSize(new Dimension(150, 150));

				// The image is scaled to fit the storylineButton's size
				ImageIcon picture;
				picture = ((Game) game).getStorylinesInProgress().get(i).getPicture();
				Image img = picture.getImage();
				Image newimg = img.getScaledInstance(150, 150,
						java.awt.Image.SCALE_SMOOTH);
				picture = new ImageIcon(newimg);
				storylineButton.setIcon(picture);

				// JLabel with the title of the storyline
				storylineTitle = new JLabel(((Game) game).getStorylinesInProgress().get(i)
						.getName()
						+ " (In Progress)", JLabel.CENTER);
				storylineTitle.setFont(new Font("Segoe UI", Font.BOLD, 20));
				storylineTitle.setForeground(JPGColorLibrary.BACKGROUND_WIN_VIEW);

				// JTextArea for the description of the storyline
				description = new JTextPane();
				description.setText(((Game) game).getStorylinesInProgress().get(i)
						.getDescription());
				description.setFont(new Font("Segoue UI", Font.ITALIC, 12));
				description.setEditable(false);
				description.setBackground(JPGColorLibrary.BACKGROUND_DESCRP);
				JScrollPane descriptionPane = new JScrollPane(description);

				// For the storylines in progress a progress bar, shows the staus of
				// the progress
				progressBar = new JProgressBar(0, ((Game) game).getStorylinesInProgress()
						.get(i).getPuzzles().size());
				progressBar.setValue(((Game) game).getStorylinesInProgress().get(i)
						.getFinishedPuzzles().size());
				progressBar.setStringPainted(true);
				progressBar.setForeground(JPGColorLibrary.JPG_CRIMSON);

				// The storylineButton, storylineTitle, and descriptionPane are
				// added to the JPanel of each storyline
				storylinesInProgress.get(i).add(storylineButton,
						BorderLayout.LINE_START);
				storylinesInProgress.get(i).add(storylineTitle,
						BorderLayout.PAGE_START);
				storylinesInProgress.get(i).add(descriptionPane,
						BorderLayout.CENTER);
				storylinesInProgress.get(i).add(progressBar, BorderLayout.PAGE_END);
			}

			
			finishedStorylines = new ArrayList<JPanel>();
			for (int i = 0; i < ((Game) game).getFinishedStorylines().size(); i++) {
				finishedStorylines.add(new JPanel(new BorderLayout()));
				finishedStorylines.get(i).setBackground(JPGColorLibrary.BACKGROUND_STRL);
			}

			// For each storyline, in its respective panel there is created and
			// added a button with an image
			// representing that storyline, a label with the name of the storyline
			// and a text area with the description of the storyline.
			for (int i = 0; i < ((Game) game).getFinishedStorylines().size(); i++) {
				storylineButton = new JButton();
				finishedStorylinesButtons.add(storylineButton);
				storylineButton.setPreferredSize(new Dimension(150, 150));
				storylineButton
						.setToolTipText("You have completed this storyline! Check the journal for the records.");

				// The picture of the storyline is scaled to fit the size of the
				// storylineButton
				ImageIcon picture;
				picture = ((Game) game).getFinishedStorylines().get(i).getPicture();
				Image img = picture.getImage();
				Image newimg = img.getScaledInstance(150, 150,
						java.awt.Image.SCALE_SMOOTH);
				picture = new ImageIcon(newimg);
				storylineButton.setIcon(picture);

				// JLabel with the title of the storyline
				storylineTitle = new JLabel(((Game) game).getFinishedStorylines().get(i)
						.getName()
						+ " (Finished)", JLabel.CENTER);
				storylineTitle.setFont(new Font("Segoe UI", Font.BOLD, 20));
				storylineTitle.setForeground(JPGColorLibrary.BACKGROUND_WIN_VIEW);

				// JTextPane with the description of the storyline
				description = new JTextPane();
				description.setText(((Game) game).getFinishedStorylines().get(i)
						.getDescription());
				description.setFont(new Font("Segoue UI", Font.ITALIC, 12));
				description.setBackground(JPGColorLibrary.BACKGROUND_DESCRP);
				description.setEditable(false);
				JScrollPane descriptionPane = new JScrollPane(description);

				// The storylineButton, storylineTitle, and descriptionPane are
				// added to the JPanel of each storyline in the set of finished
				// storylines
				finishedStorylines.get(i).add(storylineButton,
						BorderLayout.LINE_START);
				finishedStorylines.get(i).add(storylineTitle,
						BorderLayout.PAGE_START);
				finishedStorylines.get(i).add(descriptionPane, BorderLayout.CENTER);
			}

			// NOT STARTED STORYLINES

			// an ArrayList of JPanels is created for not started storylines and
			// for each storyline in this set of not started storylines
			// a JPanel is added to this ArrayList
			storylinesNotStarted = new ArrayList<JPanel>();

			for (int i = 0; i < ((Game) game).getNotStartedStorylines().size(); i++) {
				storylinesNotStarted.add(new JPanel(new BorderLayout()));
				storylinesNotStarted.get(i).setBackground(JPGColorLibrary.BACKGROUND_NOT_STARTED_STRL);
			}

			// For each storyline, in its respective panel there is created and
			// added a button with an image
			// representing that storyline, a label with the name of the storyline
			// and a text area with the description of the storyline.
			for (int i = 0; i < ((Game) game).getNotStartedStorylines().size(); i++) {
				storylineButton = new JButton();
				getStorylinesNotStartedButtons().add(storylineButton);
				storylineButton.setPreferredSize(new Dimension(150, 150));
				storylineButton
						.setToolTipText("Click here to start a new adventure!");

				// The picture of the storyline is scaled to fit storylineButton's
				// size
				ImageIcon picture;
				picture = ((Game) game).getNotStartedStorylines().get(i).getPicture();
				Image img = picture.getImage();
				Image newimg = img.getScaledInstance(150, 150,
						java.awt.Image.SCALE_SMOOTH);
				picture = new ImageIcon(newimg);
				storylineButton.setIcon(picture);

				// JLabel with the title of the storyline
				storylineTitle = new JLabel(((Game) game).getNotStartedStorylines().get(i)
						.getName()
						+ " (Not Started)", JLabel.CENTER);
				storylineTitle.setFont(new Font("Segoe UI", Font.ITALIC, 20));
				storylineTitle.setForeground(JPGColorLibrary.BACKGROUND_WIN_VIEW);

				// JTextPane with the description of the storyline
				description = new JTextPane();
				description.setText(((Game) game).getNotStartedStorylines().get(i)
						.getDescription());
				description.setFont(new Font("Segoue UI", Font.ITALIC, 12));
				description.setBackground(JPGColorLibrary.BACKGROUND_DESCRP);
				description.setEditable(false);
				JScrollPane descriptionPane = new JScrollPane(description);

				// The storylineButton, storylineTitle, and descriptionPane are
				// added to the JPanel of each storyline in the set of not started
				// storylines
				storylinesNotStarted.get(i).add(storylineButton,
						BorderLayout.LINE_START);
				storylinesNotStarted.get(i).add(storylineTitle,
						BorderLayout.PAGE_START);
				storylinesNotStarted.get(i).add(descriptionPane,
						BorderLayout.CENTER);
			}

			// The set of the panels of the storylines in progress is added to the
			// storylinesInProgressPanel
			for (int i = 0; i < storylinesInProgress.size(); i++) {
				storylinesInProgressPanel.add(storylinesInProgress.get(i));

			}

			// The set of the panels of the finished storylines is added to the
			// finishedStorylinesPanel
			for (int i = 0; i < finishedStorylines.size(); i++) {
				finishedStorylinesPanel.add(finishedStorylines.get(i));
			}

			// The set of the panels of the storylines not started is added to the
			// storylinesNotStartedPanel
			for (int i = 0; i < storylinesNotStarted.size(); i++) {
				storylinesNotStartedPanel.add(storylinesNotStarted.get(i));
			}

			repaint();
		}
	}

	//Method for returning the journal button
	public JButton getJournalButton() {
		return journalButton;
	}

	//Method for returning tutorials button
	public JButton getTutorialsButton() {
		return tutorialsButton;
	}

	//Method for returning not started storylines buttons
	public ArrayList<JButton> getStorylinesNotStartedButtons() {
		return storylinesNotStartedButtons;
	}

	//Method for retuning in progress storylines buttons
	public ArrayList<JButton> getStorylinesInProgressButtons() {
		return storylinesInProgressButtons;
	}

	//Method for adding action listeners
	public void addActionListeners( ActionListener actionListener )
	{
		if (getStorylinesNotStartedButtons().size() != 0 && getStorylinesNotStartedButtons().get(0) != null & getStorylinesNotStartedButtons().get(0).getActionListeners().length == 0)
		{			
			for( JButton b : getStorylinesNotStartedButtons()){
				b.addActionListener( actionListener);
			}
			
			for( JButton b: getStorylinesInProgressButtons()){
				b.addActionListener( actionListener);
			}			
		}
		else if (getStorylinesInProgressButtons().get(0) != null & getStorylinesInProgressButtons().get(0).getActionListeners().length == 0)
		{			
			for( JButton b : getStorylinesNotStartedButtons()){
				b.addActionListener( actionListener);
			}
			
			for( JButton b: getStorylinesInProgressButtons()){
				b.addActionListener( actionListener);
			}			
		}
		
		if(journalButton.getActionListeners().length == 0)
		{
			journalButton.addActionListener( actionListener);
			tutorialsButton.addActionListener( actionListener);	
		}
	}

} //End of HomepageView
