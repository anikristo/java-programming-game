package jpgame.game;

import java.awt.BorderLayout;

import java.awt.Component;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import jpgame.JPGColorLibrary;

/**
 * OpeningView - This class is a view class for the opening of the game. It
 * contains JLabels in a JPanel writing Java Programming Game and a gif
 * animation as a loader
 * 
 * @author Anisa Llaveshi
 * @version 1.00 2014/4/28
 */
public class OpeningView extends JPanel {

	//Serial NO
	private static final long serialVersionUID = 9202306828776415193L;

	// Properties
	private JLabel java;
	private JLabel programming;
	private JLabel game;
	private JLabel loading;
	private JPanel javaProgrammingGame;

	// Constructor
	public OpeningView() {
		setLayout(new BorderLayout());
		setBackground(JPGColorLibrary.BACKGROUND_OPEN_VIEW);

		// JLabel for the writing "JAVA"
		java = new JLabel("JAVA");
		java.setForeground(JPGColorLibrary.PURE_WHITE);
		java.setFont(new Font("Segoe UI", Font.BOLD, 80));
		java.setAlignmentX(Component.CENTER_ALIGNMENT);

		// JLabel for the writing "PROGRAMMING"
		programming = new JLabel("PROGRAMMING", JLabel.CENTER);
		programming.setForeground(JPGColorLibrary.PURE_WHITE);
		programming.setFont(new Font("Segoe UI", Font.BOLD, 80));
		programming.setAlignmentX(Component.CENTER_ALIGNMENT);

		// JLabel for the writing "GAME"
		game = new JLabel("GAME", JLabel.CENTER);
		game.setForeground(JPGColorLibrary.PURE_WHITE);
		game.setFont(new Font("Segoe UI", Font.BOLD, 80));
		game.setAlignmentX(Component.CENTER_ALIGNMENT);

		// JLabel for the loader image
		loading = new JLabel(new ImageIcon(getClass().getResource(
				"/jpgame/img/loading.gif")));

		// JPanel containing the java, progrmaming and game JLabels
		javaProgrammingGame = new JPanel();
		javaProgrammingGame.setLayout(new BoxLayout(javaProgrammingGame,
				BoxLayout.Y_AXIS));
		javaProgrammingGame.setBackground(JPGColorLibrary.BACKGROUND_OPEN_VIEW);
		javaProgrammingGame.add(java);
		javaProgrammingGame.add(programming);
		javaProgrammingGame.add(game);

		javaProgrammingGame.setBorder(BorderFactory.createEmptyBorder(60, 10,
				10, 10));

		add(javaProgrammingGame, BorderLayout.CENTER);
		add(loading, BorderLayout.PAGE_END);
	}

} //End of OpeningView
