package jpgame.partner;

import javax.swing.ImageIcon;

import jpgame.puzzle.Puzzle;
import jpgame.storyline.Storyline;

/**
 * Partner - This class extends Game Character and it may implement different
 * interfaces representing a partner
 * 
 * @author Anisa Llaveshi
 * @version 1.00 2014/19/4
 */

public class Partner extends GameCharacter 
{
	//Serial NO
	private static final long serialVersionUID = 1205785996660504473L;
	
	//PROPERTIES
	private Storyline storyline;
	transient private PartnerView partnerView;
	
	// CONSTRUCTORS
	public Partner(String name, ImageIcon picture, Storyline storyline) {
		super(name, picture);
		
		this.setStoryline(storyline);
		partnerView = new PartnerView( this, storyline.getCurrentPuzzle());
		
	}

	public Partner(String name, Storyline storyline) {
		super(name, null);
		
		this.storyline = storyline;
		
		partnerView = new PartnerView( this, storyline.getCurrentPuzzle());
	}
	
	public Partner( String name, ImageIcon picture, Storyline storyline, Puzzle puzzle){
		super( name, picture);
		
		this.storyline = storyline;
		partnerView = new PartnerView( this, puzzle);
	}
	
	//METHODS
	
	//Method for returning the storyline
	public Storyline getStoryline() {
		return storyline;
	}

	//Method for setting the storyline
	public void setStoryline(Storyline storyline) {
		this.storyline = storyline;
		notifyViews();
	}

	//Method for returning the partner view
	public PartnerView getPartnerView() {
		if ( partnerView != null) {
			partnerView.updateView( this);
			return partnerView;
		}
		
		else if(storyline != null)
			return new PartnerView( this, storyline.getCurrentPuzzle());
		else 
			return null;
	}

	//Method for setting the partner view
	public void setPartnerView(PartnerView view) {
		this.partnerView = view;
		notifyViews();
	}

}//End of Partner
