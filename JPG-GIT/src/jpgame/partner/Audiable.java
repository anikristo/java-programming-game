package jpgame.partner;

import javax.sound.sampled.AudioFileFormat;

/**
 * Audiable - is an interface that makes possible for the implementing class to have a voice message or 
 * 			  anything else related to audio.
 * 
 * @author Anisa Llaveshi
 * @version 1.00 2012/4/4
 */

public interface Audiable 
{
	public AudioFileFormat getAudio();
	//shoudln't the parameter be AudioFileFormat, not File?
	public void setAudio(AudioFileFormat audioFile);
}
