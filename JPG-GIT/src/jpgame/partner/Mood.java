package jpgame.partner;

/**
 * Mood - is an enum type that has a variation of moods that the implementing
 * character might have.
 * 
 * @author Anisa Llaveshi
 * @version 1.00 2014/19/4
 */

public enum Mood {
	HAPPY, SAD, DISAPPOINTED, DUBIOUS, MARVELLED, ANGRY, PLAIN
}
