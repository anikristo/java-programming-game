/**
 * @author Ani Kristo
 * @version 1.0
 * 
 * 12 / 05 / 2014
 * 
 * DESCRIPTION: This dialog box will display the content of the next hint to be received from the user upon requesting it by 
 * 				pressing on "I want a new hint" button in the Partner Scene.
 */
package jpgame.partner.hints;

import java.awt.BorderLayout;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import jpgame.JPGColorLibrary;
import jpgame.JPGComponent;
import jpgame.puzzle.Puzzle;

public class NewHintDialog extends JDialog implements JPGComponent,
		ActionListener {

	//Serial NO
	private static final long serialVersionUID = -4527569004701539850L;

	// PROPERTIES
	private JLabel newHintLb;
	private JTextArea hintContent;
	private JPanel hintContentPanel;
	private JPanel buttonPane;
	private JButton okButton;

	// CONSTRUCTOR
	public NewHintDialog(Puzzle puzzle) {
		newHintLb = new JLabel("New Hint");
		newHintLb.setFont(new Font("Segoe UI", Font.BOLD | Font.ITALIC, 24));
		newHintLb.setForeground(JPGColorLibrary.JPG_CRIMSON);

		hintContent = new JTextArea();
		hintContent.setEditable(false);
		hintContent.setLineWrap(true);

		if (puzzle.getNextHint() != null) {
			hintContent.setText(puzzle.getNextHint().getText());
			puzzle.getNextHint().setReceived(true);
		} else {
			hintContent
					.setText("There are no available hints for this puzzle right now!");
			newHintLb.setText("No available Hints!");
		}

		hintContent.setFont(new Font("Segoe UI", Font.ITALIC, 14));
		hintContent.setForeground(JPGColorLibrary.TEXT_BLUE);
		hintContent.setOpaque(false);
		hintContent.setLineWrap(true);
		hintContent.setPreferredSize(new Dimension(500, 100));
		hintContent.setMargin(new Insets(10, 10, 10, 10));

		hintContentPanel = new JPanel();
		hintContentPanel.add(hintContent);

		okButton = new JButton("OK");
		okButton.setBackground(JPGColorLibrary.JPG_CRIMSON);
		okButton.setForeground(JPGColorLibrary.PURE_WHITE);
		okButton.addActionListener(this);

		buttonPane = new JPanel(new FlowLayout(FlowLayout.CENTER));
		buttonPane.add(okButton);

		setLayout(new BorderLayout());
		add(newHintLb, BorderLayout.NORTH);
		add(hintContentPanel, BorderLayout.CENTER);
		add(buttonPane, BorderLayout.SOUTH);

		pack();
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setTitle("New Hint");
		Dimension screensize = java.awt.Toolkit.getDefaultToolkit()
				.getScreenSize();
		setLocation(new Point((screensize.width - 500) / 2,
				(screensize.height - 500) / 2));

		setVisible(true);
	}

	//METHODS
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == okButton) {
			dispose();
		}
	}
}//End of NewHintDialog
