/**
 * @author Ani Kristo
 * @version 1.0
 * 
 * 12 / 05 / 2014
 * 
 * DESCRIPTION: This JDialog will show the list of all the collected hints from the user. It will display the Hint number and its
 * 				content. In case there are no hints collected, it will display a message saying so. The size of the frame will vary
 * 				proportionally with the screen size. On the other hand, if there are a lot of hints, it will simply pick the maximum size
 */
package jpgame.partner.hints;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;


import java.util.Vector;

import jpgame.JPGColorLibrary;
import jpgame.JPGComponent;
import jpgame.JPGView;
import jpgame.puzzle.Puzzle;

public class CollectedHints extends JDialog implements ActionListener, JPGView {

	
	//Serial NO
	private static final long serialVersionUID = 7781682446439249343L;
	// PROPERTIES
	private JPanel contentPanel;
	private JButton okButton;
	private JLabel collectedHintslb;
	private JPanel labelPanel;
	private JPanel buttonPane;
	private Vector<JPanel> hintPanels;
	private JPanel listOfHintsPanel;
	private JScrollPane scrollPane;
	private Puzzle puzzle;

	// CONSTRUCTOR
	// -----------------------------------------------------------------------------------------------------------------------------
	public CollectedHints(Puzzle puzzle) {

		this.puzzle = puzzle;

		hintPanels = new Vector<JPanel>();
		if (puzzle.getPreviousHints().size() == 0) {
			JPanel tmpPanel = new JPanel();
			JLabel tmpLabel = new JLabel("No Hints collected yet!");
			tmpLabel.setFont(new Font("Segoe UI", Font.ITALIC, 16));
			tmpLabel.setForeground(JPGColorLibrary.TEXT_BLUE);
			tmpPanel.add(tmpLabel);
			hintPanels.add(tmpPanel);
		}
		for (int i = 0; i < puzzle.getPreviousHints().size(); i++) {

			JPanel tmpPanel = new JPanel(new GridLayout(2, 1));

			JLabel tmpLabel = new JLabel("Hint #" + (i + 1));
			tmpLabel.setFont(new Font("Segoe UI", Font.BOLD | Font.ITALIC, 16));
			tmpLabel.setForeground(JPGColorLibrary.TEXT_BLUE);

			JTextPane tmpTA = new JTextPane();
			tmpTA.setText(puzzle.getPreviousHints().get(i).getText());
			tmpTA.setFont(new Font("Segoe UI", Font.ITALIC, 14));
			tmpTA.setEditable(false);
			tmpTA.setOpaque(false);

			tmpPanel.add(tmpLabel);
			tmpPanel.add(tmpTA);

			hintPanels.add(tmpPanel);
		}

		// add the panels to the scroll pane
		listOfHintsPanel = new JPanel(new GridLayout(puzzle.getPreviousHints()
				.size(), 1));
		for (JPanel panel : hintPanels) {
			listOfHintsPanel.add(panel);
		}
		scrollPane = new JScrollPane(listOfHintsPanel);

		collectedHintslb = new JLabel("Collected Hints");
		collectedHintslb.setFont(new Font("Segoe UI", Font.BOLD | Font.ITALIC,
				24));
		collectedHintslb.setForeground(JPGColorLibrary.JPG_CRIMSON);

		labelPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		labelPanel.add(collectedHintslb);

		contentPanel = new JPanel(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPanel.add(labelPanel, BorderLayout.NORTH);
		contentPanel.add(scrollPane, BorderLayout.CENTER);

		okButton = new JButton("OK");
		okButton.setBackground(JPGColorLibrary.JPG_CRIMSON);
		okButton.setForeground(JPGColorLibrary.PURE_WHITE);
		okButton.addActionListener(this);

		buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPane.add(okButton);

		setLayout(new BorderLayout());
		add(contentPanel, BorderLayout.CENTER);
		add(buttonPane, BorderLayout.SOUTH);

		// Get Screen size and determine the dimensions of the window
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension windowSize = new Dimension(screenSize.width / 2,
				screenSize.height / 3 * 2);

		pack();

		if (getHeight() < windowSize.height - 80)
			setBounds((screenSize.width - windowSize.width) / 2,
					(screenSize.height - getHeight()) / 2, windowSize.width,
					getHeight()); // esthetics
		else
			setBounds((screenSize.width - windowSize.width) / 2,
					(screenSize.height - windowSize.height) / 2,
					windowSize.width, windowSize.height); // centered

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		setTitle("Java Programming Game - Collected Hints");

		setVisible(true);
	}

	@Override
	public void updateView(JPGComponent comp) {
		if (comp instanceof Puzzle) {
			puzzle = (Puzzle) comp;
			new CollectedHints(puzzle);
		}

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == okButton) {
			super.dispose();
		}
	}

}//End of CollectedHints
