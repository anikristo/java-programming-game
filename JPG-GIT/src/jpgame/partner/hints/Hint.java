package jpgame.partner.hints;

/**
 * Hint	a class to hold information about hints
 *
 * @author Burcu Canakci
 * @version 1.00 2014/4/20
 */

import java.io.Serializable;

import jpgame.JPGComponent;
import jpgame.puzzle.Puzzle;

public class Hint implements JPGComponent, Serializable
{
	//Serial NO
	private static final long serialVersionUID = 3198684323830578547L;
	// PROPERTIES
	private String text;
	private Puzzle puzzle;
	private long revealingTime;
	protected boolean isReceived;

	// CONSTRUCTOR
	public Hint(Puzzle puzzle)
	{
		this.puzzle = puzzle;
		text = "";
		revealingTime = 0;
		isReceived = false;
	}

	// METHODS

	//Method for returning the text
	public String getText()
	{
		return text;
	}

	//Method for returning the puzzle
	public Puzzle getPuzzle()
	{
		return puzzle;
	}

	//Method for returning the time
	public long getTime()
	{
		return revealingTime;
	}

	//Method for returning isReceived value
	public boolean isReceived()
	{			
		return isReceived;
	}
		
	//Method for setting the text
	public void setText(String text)
	{
		this.text = text;
	}

	//Method for setting the time
	public void setTime(long revealingTime)
	{
		this.revealingTime = revealingTime;
	}

	//Method for setting isReceived value
	public void setReceived(boolean value)
	{
		isReceived = value;
	}

	// method to compare the puzzle's time with the hint's revealing time and
	// return the correct status
	public boolean canDisplay()
	{
		if (puzzle != null && puzzle.getTime() >= revealingTime)
			return true;
		return false;
	}

}//End of Hint
