package jpgame.partner;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;

import jpgame.JPGView;

/**
*JPGPartnerView.java
*
*JPGPartnerView application at JavaProgrammingGame

*Interface for creating partner views
*Contains methods that are required to be implemented in partner view instances
*
*Author: Ayse Berceste Dincer
*version 1.00 2014/21/7 
**/

public interface JPGPartnerView extends JPGView
{
	public JButton getExplainMissionButton();
	public JButton getGoBackButton();
	public JLabel  getTitle();
	public void    addActionListeners( ActionListener actionListener );
	public JButton getGetHintButton();
	public JButton getViewHintsButton();

} //End of JPGPartnerView
