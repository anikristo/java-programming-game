package jpgame.partner;
import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.ImageIcon;

import jpgame.JPGComponent;
import jpgame.JPGView;

/**
 * GameCharacter - GameCharacter is an abstract class and it is a JPGComponent.
 * 				   Each GameCharacter has a name and picture and related get and set methods.
 * 
 * @author Anisa Llaveshi
 * @version 1.00 2014/19/4
 */

public abstract class GameCharacter implements JPGComponent, Serializable
{
	//Serial NO
	private static final long serialVersionUID = -2900814274130849663L;
	
	//Properties
	private String name;
	private ImageIcon picture;
	private transient ArrayList<JPGView> views;
	
	//Constructor
	public GameCharacter (String name, ImageIcon picture)
	{
		this.name = name;
		this.picture = picture;
		views = new ArrayList<JPGView>();
	}
	
	//Method that returns name of the GameCharacter
	public String getName()
	{
		return name;
	}
	
	//Method that sets the name of the GameCharacter
	public void setName(String name)
	{
		this.name = name;
		notifyViews();
	}
	
	//Method that returns picture of the GameCharacter
	public ImageIcon getPicture()
	{
		return picture;
	}
	
	//Method tht sets picture of the GameCharacter
	public void setPicture(ImageIcon picture)
	{
		this.picture = picture;
		notifyViews();
	}
	
	//Method that adds GameCharacter view
	public void addView( JPGView view)
	{
		views.add( view);
		if ( view != null)
			view.updateView( this);
	}

	//Method that notifies change in GameCharacter views 
	public void notifyViews()
	{
		if ( views != null)
			for ( JPGView view : views)
				view.updateView( this);
	}

	//Method that removes GameCharacter view
	public void removeView( JPGView view)
	{
		views.remove( view);
	}

} //End of GameCharacter
