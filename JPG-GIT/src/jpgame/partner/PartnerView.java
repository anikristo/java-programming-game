package jpgame.partner;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import jpgame.JPGColorLibrary;
import jpgame.JPGComponent;
import jpgame.puzzle.Puzzle;

/**
 * PartnerView - This class is a view class for the Partner. It displays
 * partner's picture and related buttons
 * 
 * @author Anisa Llaveshi
 * @version 1.00 2014/4/28
 */

public class PartnerView extends JPanel implements JPGPartnerView 
{
	//Serial NO
	private static final long serialVersionUID = 7133595843881241171L;

	// PROPERTIES
	private Puzzle puzzle;
	private JLabel title;
	private ImageIcon image;
	private JPanel buttonsPanel;
	private JPanel goBackPanel;
	private JButton getHintButton;
	private JButton viewHintsButton;
	private JButton explainMissionButton;
	private JButton goBackButton;

	// CONSTRUCTOR
	@SuppressWarnings("unchecked")
	public PartnerView(Partner partner, Puzzle puzzle) {
		
		this.puzzle = puzzle;

		// The layout of this panel is set to BorderLayout
		setLayout(new BorderLayout());

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setPreferredSize(screenSize);
	

		// JLabel containing the name of the Partner
		title = new JLabel("My Partner: " + partner.getName(), JLabel.CENTER);
		title.setFont(new Font("Segoe UI", Font.BOLD, 48));
		title.setForeground(JPGColorLibrary.JPG_CRIMSON);

		// JLabel containing the picture of the partner
		if (partner instanceof Moody<?>) {
			image = (ImageIcon) ((Moody<ImageIcon>) partner)
					.getCurrentMoodExpression();
		} else
			image = partner.getPicture();

		JPanel imagePanel = new JPanel() {

			private static final long serialVersionUID = -7662713010650784133L;

			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				image.paintIcon(this, g,
						(this.getWidth() - image.getIconWidth()) / 2,
						(this.getHeight() - image.getIconHeight()) / 2);
			}
		};

		// New jButtons are created and modified accordingly to their functions
		// JButton to request a hint
		getHintButton = new JButton("I want a hint");
		getGetHintButton().setFont(new Font("Segoe UI", Font.BOLD, 20));
		getGetHintButton().setFont(new Font("Segoe UI", Font.BOLD, 20));
		getGetHintButton().setBackground(JPGColorLibrary.JPG_CRIMSON);
		getGetHintButton().setForeground(JPGColorLibrary.PURE_WHITE);

		// JButton to view the hints collected
		viewHintsButton = new JButton("View the hints collected");
		getViewHintsButton().setFont(new Font("Segoe UI", Font.BOLD, 20));
		getViewHintsButton().setBackground(JPGColorLibrary.JPG_CRIMSON);
		getViewHintsButton().setForeground(JPGColorLibrary.PURE_WHITE);

		// JButton to explain the mission again
		explainMissionButton = new JButton("Explain my mission again");
		getExplainMissionButton().setFont(new Font("Segoe UI", Font.BOLD, 20));
		getExplainMissionButton().setBackground(JPGColorLibrary.JPG_CRIMSON);
		getExplainMissionButton().setForeground(JPGColorLibrary.PURE_WHITE);

		// JButton to go back to the previouspage
		goBackButton = new JButton("Go back");
		getGoBackButton().setFont(new Font("Segoe UI", Font.BOLD, 20));
		getGoBackButton().setBackground(JPGColorLibrary.JPG_CRIMSON);
		getGoBackButton().setForeground(JPGColorLibrary.PURE_WHITE);

		// A new JPanel is created which contains the buttons previously created
		buttonsPanel = new JPanel(new GridLayout(3, 1, 0, 120));
		buttonsPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 20));
		buttonsPanel.add(getGetHintButton());
		buttonsPanel.add(getViewHintsButton());
		buttonsPanel.add(getExplainMissionButton());

		// A JPanel containing the goBack button
		goBackPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		goBackPanel.add(getGoBackButton());
		
		// Disable Buttons according to the hints Size
		if (puzzle.getAllHints().size() == 0) 
		{
			getHintButton.setEnabled(false);
			viewHintsButton.setEnabled(false);
		}

		// The relevant components are added to the panel
		add(title, BorderLayout.PAGE_START);
		add(imagePanel, BorderLayout.CENTER);
		add(buttonsPanel, BorderLayout.LINE_END);
		add(goBackPanel, BorderLayout.PAGE_END);

	}

	// METHODS

	// This method overrides JPGView updateView methods and updates the PartnerView
	@Override
	public void updateView(JPGComponent partner) {
		if (partner instanceof Partner) {
			puzzle = ((Partner) partner).getStoryline().getCurrentPuzzle();
		}
	}

	//Method for returning the title
	public JLabel getTitle() {
		return title;
	}
	
	//Method for returning the puzzle 
	public Puzzle getPuzzle() {
		return puzzle;
	}

	//Method for setting the puzzle
	public void setPuzzle(Puzzle puzzle) {
		this.puzzle = puzzle;
	}

	//Method for returning the getHints button
	public JButton getGetHintButton() {
		return getHintButton;
	}
	
	//Method for returning viewHints button
	public JButton getViewHintsButton() {
		return viewHintsButton;
	}
		
	//Method for returning explainMission button
	public JButton getExplainMissionButton() {
		return explainMissionButton;
	}
		
	//Method for returning goBack button
	public JButton getGoBackButton() {
		return goBackButton;
	}
		
	//JPGPartnerView method
	public void addActionListeners( ActionListener actionListener )
	{
		//Adds action listeners to all buttons
		if (getHintButton.getActionListeners().length == 0)
		{
			getHintButton.addActionListener( actionListener );
			viewHintsButton.addActionListener( actionListener );
			explainMissionButton.addActionListener( actionListener );
			goBackButton.addActionListener( actionListener );
		}
	}

} //End of PartnerView
