package jpgame.partner;

/**
 * Mood - is an interface that is implemented from the Partner class. It
 * provides a mood for this GameCharacter by making it give messages in form
 * of text, sounds or even animations, according to the game situation.
 * 
 * The generics will define the type of mood expression, i.e text, picture, audio, video, animation etc.
 * 
 * @author Anisa Llaveshi
 * @version 1.00 2014/19/4
 */

public interface Moody<Expression> {
	/*
	 * This should return a text representation of the message sent to the user according to the mood
	 */
	public String getMoodMessage(Mood mood);

	public Mood getMood();

	public Expression getMoodExpression(Mood mood);
	
	public Expression getCurrentMoodExpression();
	
	public void setMood(Mood mood);
	
	public void updateMood();
}
