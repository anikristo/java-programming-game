package jpgame.storyline;

import java.util.Vector;

import javax.swing.ImageIcon;

import jpgame.partner.Partner;
import jpgame.puzzle.Puzzle;

/**
 * Partner - This class extends Storyline and it has an additional property
 * passCode, which makes a storyline private.
 * 
 * @author Anisa Llaveshi
 * @version 1.00 2014/19/4
 */

public abstract class PrivateStoryline extends Storyline {
	
	//Serial NO
	private static final long serialVersionUID = 7686839174933751314L;

	// PROPERTIES
	private String passCode;

	// CONSTRUCTOR
	public PrivateStoryline(Vector<Puzzle> puzzles, String name,
			ImageIcon picture, Partner partner, String description,
			String conclusion, String passCode, ImageIcon descriptionPicture) {
		super(puzzles, name, picture, partner, description, conclusion,
				descriptionPicture);
		this.passCode = passCode;
	}

	// METHODS
	
	// checks if the passCode is correct
	public boolean checkPassCode(String passCode) {
		if (this.passCode == passCode)
			return true;
		else
			return false;
	}

	//Method for returning the passcode
	public String getPassCode() {
		return passCode;
	}

	//Method for modifying the passcode
	public void setPassCode(String passCode) {
		this.passCode = passCode;
	}
	
}//End of PrivateStoryline
