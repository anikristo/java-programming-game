package jpgame.storyline.win;

import java.awt.event.ActionListener;

import javax.swing.JButton;

import jpgame.JPGView;

/**
 * JPGWinningView - This class represents an interface for WInningView class
 * 
 * @author Anisa Llaveshi
 * @version 1.00 2014/22/7
 */

public interface JPGWinningView extends JPGView {
	public JButton getHomepageButton();

	public JButton getNotesButton();

	public void addActionListeners(ActionListener actionListener);

}
