package jpgame.storyline.win;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;

import jpgame.JPGColorLibrary;
import jpgame.JPGComponent;
import jpgame.storyline.Storyline;

/**
 * WinningView - This class is a view class for the winning case of a game. It
 * displays storyline's picture, a conclusion statement and the total score
 * collected in that storyline
 * 
 * @author Anisa Llaveshi
 * @version 1.00 2014/4/28
 */

public class WinningView extends JPanel implements JPGWinningView {
	
	//Serial NO
	private static final long serialVersionUID = -3195225900959018801L;
	//PROPERTIES
	private JLabel theEnd;
	private JLabel storylinePicLabel;
	private JLabel storylinePicLabel2;
	private JLabel totalScoreLabel;
	private JPanel theEndPanel;
	private JPanel buttonsPanel;
	private JPanel storylinePicPanel;
	private JPanel storylinePicPanel2;
	private JPanel totalScorePanel;
	private JPanel buttonsAndScorePanel;
	private JButton homepageButton;
	private JButton notesButton;
	private JTextPane conclusion;

	//CONSTRUCTOR
	public WinningView(Storyline storyline) {
		setLayout(new BorderLayout());

		if (storyline != null) {
			// JLabel containing the writing "The END" and JPanel containing
			// this JLabel
			theEnd = new JLabel("The END");
			theEnd.setFont(new Font("Segoe UI", Font.BOLD, 50));
			theEnd.setForeground(JPGColorLibrary.WIN_TXT_FOREGR);
			theEndPanel = new JPanel();
			theEndPanel.add(theEnd);
			theEndPanel.setBackground(JPGColorLibrary.BACKGROUND_WIN_VIEW);

			// JTextPane containing the conclusion statement of the storyline
			conclusion = new JTextPane();
			conclusion.setText(storyline.getConclusion());
			conclusion.setForeground(JPGColorLibrary.WIN_TXT_FOREGR);
			conclusion.setFont(new Font("Segoue UI", 0, 20));
			conclusion.setEditable(false);
			conclusion.setBackground(JPGColorLibrary.BACKGROUND_WIN_VIEW);

			// Storyline picture is scaled to a certain size
			ImageIcon picture;
			picture = storyline.getPicture();
			if (picture != null) {
				Image img = picture.getImage();
				Image newimg = img.getScaledInstance(250, 250,
						java.awt.Image.SCALE_SMOOTH);
				picture = new ImageIcon(newimg);
			}

			// JLabel containing the storyline picture
			storylinePicLabel = new JLabel(picture);
			storylinePicPanel = new JPanel();
			storylinePicPanel.add(storylinePicLabel);
			storylinePicPanel.setBackground(JPGColorLibrary.BACKGROUND_WIN_VIEW);
			storylinePicLabel2 = new JLabel(picture);
			storylinePicPanel2 = new JPanel();
			storylinePicPanel2.add(storylinePicLabel2);
			storylinePicPanel2.setBackground(JPGColorLibrary.BACKGROUND_WIN_VIEW);

			// JPanel containing the neccessary JButtons
			buttonsPanel = new JPanel();
			buttonsPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 50, 50));
			buttonsPanel.setBackground(JPGColorLibrary.BACKGROUND_WIN_VIEW);

			totalScorePanel = new JPanel(
					new FlowLayout(FlowLayout.CENTER, 0, 0));
			totalScorePanel.setBackground(JPGColorLibrary.BACKGROUND_WIN_VIEW);

			// Total score JLabel;
			totalScoreLabel = new JLabel("Total score collected: "
					+ storyline.getTotalScore());
			totalScoreLabel.setFont(new Font("Segoe UI", Font.BOLD, 50));
			totalScoreLabel.setForeground(JPGColorLibrary.WIN_TXT_FOREGR);
			totalScorePanel.add(totalScoreLabel);

			// Homepage button
			homepageButton = new JButton("Go to HomePage");
			getHomepageButton().setFont(new Font("Segoue UI", Font.BOLD, 15));
			getHomepageButton().setBackground(JPGColorLibrary.BACKGROUND_DARK_BLUE);
			getHomepageButton().setForeground(JPGColorLibrary.WIN_TXT_FOREGR);
			getHomepageButton().setPreferredSize(new Dimension(200, 50));

			// Edit notes button
			notesButton = new JButton("Add notes to my journal");
			getNotesButton().setFont(new Font("Segoue UI", Font.BOLD, 15));
			getNotesButton().setBackground(JPGColorLibrary.BACKGROUND_DARK_BLUE);
			getNotesButton().setForeground(JPGColorLibrary.WIN_TXT_FOREGR);
			getNotesButton().setPreferredSize(new Dimension(200, 50));

			buttonsPanel.add(getHomepageButton());
			buttonsPanel.add(getNotesButton());

			// JPanel containing the totalScorePanel and buttonsPanel
			buttonsAndScorePanel = new JPanel(new GridLayout(2, 1));
			buttonsAndScorePanel.add(totalScorePanel);
			buttonsAndScorePanel.add(buttonsPanel);
			buttonsAndScorePanel.setBackground(JPGColorLibrary.BACKGROUND_WIN_VIEW);

			add(theEndPanel, BorderLayout.PAGE_START);
			add(conclusion, BorderLayout.CENTER);
			add(storylinePicPanel, BorderLayout.LINE_END);
			add(storylinePicPanel2, BorderLayout.LINE_START);
			add(buttonsAndScorePanel, BorderLayout.PAGE_END);
		} else
			add(new JLabel("No Storyline"));

	}

	// METHODS

	// This method overrides JPGView updateView methods and updates the
	// WinningView
	@Override
	public void updateView(JPGComponent storyline) {
		if (storyline instanceof Storyline) {
			totalScoreLabel.setText("Total score collected: "
					+ ((Storyline) storyline).getTotalScore());
		}
	}

	// Method to return homepageButton
	public JButton getHomepageButton() {
		return homepageButton;
	}

	// Method to return notesButton
	public JButton getNotesButton() {
		return notesButton;
	}

	// Method to add actionListener to the buttons0
	public void addActionListeners(ActionListener actionListener) {
		if (homepageButton.getActionListeners().length == 0) {
			homepageButton.addActionListener(actionListener);
			notesButton.addActionListener(actionListener);
		}
	}
} //End of WinningView
