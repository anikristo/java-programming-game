package jpgame.storyline;

import javax.swing.ImageIcon;

import java.io.Serializable;
import java.util.Vector;

import jpgame.JPGComponent;
import jpgame.journal.Journal;
import jpgame.partner.Partner;
import jpgame.puzzle.Puzzle;
import jpgame.storyline.desc.DescriptionView;
import jpgame.storyline.desc.JPGDescriptionView;
import jpgame.storyline.win.JPGWinningView;
import jpgame.storyline.win.WinningView;

/**
 * Storyline - The Storyline class is an abstract class and represents a
 * Storyline of the game. It is a JPGComponent and it is Serializable. A
 * Storyline consists of a set of puzzles thus it has a Vector of Puzzle
 * objects. Furthermore each storyline has a name, description, picture,
 * conclusion statement and partner. In addition a storyline keeps track of the
 * compile and runtime errors occurred throughout the playing of the puzzles of
 * that storyline. It has also a totalScore property containing the total score
 * of the storyline. Furthermore each storyline has a status: NOT_STARTED,
 * IN_PROGRESS or COMPLETED. The Storyline class also keeps track of the
 * currentPuzzle that is being played. It has get method for it, and also
 * getNextPuzzle() method. Additionally, it has a Vector of Puzzle objects
 * containing the finishedPuzzles of that storyline and get and update methods
 * for it. It also has get methods to get the set of attempted puzzles, the
 * followingPuzzle to be attempted and the previous one.
 * 
 * @author Anisa Llaveshi
 * @version 1.00 2014/19/4
 */

public abstract class Storyline implements JPGComponent, Serializable {

	// PROPERTIES 
	private static final long serialVersionUID = 1267295804883343979L;
	// CONSTANTS
	public final static int NOT_STARTED = -1;
	public final static int IN_PROGRESS = 0;
	public final static int COMPLETED = 1;

	private Vector<Puzzle> puzzles;
	private String name;
	private String description;
	private String conclusion;
	private ImageIcon picture;
	private int totalScore;
	private int totalCompileErrors;
	private int totalRunTimeErrors;
	private int status;
	private int currentPuzzle;
	private ImageIcon descriptionPicture;
	private Partner partner;
	private Vector<Puzzle> finishedPuzzles;
	transient private JPGDescriptionView descriptionView;
	transient private JPGWinningView winningView;

	// CONSTRUCTOR
	public Storyline(Vector<Puzzle> puzzles, String name, ImageIcon picture,
			Partner partner, String description, String conclusion,
			ImageIcon descriptionPicture) {
		this.setPuzzles(puzzles);
		this.setName(name);
		this.setPicture(picture);
		this.setPartner(partner);
		this.setDescription(description);
		this.setConclusion(conclusion);
		totalScore = 0;
		totalCompileErrors = 0;
		totalRunTimeErrors = 0;
		status = NOT_STARTED;
		currentPuzzle = 0;
		finishedPuzzles = new Vector<Puzzle>();
		descriptionView = new DescriptionView(this);
		winningView = new WinningView(this);
		this.setDescriptionPicture(descriptionPicture);
	}

	// Empty constructor
	public Storyline() {
		setPuzzles(new Vector<Puzzle>());
		setName("Storyline");
		setPicture( new ImageIcon( getClass().getResource( "/jpgame/img/DefaultStorylineIcon.jpg")));
		setPartner(null);
		setDescription("Storyline Description");
		setConclusion("Conclusion");
		totalScore = 0;
		totalCompileErrors = 0;
		totalRunTimeErrors = 0;
		status = NOT_STARTED;
		currentPuzzle = 0;
		finishedPuzzles = new Vector<Puzzle>();
		descriptionView = new DescriptionView(this);
		setDescriptionPicture(null);
		winningView = new WinningView(this);
	}

	// METHODS
	public Vector<Puzzle> getPuzzles() {
		return puzzles;
	}

	public Puzzle getCurrentPuzzle() 
	{
		if (status == NOT_STARTED)
			return getPuzzles().get(0);
		else if (status == IN_PROGRESS) 
		{
			return getPuzzles().get(finishedPuzzles.size());
		}
		// if the game is finished, there is no current puzzle being played
		else 
			return null;

	}

	// This method returns the next puzzle to be played in the storyline, if
	// there is not on, it returns null
	public Puzzle getNextPuzzle() {
		if (currentPuzzle < getPuzzles().size() - 1) {
			currentPuzzle = currentPuzzle + 1;
			return getPuzzles().get(currentPuzzle);
		} else {
			return null;
		}
	}

	// This method returns the name of the storyline
	public String getName() {
		return name;
	}

	// This method sets the name of the storyline
	public void setName(String name) {
		this.name = name;
	}

	// This method returns the description of the storyline
	public String getDescription() {
		return description;
	}

	// This method sets the description of the storyline
	public void setDescription(String description) {
		this.description = description;
	}

	// This method returns the conclusion of the storyline
	public String getConclusion() {
		return conclusion;
	}

	// This method sets the conclusion of the storyline
	public void setConclusion(String conclusion) {
		this.conclusion = conclusion;
	}

	// This method returns the description picture of the storyline
	public ImageIcon getDescPicture() {
		return getDescriptionPicture();
	}

	// This method sets the description picture of the storyline
	public void setDescPicture(ImageIcon descriptionPicture) {
		this.setDescriptionPicture(descriptionPicture);
	}

	// This method returns the picture of the storyline
	public ImageIcon getPicture() {
		return picture;
	}

	// This method sets the picture of the storyline
	public void setPicture(ImageIcon picture) {
		this.picture = picture;
	}

	// This method returns the total score of the storyline
	public int getTotalScore() {
		for (int i = 0; i < getFinishedPuzzles().size(); i++) {
			totalScore = totalScore + finishedPuzzles.get(i).getScore();
		}
		return totalScore;
	}

	// This method returns the total number of compile errors of the storyline
	public int getTotalCompileErrors() {
		return totalCompileErrors;
	}

	// This method updates the total number of compile errors of the storyline
	public void updateTotalCompileErrors() {
		for (int i = 0; i < getPuzzles().size(); i++) {
			totalCompileErrors = totalCompileErrors
					+ getPuzzles().get(i).getNumberOfCompilationErrors();
		}
	}

	// This method returns the total number of runtime errors of the storyline
	public int getTotalRunTimeErrors() {
		return totalRunTimeErrors;
	}

	// This method updates the total number of runtime errors of the storyline
	public void updateTotalRunTimeErrors() {
		for (int i = 0; i < getPuzzles().size(); i++) {
			totalRunTimeErrors = totalRunTimeErrors
					+ getPuzzles().get(i).getNumberOfRuntimeErrors();
		}
	}

	// This method returns the status of the storyline
	public int getStatus() {
		return status;
	}

	// This method sets the status of the storyline
	public void setStatus() {
		if (finishedPuzzles.size() == getPuzzles().size()) {
			status = COMPLETED;
		} else
			status = IN_PROGRESS;
		Journal.getIndexPage().notifyViews();
	}

	// This method returns the partner
	public Partner getPartner() {
		return partner;
	}

	// This method sets the partner
	public void setPartner(Partner partner) {
		if ( partner != null) {
			this.partner = partner;
			this.partner.notifyViews();
		}
	}

	// This method returns a Vector with the finished puzzles of the storyline
	public Vector<Puzzle> getFinishedPuzzles() {
		updateFinishedPuzzles();
		return finishedPuzzles;
	}

	// This method returns a Vector with the attempted puzzles of the storyline
	public Vector<Puzzle> getAttemptedPuzzles() {
		Vector<Puzzle> attemptedPuzzles = new Vector<Puzzle>(finishedPuzzles);
		if (getCurrentPuzzle() != null)
			attemptedPuzzles.add(attemptedPuzzles.size(), getCurrentPuzzle());
		return attemptedPuzzles;
	}

	// This method updates the vector with the finished puzzles of the storyline
	public void updateFinishedPuzzles() {
		finishedPuzzles = new Vector<Puzzle>();
		for (int i = 0; i < getPuzzles().size(); i++) {
			if (getPuzzles().get(i).isSolved() == true) {
				finishedPuzzles.add(getPuzzles().get(i));
			}
		}
	}

	//This method returns the descriptionView
	public JPGDescriptionView getDescriptionView() {
		return descriptionView;
	}
	
	//This method sets the descriptionView
	public void setDescriptionView(JPGDescriptionView descriptionView) {
		this.descriptionView = descriptionView;
	}

	//This method returns the winningView
	public JPGWinningView getWinningView() {
		winningView.updateView( this);
		return winningView;
	}

	//This method sets the winningView
	public void setWinningView(JPGWinningView winningView) {
		this.winningView = winningView;
	}

	// This method returns the next puzzle in attempted puzzles
	public Puzzle getFollowingPuzzleInAttempted(Puzzle p) {
		for (int i = 0; i < getAttemptedPuzzles().size(); i++) {
			if (p == getAttemptedPuzzles().get(i)) {
				if (i == getAttemptedPuzzles().size() - 1)
					return null;
				else
					return getAttemptedPuzzles().get(i + 1);
			}
		}
		return null;
	}

	// This method returns the previous puzzle in attempted puzzles
	public Puzzle getPreviousPuzzleInAttempted(Puzzle p) {
		for (int i = 0; i < getAttemptedPuzzles().size(); i++) {
			if (p == getAttemptedPuzzles().get(i)) {
				if (i == 0)
					return null;
				else
					return getAttemptedPuzzles().get(i - 1);
			}
		}
		return null;
	}

	public abstract void updatePartner();

	public void createViews() 
	{
		descriptionView = new DescriptionView(this);
		winningView = new WinningView(this);
		
		for (Puzzle p : getPuzzles())
			p.createViews(this);
	}

	public void setPuzzles(Vector<Puzzle> puzzles) {
		this.puzzles = puzzles;
	}

	protected ImageIcon getDescriptionPicture() {
		return descriptionPicture;
	}

	protected void setDescriptionPicture(ImageIcon descriptionPicture) {
		this.descriptionPicture = descriptionPicture;
	}
}
