package jpgame.storyline.desc;

import java.awt.BorderLayout;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;

import jpgame.JPGColorLibrary;
import jpgame.JPGComponent;
import jpgame.storyline.Storyline;

/**
 * DescriptionView - This class represents a view class for a storyline. It has
 * a description title a picture representing a storyline, a disabled JTextpane
 * containing the description of the storyline and next and back buttons
 * contained in a JPanel.
 * 
 * @author Anisa Llaveshi
 * @version 1.00 2014/28/4
 */

public class DescriptionView extends JPanel implements JPGDescriptionView {

	//Serial NO
	private static final long serialVersionUID = 1L;
	
	//Properties
	private Storyline storyline;
	private JLabel descriptionTitle;
	private JTextPane description;
	private JPanel nextPanel;
	private JPanel backPanel;
	private JPanel nextAndBack;
	private JButton nextButton;
	private JButton backButton;
	private JLabel imageLabel;

	//Constructor
	public DescriptionView(Storyline storyline) {
		this.setStoryline(storyline);
		setLayout(new BorderLayout());

		// Size of the JPanel is set in proportion to the size of the Screen
		setPreferredSize(Toolkit.getDefaultToolkit().getScreenSize());

		// JLabel containing the "Description" title of the page and its
		// formatting
		descriptionTitle = new JLabel("Description");
		descriptionTitle.setFont(new Font("Segoe UI", Font.BOLD | Font.ITALIC,
				48));
		descriptionTitle.setForeground(JPGColorLibrary.JPG_CRIMSON);

		// JTextPane containing the description of the storyline is created and
		// its style is formatted
		description = new JTextPane();
		description.setText(storyline.getDescription());
		description.setFont(new Font("Segoe UI", Font.ITALIC, 16));
		description.setForeground(JPGColorLibrary.DESC_TXT_FOREGR);
		description.setEnabled(false);
		description.setMargin(new Insets(10, 10, 10, 10));
		description.setDisabledTextColor(JPGColorLibrary.PURE_BLACK);

		// Back JButton is created and its style is formatted
		backButton = new JButton("Back");
		getBackButton().setPreferredSize(new Dimension(100, 40));
		getBackButton().setBackground(JPGColorLibrary.JPG_CRIMSON);
		getBackButton().setForeground(JPGColorLibrary.PURE_WHITE);
		getBackButton().setFont(new Font("Segoe UI", Font.BOLD, 20));

		// Next JButton is created and its style is formatted
		nextButton = new JButton("Next");
		getNextButton().setBackground(JPGColorLibrary.JPG_CRIMSON);
		getNextButton().setForeground(JPGColorLibrary.PURE_WHITE);
		getNextButton().setFont(new Font("Segoe UI", Font.BOLD, 20));
		getNextButton().setPreferredSize(new Dimension(100, 40));

		// Two panels in FlowLayout are created containing next and back buttons
		nextPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		backPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));

		// Next and back buttons are added to the respective panels
		backPanel.add(getBackButton());
		nextPanel.add(getNextButton());

		// A JPanel in GridLayout is created and the next and back JPanels are
		// added
		nextAndBack = new JPanel(new GridLayout(1, 2, 10, 0));
		nextAndBack.add(backPanel);
		nextAndBack.add(nextPanel);

		// A JLabel containing the picture of the storyline is added to this
		// JLabel
		imageLabel = new JLabel(storyline.getPicture());
		imageLabel.setPreferredSize(new Dimension(200, 200));

		// If the storyline does not have a picture for the description, the
		// storyline's picture is put in the JPanel
		// else, the description picture is put
		if (storyline.getDescPicture() == null) {
			add(descriptionTitle, BorderLayout.NORTH);
			add(description, BorderLayout.EAST);
			add(nextAndBack, BorderLayout.SOUTH);
			add(imageLabel, BorderLayout.CENTER);
		} else {
			imageLabel = new JLabel(storyline.getDescPicture());
			descriptionTitle.setAlignmentX(CENTER_ALIGNMENT);
			add(descriptionTitle, BorderLayout.NORTH);
			add(nextAndBack, BorderLayout.SOUTH);
			add(imageLabel, BorderLayout.CENTER);
		}

	}

	//Methods
	
	//Method to update the view
	public void updateView(JPGComponent component) {
		// nothing actually
	}

	//Method to return nextButton
	public JButton getNextButton() {
		return nextButton;
	}
	
	//Method to get backButton
	public JButton getBackButton() {
		return backButton;
	}
	
	//Method to return the Storyline
	public Storyline getStoryline() {
		return storyline;
	}

	//Method to set the Storyline
	public void setStoryline(Storyline storyline) {
		this.storyline = storyline;
	}

	//Method to add actionListeners to the buttons
	public void addActionListeners(ActionListener actionListener) {
		if(nextButton.getActionListeners().length == 0)
		{
			nextButton.addActionListener(actionListener);
			backButton.addActionListener(actionListener);
		}
	}
}
