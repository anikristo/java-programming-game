package jpgame.storyline.desc;

import java.awt.event.ActionListener;

import javax.swing.JButton;

import jpgame.JPGView;

/**
 * JPGDescriptionView - interface representing a DescritionView
 * 
 * @author Anisa Llaveshi
 * @version 1.00 2014/22/7
 */

public interface JPGDescriptionView extends JPGView {
	public JButton getNextButton();

	public JButton getBackButton();

	public void addActionListeners(ActionListener actionListener);
}
