/**
 * @author Ani Kristo
 * @version 1.0
 * 
 * DESCRIPTION: This interface simply provides a marker for the implementing classes to make sure that every implementing class and methods that
 * 				use JavaProgrammingGame components, will be constrained to JPComponents rather than Object.
 */
package jpgame;

public interface JPGComponent {
}
