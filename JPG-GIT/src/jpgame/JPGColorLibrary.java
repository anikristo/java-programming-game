/**
 * @author Ani Kristo
 * @version 1.0
 * @date 29 / 07 / 2014
 * 
 * Description: This is the class which contains all the color values used in JPG. 
 * 				It is created in order to avoid the ambiguous Color declarations in the code. 
 */
package jpgame;

import java.awt.Color;

public class JPGColorLibrary {

	// BUTTONS
	public static final Color JPG_CRIMSON = new Color( 153, 0, 0);
	public static final Color BUTTON_BLUE = new Color(25, 25, 112);

	// BACKGROUNDS
	public static final Color BACKGROUND_DARK_BLUE = new Color(25, 25, 45);
	public static final Color BACKGROUND_NOT_STARTED_STRL = new Color(130, 180,	243);
	public static final Color BACKGROUND_FINISHED_STRL = new Color(71, 156, 242);
	public static final Color BACKGROUND_STRL = new Color(7, 126, 245);
	public static final Color BACKGROUND_DESCRP = new Color(255, 255, 224);
	public static final Color BACKGROUND_OPEN_VIEW = new Color(153, 204, 255);
	public static final Color BACKGROUND_WIN_VIEW = new Color(128, 0, 0);
	public static final Color BACKGROUND_DARK_GREEN = new Color( 0, 153, 153);

	// TEXT
	public static final Color TEXT_CRIMSON = new Color(128, 0, 0);
	public static final Color TEXT_BLUE = Color.BLUE;
	public static final Color PURE_WHITE = Color.WHITE;
	public static final Color PURE_BLACK = Color.BLACK;
	public static final Color DESC_TXT_FOREGR = new Color(255, 36, 74);
	public static final Color WIN_TXT_FOREGR = new Color(255, 255, 204);

	// TUTORIALS
	public static final Color TUTS_LIGHT_VIOLET = new Color(227, 209, 226);
	public static final Color TUTS_DARK_VIOLET = new Color(105, 67, 100);
	
	// JOURNAL
	public static final Color JOURN_SCENE_BACKGR = new Color(176, 224, 230);
	public static final Color JOURN_LB_FOREGR = new Color(25, 25, 112);
	public static final Color JOURN_QUESTION_BACKGR = new Color(224, 255, 255);
	public static final Color JOURN_SOLN_BACKGR = new Color(173, 255, 255);
	public static final Color JOURN_CODES_BACKGR = new Color(173, 216, 230);
	public static final Color JOURN_BACKGR = new Color(240, 255, 255);
	public static final Color JOURN_NOTES_BACKGR = new Color(175, 238, 238);

}
