/**
 * @author Ani Kristo
 * @version 1.0
 * 
 * DESCRIPTION: JPView interface should be implemented from the Java Classes that deal with the visual representation of the model classes.
 * 				It contains only the updateView method that gives to the views the information for the model class, and forces it to change the view.
 */
package jpgame;

public interface JPGView extends JPGComponent {

	public void updateView(JPGComponent component);
}
