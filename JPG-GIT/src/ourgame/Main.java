/**
 * Main - This class is contains the main method which is necessary to start the program running.
 * 
 * @author Burcu Canakci
 * @version 1.00 2014/5/10
 */

package ourgame;

import javax.swing.SwingUtilities;

import jpgame.game.Game;
import jpgame.game.GameFrame;
import jpgame.journal.Journal;

public class Main {
	public static void main(String[] args) {
		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				Game game = new OurGame();
				Journal.loadContents();
				game = (OurGame) Game.load("game.data");
				
				game.createViews();
				new GameFrame(game);
			}
		});
		
	}
}