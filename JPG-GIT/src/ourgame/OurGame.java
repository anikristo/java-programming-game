 package ourgame;

import java.io.File;

import storylines.catchinghope.CHStoryline;
import storylines.intoKGB.IKStoryline;
import storylines.magicalworld.MWStoryline;
import storylines.pyramids.PRStoryline;
import storylines.watsonsjournal.WJStoryline;
import jpgame.game.Game;
import jpgame.journal.Journal;

/**
 * OurGame - This class extends the Game class and represents a game. Storyline
 * are added to it and its data is saved in a file "game.data"
 * 
 * @author Anisa Llaveshi
 * @version 1.00 2014/5/10
 */

public class OurGame extends Game {
	private static final long serialVersionUID = 7224503152245154862L;

	public OurGame() {
		//Storylines are added to the game
		addStoryline(new CHStoryline());
		addStoryline(new IKStoryline());
		addStoryline(new PRStoryline());
		addStoryline(new WJStoryline());
		addStoryline(new MWStoryline());

		//The data of the game is saved in a file "game.data"
		File gameDataFile = new File("game.data");
		if (!gameDataFile.exists())
			save("game.data");
		
		File clistDataFile = new File("ChroList.data");
		File plistDataFile = new File("PortList.data");
		if (!clistDataFile.exists() && !plistDataFile.exists())
			Journal.saveContents();
	}
}
