package storylines.pyramids;
import javax.swing.ImageIcon;

import jpgame.storyline.Storyline;

/**
* PRStoryline.java
* 
* PRStoryline application at JavaProgrammingGame
* 
* Creates the storyline pyramids
* 
* Author: Ayse Berceste Dincer
* version 1.00 2014/5/5
**/

public class PRStoryline extends Storyline
{
	//CONSTANTS
	private static final long serialVersionUID = -7187373449609838191L;

	//Constructor
	public PRStoryline()
	{
		getPuzzles().add (new PRPuzzle1(new PRPuzzle1AdvSetup(), this));
		setName("Pyramids");
		setDescription("You are to discover the mystery of the Egypt Pyramids.\n Surprises await!");
	
		setPartner(new PRPartner( this));
		
		setPicture(new ImageIcon( getClass().getResource( "/storylines/pyramids/img/PRStoryline.jpg")));
		setDescriptionPicture(new ImageIcon( getClass().getResource( "/storylines/pyramids/img/PRDescription.png" )));
		setConclusion("You did it! You solved the mystery of the pyramids! All the Egyptian secrets are revealed now. "
				+ " And you know what? The pyramids are the marvels of the enginnerings and even though you had"
				+ " to go through the traps, obstacles and you experienced the fury of Gods, you posses the world's biggest "
				+ " and most precious treasure! The key to unlocking the past!");
	}

	@Override
	public void updatePartner() {
	}

} //End of PRStoryline class

