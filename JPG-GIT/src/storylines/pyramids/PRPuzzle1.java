package storylines.pyramids;

import java.util.Scanner;

import jpgame.partner.hints.Hint;
import jpgame.puzzle.Puzzle;
import jpgame.puzzle.setup.AdvancedPuzzleSetup;
import jpgame.puzzle.setup.BasicPuzzleSetup;
import jpgame.storyline.Storyline;

public class PRPuzzle1 extends Puzzle {

/**
* PRPuzzle1.java
* 
* PRPuzzle1 application at JavaProgrammingGame
* 
* Creates the first puzzle of the Pyramids storyline
* 
* Author: Ayse Berceste Dincer
* version 1.00 2014/5/5
**/
	
	//CONSTANTS 
	private static final long serialVersionUID = -1620387947917350664L;
	static final int COMPILATION_BONUS = 100;
	static final int CORRECT_SOLN_BONUS = 300;
	
	// PROPERTIES
	private BasicPuzzleSetup setup;
	// CONSTRUCTOR
	public PRPuzzle1(AdvancedPuzzleSetup setup, Storyline s) {
		super(setup, s);
		this.setup = setup;
		//Creates the hints of the particular puzzle
		Hint hint1 = new Hint (this);
		Hint hint2 = new Hint (this);
		//Creates the texts of the hints
		hint1.setText ("You need to do some calculations");
		hint2.setText ("The formula of the volume of the pyramid is baseLength*baseLength*height*1/3");
		//Sets the revealing time for hints	
		hint1.setTime(30);		
		hint2.setTime(120);		
		//Adds the hints to the journal
		getAllHints().add (hint1);	
		getAllHints().add (hint2);	
	}

	//Method for checking the solution of the puzzle
	@Override
	public boolean checkSolution() {
		boolean isCorrect = false;

		compile();
		runKeySoln();
		runInputFile();
		
		try {
			Thread.sleep( 1000);
		} catch (InterruptedException e1){}

		try {
			Scanner readingResOutput = new Scanner(setup.getResultsOutputFile());

			Scanner readingSolnOutput = new Scanner(setup.getSolutionOutputFile());

			 if(readingResOutput.hasNext())
				 isCorrect= true;
			 
			while (readingResOutput.hasNext() && readingSolnOutput.hasNext()) {
				if (readingResOutput.nextInt() != readingSolnOutput.nextInt()) {
					isCorrect = false;
					updateScore();
				}

			}
			if(isCorrect){
				setSolved( true);
				updateScore();
			}
			
			readingResOutput.close();
			readingSolnOutput.close();

			
			return isCorrect;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	//Method for updating the score according to the number of errors
	@Override
	public void updateScore() {
		setScore((int) (getScore() - Math.pow(getNumberOfCompilationErrors(), 0.5)));
		setScore((int) (getScore() - Math.pow(getNumberOfRuntimeErrors(), 0.5)));
		setScore((int) (getScore() + getTimeBonus()));
		if (isCompiled()) {
			setScore((int) (getScore() + COMPILATION_BONUS));
		}

		if (isSolved()) {
			setScore((int) (getScore() + CORRECT_SOLN_BONUS));
		}
	}

	//Method for returning the bonus point according to the time spend on each puzzle
	private int getTimeBonus() {
		if (getTime() <= 120)
			return 100;
		else if (getTime() <= 300)
			return 50;
		else
			return 0;
	}

} //End of PRPuzzle1
