package storylines.pyramids;
import javax.swing.*;

import jpgame.partner.Partner;
import jpgame.storyline.Storyline;

/**
* PRPartner.java
* 
* PRPartner application at JavaProgrammingGame
* 
* Creates the partner of the Pyramids storyline
* 
* Author: Ayse Berceste Dincer
* version 1.00 2014/5/5
**/

class PRPartner extends Partner
{	
	//Serial NO
	private static final long serialVersionUID = -7001669267011016601L;

	//Constructor
	public PRPartner( Storyline s)
	{
		super( "Mr.Explorer", s);
		setPicture( new ImageIcon( getClass().getResource( "/storylines/pyramids/img/PRPartner.jpg")));
	}	

} //End of PRPartner class
