package storylines.pyramids;

import java.io.File;
import java.io.Serializable;

import javax.swing.ImageIcon;

import jpgame.puzzle.setup.AdvancedPuzzleSetup;

/**
* PRPuzzle1AdvSetup.java
* 
* PRPuzzle1AdvSetup application at JavaProgrammingGame
* 
* Creates the setup for the first puzzle of the pyramids storyline
* 
* Author: Ayse Berceste Dincer
* version 1.00 2014/5/5
**/

public class PRPuzzle1AdvSetup implements AdvancedPuzzleSetup, Serializable
{
	//Serial NO
	private static final long serialVersionUID = -255681421462270168L;

	//Method for returning the difficulty coefficient
	@Override
	public double getDifficultyCoefficient() {
		return 3.0;
	}

	//Method for returning the puzzle name
	@Override
	public String getPuzzleName() {
		return "First Puzzle";
	}

	//Method for returning the puzzle question
	@Override
	public String getPuzzleQuestion() {
		return "Welcome to your first mission in Pyramids! \n"
				+ " Your missions are becoming more difficult as you are more experienced now."
				+ "\n You know that our aim is to solve the mystery of the pyramids. "
				+ "Our first step is discovering the architectural mystery:\n "
				+ "\nWhat you need to do is to calculate the volume of the pyramids."
				+ "\nYou are given the height and length of the base square. "
				+ " The method signature is public static int volume(int side, int height)"
				+ "\n Good Luck!";
	}

	//Method for returning the ErrorLogFile
	@Override
	public File getErrorLogFile() {
		File errorLog = new File( "PRPuzzle1ErrLog.log");
		errorLog.deleteOnExit();
		return errorLog;
	}

	//Method for returning the InputFile
	@Override
	public File getInputFile() {
		File input = new File( "PRPuzzle1Input.java");
		input.deleteOnExit();
		return input;
	}

	//Method for returning the KeySolutionFile
	@Override
	public File getKeySolutionFile() {
		return new File( "PRPuzzle1KeySoln.class");
	}

	//Method for returning the ResultsOutputFile
	@Override
	public File getResultsOutputFile() {
		File resOutput = new File( "PRPuzzle1ResOutput.txt");
		resOutput.deleteOnExit();
		return resOutput;
	}

	//Method for returning the TemplateFile
	@Override
	public File getTemplateFile() {
		return new File( "PRPuzzle1Template.txt");
	}

	//Method for returning the BackgroundPicture
	@Override
	public ImageIcon getBackgroundPicture() {
		return new ImageIcon( getClass().getResource( "/storylines/pyramids/img/PRPuzzle1.jpg"));
	}

	//Method for returning the SolutionOutputFile
	@Override
	public File getSolutionOutputFile() {
		File solnOutput = new File( "PRPuzzle1SolnOutput.txt");
		solnOutput.deleteOnExit();
		return solnOutput;
	}
	
	@Override
	public String getStorylineName() {
		return "Pyramids";
	}

	@Override
	public String getPreliminaryCode() {
		return "public static int volume(int side, int height)";
	}

} //End of PRPuzzle1AdvSetup class

