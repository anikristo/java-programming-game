package storylines.magicalworld;

import java.awt.Point;
import java.util.Scanner;

import jpgame.partner.hints.Hint;
import jpgame.puzzle.Puzzle;
import jpgame.puzzle.clues.Clue;
import jpgame.puzzle.setup.AdvancedPuzzleSetup;
import jpgame.puzzle.setup.BasicPuzzleSetup;
import jpgame.storyline.Storyline;

/**
 * MWPuzzle1 - This is the first puzzle of the Magical World Storyline and it extends Puzzle
 *
 * @author Anisa Llaveshi
 * @version 1.00 2014/5/8
 */

public class MWPuzzle1 extends Puzzle {

	private static final long serialVersionUID = 6190790064605582603L;
	// PROPERTIES
	
	static final int COMPILATION_BONUS = 20;
	static final int CORRECT_SOLN_BONUS = 200;
	
	private BasicPuzzleSetup setup;

	// CONSTRUCTOR
	public MWPuzzle1(AdvancedPuzzleSetup setup, Storyline s) {
		super(setup, s);
		
		// Hints are created and added
		this.setup = setup;
		Hint hint1 = new Hint (this);
		hint1.setText ("Consider making all the characters same case.");
		hint1.setTime(30);		
		getAllHints().add (hint1);		
		
		// Clues are created and added
		Clue clue1 = new Clue (this);
		clue1.setText ("Searching in an array may be helpful, keep that in mind!");
		clue1.setLocation(new Point (120,210));
		clue1.setTime(20);		
		getAllClues().add (clue1);
	}

	// METHODS
	
	// This method checks the solution of a the puzzle
	@Override
	public boolean checkSolution() {
		boolean isCorrect = false;

		compile();
		runKeySoln();
		runInputFile();
		
		try {
			Thread.sleep( 1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			Scanner readingResOutput = new Scanner(setup.getResultsOutputFile());

			Scanner readingSolnOutput = new Scanner(setup.getSolutionOutputFile());
			
			 if(readingResOutput.hasNext())
				 isCorrect= true;

			while (readingResOutput.hasNext() && readingSolnOutput.hasNext()) {
				if (readingResOutput.nextInt() != readingSolnOutput.nextInt()) {
					isCorrect = false;
					updateScore();
				}

			}
			if(isCorrect){
				setSolved( true);
				updateScore();
			}

			readingResOutput.close();
			readingSolnOutput.close();

			
			return isCorrect;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

//	public int getNumberOfRuntimeErrors() {
//		return numberOfRuntimeErrors;
//	}

	// This method calculates and updates the score collected depending on the
	// number of errors, difficulty coefficient and time spent on it
	@Override
	public void updateScore() {
		setScore((int) (getScore() - Math.pow(getNumberOfCompilationErrors(), 0.5)));
		setScore((int) (getScore() - Math.pow(getNumberOfRuntimeErrors(), 0.5)));
		setScore((int) (getScore() + getTimeBonus()));
		if (isCompiled()) {
			setScore((int) (getScore() + COMPILATION_BONUS));
		}

		if (isSolved()) {
			setScore((int) (getScore() + CORRECT_SOLN_BONUS));
		}
	}

	// According to the time spent on solving a puzzle a bonus may be awarded
	private int getTimeBonus() {
		if (getTime() <= 60)
			return 100;
		else if (getTime() <= 300)
			return 50;
		else
			return 0;
	}

}
