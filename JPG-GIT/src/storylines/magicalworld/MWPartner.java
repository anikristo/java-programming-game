package storylines.magicalworld;
import javax.swing.*;

import jpgame.partner.Partner;
import jpgame.storyline.Storyline;

/**
 * MWPartner - This is a partner for the Magical World storyline
 *
 * @author Anisa Llaveshi
 * @version 1.00 2014/4/31
 */

class MWPartner extends Partner
{	
	private static final long serialVersionUID = 7901316295254288547L;

	//Constructor
	public MWPartner( Storyline s)
	{
		super( "YOU KNOW WHO", s);
		setPicture( new ImageIcon( getClass().getResource( "/storylines/magicalworld/img/MWPartner.jpg")));
	}	
}
