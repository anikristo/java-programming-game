package storylines.magicalworld;

import java.io.File;
import java.io.Serializable;

import javax.swing.ImageIcon;

import jpgame.puzzle.setup.AdvancedPuzzleSetup;
/**
 * MWPuzzle1AdvSetup - This is the setup for the first puzzle of magical world storyline
 *
 * @author Anisa Llaveshi
 * @version 1.00 2014/5/8
 */

public class MWPuzzle1AdvSetup implements AdvancedPuzzleSetup, Serializable {

	private static final long serialVersionUID = 8651867504574687085L;
	@Override
	public double getDifficultyCoefficient() {
		return 1.0;
	}

	@Override
	public String getPuzzleName() {
		return "First Puzzle";
	}

	@Override
	public String getPuzzleQuestion() {
		return "Hello and welcome to the MAGICAL WORLD first Puzzle!"
				+ "What Harry firstly does is challenge you to prove him that you are the right person."
				+ "You are in the train station and you have to find the platform number of the train "
				+ "you have to take to get to Harry. The number of the platform is the number of times a "
				+ "certain character is repeated in a given String. Write a method to return this number. "
				+ "The method signature is public static int count(String str, char c)"
				+ "\n Good Luck!";
	}

	@Override
	public File getErrorLogFile() {
		File errorLog = new File( "MWPuzzle1ErrLog.log");
		errorLog.deleteOnExit();
		return errorLog;
	}

	@Override
	public File getInputFile() {
		File input = new File( "MWPuzzle1Input.java");
		input.deleteOnExit();
		return input;
	}

	@Override
	public File getKeySolutionFile() {
		return new File( "MWPuzzle1KeySoln.class");
	}

	@Override
	public File getResultsOutputFile() {
		File resOutput = new File( "MWPuzzle1ResOutput.txt");
		resOutput.deleteOnExit();
		return resOutput;
	}

	@Override
	public File getTemplateFile() {
		return new File( "MWPuzzle1Template.txt");
	}

	@Override
	public ImageIcon getBackgroundPicture() {
		return new ImageIcon( getClass().getResource( "/storylines/magicalworld/img/MWPuzzle1Background.jpg"));
	}

	@Override
	public File getSolutionOutputFile() {
		File solnOutput = new File( "MWPuzzle1SolnOutput.txt");
		solnOutput.deleteOnExit();
		return solnOutput;
	}
	
	@Override
	public String getStorylineName() {
		return "Magical World";
	}

	@Override
	public String getPreliminaryCode() {
		return "public static int count(String str, char c)";
	}
}
