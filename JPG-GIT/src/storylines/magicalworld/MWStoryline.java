package storylines.magicalworld;

import javax.swing.ImageIcon;

import jpgame.storyline.Storyline;

/**
 * MWStoryline - This is the class representing the Magical World storyline
 * 
 * @author Anisa Llaveshi
 * @version 1.00 2014/5/8
 */

public class MWStoryline extends Storyline {
	private static final long serialVersionUID = -3567662648871612326L;

	public MWStoryline() {
		// Puzzles are added to the storyline
		getPuzzles().add(new MWPuzzle1(new MWPuzzle1AdvSetup(), this));

		setPartner(new MWPartner( this));
		
		setName("Magical World");
		// Description of the storyline Magical World
		setDescription(("Do you like magic? Do you like Harry Potter? \n"
				+ "Do you like adventures? Then this is the perfect place for you. \n"
				+ "Welcome to the magical world. Voldemort may be dead but adventures for Harry do not end"
				+ "there. \nHis invisibility cloak suddenly disappears and Harry doubts that old enemies hide after that."
				+ "\nIn absence of Ron and Hermione, help Harry in his new journey."));

		// Conclusion statement
		setConclusion("The invisibility cloak is back in Harry's hands. He would never had done it without you and he is"
				+ "very grateful. He enjoyed this adventure a lot, and we hope you enjoyed this adventure with him."
				+ "See you again in other adventures!");

		setPicture(new ImageIcon(getClass().getResource(
				"/storylines/magicalworld/img/MWStoryline.jpg")));
	}

	@Override
	public void updatePartner() {
	}
}
