/**
 * @author Ani Kristo
 * @version 1.0
 * 
 * 12 / 05 / 2014
 * 
 * DESCRIPTION: This is the partner character for the Into KGB storyline. It is a very crude implementation as it has no interaction 
 * 				with the user and it simply is necessary for further reference in the storyline. 
 */
package storylines.intoKGB;

import javax.swing.ImageIcon;

import jpgame.partner.Partner;
import jpgame.storyline.Storyline;

public class IKPartner extends Partner {

	/**
	 * Generated serialVersionUID, which is necessary for serialization
	 */
	private static final long serialVersionUID = -4672984311367666482L;

	public IKPartner( Storyline s) {
		super("Gaston", s);
		setPicture( new ImageIcon( getClass().getResource( "/storylines/intoKGB/img/IKPartner.png")));
	}
}
