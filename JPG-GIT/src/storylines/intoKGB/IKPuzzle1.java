/**
 * @author Ani Kristo
 * @version 1.0
 * 
 * 12 / 05 / 2014
 * 
 * DESCRIPTION: This is the sole puzzle for the Into KGB storyline. It is supposed to challenge the user into a totally amateur-level
 * 				question, such that it is friendly to starters.  
 */
package storylines.intoKGB;

import java.util.Scanner;

import jpgame.partner.hints.Hint;
import jpgame.puzzle.Puzzle;
import jpgame.puzzle.setup.BasicPuzzleSetup;
import jpgame.storyline.Storyline;

public class IKPuzzle1 extends Puzzle {

	/**
	 * Generated serialVersionUID, which is necessary for serialization
	 */
	
	// PROPERTIES
	private static final long serialVersionUID = 8902621004246810671L;
	private BasicPuzzleSetup setup;

	// CONSTRUCTOR
	public IKPuzzle1(BasicPuzzleSetup setup, Storyline s) {
		super(setup, s);
		this.setup = setup;

		Hint hint1 = new Hint(this);
		hint1.setText("The 'return' statement is used to indicate what the method's response will be.");
		hint1.setTime(15);

		getAllHints().add(hint1);
	}

	// METHODS
	@Override
	public boolean checkSolution() {

		boolean isCorrect = false;

		compile();
		runKeySoln();
		runInputFile();

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
		}

		try {
			Scanner readingResOutput = new Scanner(setup.getResultsOutputFile());

			Scanner readingSolnOutput = new Scanner(
					setup.getSolutionOutputFile());

			if (readingResOutput.hasNext())
				isCorrect = true;

			while (readingResOutput.hasNext() && readingSolnOutput.hasNext()) {
				if (!readingResOutput.next().equals(readingSolnOutput.next())) {
					isCorrect = false;
					updateScore();
				}

			}
			if (isCorrect) {
				setSolved(true);
				updateScore();
			}

			readingResOutput.close();
			readingSolnOutput.close();

			return isCorrect;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public void updateScore() {
		setScore((int) (getScore() - Math.pow(getNumberOfCompilationErrors(), 0.5)));
		setScore((int) (getScore() - Math.pow(getNumberOfRuntimeErrors(), 0.5)));
		setScore((int) (getScore() + getTimeBonus()));
		if (isCompiled()) {
			setScore((int) (getScore() + 20));
		}

		if (isSolved()) {
			setScore((int) (getScore() + 20));
		}
	}

	private int getTimeBonus() {
		if (getTime() <= 60)
			return 100;
		else if (getTime() <= 300)
			return 50;
		else
			return 0;
	}
}
