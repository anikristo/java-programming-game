/**
 * @author Ani Kristo
 * @version 1.0
 * 
 * 12 / 05 / 2014
 * 
 * DESCRIPTION: This is the setup for the Into KGB storyline's first and only puzzle. As a setup is meant to, it provides the 
 * 				required information for the setting up of files that the Puzzle 1 will use. 
 */
package storylines.intoKGB;

import java.io.File;
import java.io.Serializable;

import jpgame.puzzle.setup.BasicPuzzleSetup;

public class IKPuzzle1AdvSetup implements BasicPuzzleSetup, Serializable {

	/**
	 * Generated serialVersionUID, which is necessary for serialization
	 */
	private static final long serialVersionUID = -5521241184607476065L;

	// METHODS
	@Override
	public String getPuzzleName() {
		return "The Strange Message";
	}

	@Override
	public String getPuzzleQuestion() {
		return " After you received the strange message, you will try to send it to your partner, for backup."
				+ "Provide the definition of a method that gives your partner this message that you received."
				+ "The signature should look like that: public static String sendToPartner( String message)";
	}

	@Override
	public File getErrorLogFile() {
		File IKPuzzle1ErrorLog = new File("IKPuzzle1ErrorLog.log");
		IKPuzzle1ErrorLog.deleteOnExit();
		return IKPuzzle1ErrorLog;
	}

	@Override
	public File getInputFile() {
		File IKPuzzle1Input = new File("IKPuzzle1Input.java");
		IKPuzzle1Input.deleteOnExit();
		return IKPuzzle1Input;
	}

	@Override
	public File getKeySolutionFile() {
		File IKPuzzle1KeySoln = new File("IKPuzzle1KeySoln.class");
		return IKPuzzle1KeySoln;
	}

	@Override
	public File getResultsOutputFile() {
		File IKPuzzle1ResOutput = new File("IKPuzzle1ResOutput.txt");
		IKPuzzle1ResOutput.deleteOnExit();
		return IKPuzzle1ResOutput;
	}

	@Override
	public File getSolutionOutputFile() {
		File IKPuzzle1SolnOutput = new File("IKPuzzle1SolnOutput.txt");
		IKPuzzle1SolnOutput.deleteOnExit();
		return IKPuzzle1SolnOutput;
	}

	@Override
	public File getTemplateFile() {
		File IKPuzzle1Template = new File("IKPuzzle1Template.txt");
		return IKPuzzle1Template;
	}

	@Override
	public String getStorylineName() {
		return "Into KGB";
	}
}
