/**
 * @author Ani Kristo
 * @version 1.0
 * 
 * 12 /05 / 2014
 * 
 * DESCRIPTION: This is the class that creates the Into KGB storyline. It contains only one puzzle for a beginner level. Mostly 
 * 				intended for ease of demo.
 */
package storylines.intoKGB;

import jpgame.storyline.Storyline;

public class IKStoryline extends Storyline {

	// PROPERTIES

	/**
	 * Generated serialVersionUID, which is necessary for serialization
	 */
	private static final long serialVersionUID = 66887448743879824L;

	// CONSTRUCTOR
	public IKStoryline() {
		getPuzzles().add(new IKPuzzle1(new IKPuzzle1AdvSetup(), this));
		setPartner(new IKPartner( this));
		setName("Into KGB");
		setDescription((" One day you received a strange message from an unknown number. \n"
				+ "The message was not clear. It wasn't in English, so you couldn't understand.\n"
				+ "However, you have a partner named Gaston, who is a master at understanding"
				+ "foreign languages. \nYou first send the message to him, and then wait for the "
				+ "new adventures!"));

	}

	@Override
	public void updatePartner() {}

}
