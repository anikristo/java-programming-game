/**
 * @author Ani Kristo
 * @version 1.0
 * 
 * 12 / 05 / 2014
 * 
 * DESCRIPTION: This is the setup for Puzzle1 in Catching Hope storyline. It gives the necessary Background picture file, 
 * 				input, template, outputs and solution files. 
 */

package storylines.catchinghope;

import java.io.File;
import java.io.Serializable;

import javax.swing.ImageIcon;

import jpgame.puzzle.setup.AdvancedPuzzleSetup;

public class CHPuzzle1AdvSetup implements AdvancedPuzzleSetup, Serializable {

	/**
	 * Generated serialVersionUID, which is necessary for serialization
	 */
	private static final long serialVersionUID = 1343002743285898861L;

	// METHODS
	@Override
	public double getDifficultyCoefficient() {
		return 1.0;
	}

	@Override
	public String getPuzzleName() {
		return "First Puzzle";
	}

	@Override
	public String getPuzzleQuestion() {
		return "<html><font face = \"Segoe UI\" >Hello and welcome to the first Puzzle! <br>"
				+ "As you can see, in the front door at the museum's entrance, there are a lot of numbers written"
				+ "in a randomn order. On the other hand, if you want to enter the museum, the lock gets a combination"
				+ "of numbers. That is going to be the sum of all the numbers written in the verge of the door."
				+ "In this puzzle you are required to write a method for summing values from a given array of ints."
				+ "<br>The method definition should look like that: <b>public static int sum( int[] x)</b>"
				+ "<br>Good Luck!</html>";
	}

	@Override
	public File getErrorLogFile() {
		File errorLog = new File("CHPuzzle1ErrLog.log");
		errorLog.deleteOnExit();
		return errorLog;
	}

	@Override
	public File getInputFile() {
		File input = new File("CHPuzzle1Input.java");
		input.deleteOnExit();
		return input;
	}

	@Override
	public File getKeySolutionFile() {
		return new File("CHPuzzle1KeySoln.class");
	}

	@Override
	public File getResultsOutputFile() {
		File resOutput = new File("CHPuzzle1ResOutput.txt");
		resOutput.deleteOnExit();
		return resOutput;
	}

	@Override
	public File getTemplateFile() {
		return new File("CHPuzzle1Template.txt");
	}

	@Override
	public ImageIcon getBackgroundPicture() {
		return new ImageIcon(getClass().getResource(
				"/storylines/catchinghope/img/CHPuzzle1Background.jpg"));
	}

	@Override
	public File getSolutionOutputFile() {
		File solnOutput = new File("CHPuzzle1SolnOutput.txt");
		solnOutput.deleteOnExit();
		return solnOutput;
	}

	@Override
	public String getStorylineName() {
		return "Catching Hope";
	}

	@Override
	public String getPreliminaryCode() {
		return "public static int sum( int[] x){"
				+ "\n	// CODE HERE"
				+ "\n"
				+ "\n}";
	}
}
