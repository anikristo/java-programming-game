package storylines.catchinghope;
import java.io.File;
import java.io.Serializable;

import javax.swing.ImageIcon;

import jpgame.puzzle.setup.AdvancedPuzzleSetup;

/**
* CHPuzzle3AdvSetup.java
* 
* CHPuzzle3AdvSetup application at JavaProgrammingGame
* 
* Creates the setup for the third puzzle of the catching hope storyline
* 
* Author: Ay�e Berceste Din�er
* version 1.00 2014/5/5
**/

public class CHPuzzle3AdvSetup implements AdvancedPuzzleSetup, Serializable {

	//CONSTANTS
	private static final long serialVersionUID = 1159837538411698298L;

	//Method for returning the difficulty coefficient
	@Override
	public double getDifficultyCoefficient() {
		return 5.0;
	}

	//Method for returning the puzzle name
	@Override
	public String getPuzzleName() {
		return "Third Puzzle";
	}

	//Method for returning the puzzle question
	@Override
	public String getPuzzleQuestion() {
		return "<html><font face= \"Segoe UI\" >You are going well and already have reached the third puzzle. <br>"
				+ " Your missions are becoming more difficult as you are more experienced now."
				+ "In American History Museum the rubby slippers of Dorothy are displayed."
				+ "These shoes are precious and we should steal them. "
				+ "However there are also copies of the real shoes in the museum, we need to steal the original one."
				+ "What you need to do is to search the array of shoes and find the real one."
				+ "You are given an array of Strings and you know the String of the real slippers."
				+ "Since you do not know the elements of the array you need to search the complete array"
				+ " and return the index of the real slippers. The method signature is <b>public static int search(String[] array, String key)</b>"
				+ "<br> Good Luck!";
	}

	//Method for returning the ErrorLogFile
	@Override
	public File getErrorLogFile() {
		File errorLog = new File( "CHPuzzle3ErrLog.log");
		errorLog.deleteOnExit();
		return errorLog;
	}

	//Method for returning the InputFile
	@Override
	public File getInputFile() {
		File input = new File( "CHPuzzle3Input.java");
		input.deleteOnExit();
		return input;
	}

	//Method for returning the KeySolutionFile
	@Override
	public File getKeySolutionFile() {
		return new File( "CHPuzzle3KeySoln.class");
	}

	//Method for returning the ResultsOutputFile
	@Override
	public File getResultsOutputFile() {
		File resOutput = new File( "CHPuzzle3ResOutput.txt");
		resOutput.deleteOnExit();
		return resOutput;
	}

	//Method for returning the TemplateFile
	@Override
	public File getTemplateFile() {
		return new File( "CHPuzzle3Template.txt");
	}

	//Method for returning the BackgroundPicture
	@Override
	public ImageIcon getBackgroundPicture() {
		return new ImageIcon( getClass().getResource( "/storylines/catchinghope/img/CHPuzzle3Background.jpg"));
	}

	//Method for returning the SolutionOutputFile
	@Override
	public File getSolutionOutputFile() {
		File solnOutput = new File( "CHPuzzle3SolnOutput.txt");
		solnOutput.deleteOnExit();
		return solnOutput;
	}
	
	@Override
	public String getStorylineName() {
		return "Catching Hope";
	}

	@Override
	public String getPreliminaryCode() {
		return "public static int search(String[] array, String key){"
				+ "\n	// CODE HERE"
				+ "\n"
				+ "\n}";
	}

} //End of CHPuzzle3AdvSetup
