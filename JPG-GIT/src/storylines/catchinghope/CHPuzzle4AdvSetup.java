package storylines.catchinghope;

/**
 * CHPuzzle4AdvancedSetup	Setup for Puzzle#4 in Storyline Catching Hope
 * @author Burcu Canakci
 * @version 1.00 2014/5/5
 */

import java.io.File;
import java.io.Serializable;

import javax.swing.ImageIcon;

import jpgame.puzzle.setup.AdvancedPuzzleSetup;

public class CHPuzzle4AdvSetup implements AdvancedPuzzleSetup, Serializable
{

	private static final long serialVersionUID = 4474425778213194612L;

	@Override
	public double getDifficultyCoefficient()
	{
		return 10.0;
	}

	@Override
	public String getPuzzleName()
	{
		return "Fourth Puzzle";
	}

	@Override
	public String getPuzzleQuestion()
	{
		return "<html><font face= \"Segoe UI\" >Be patient! It is all almost over <br>"
				+ "You are given an encrypted file. I have managed to find out that every 6 characters you get an n^2 unused chars, n being the nth useless part starting from 1."
				+ "I have also managed to realize that if the string has at its end less than n*n characters to be removed, those characters are removed and no further action is taken"
				+ "The method definition should look like that: <b>public static String decrypt (String s)</b>"
				+ "<br>Good Luck! Do not fail me now.";
	}

	@Override
	public File getErrorLogFile()
	{
		File errorLog = new File("CHPuzzle4ErrLog.log");
		errorLog.deleteOnExit();
		return errorLog;
	}

	@Override
	public File getInputFile()
	{
		File input = new File("CHPuzzle4Input.java");
		input.deleteOnExit();
		return input;
	}

	@Override
	public File getKeySolutionFile()
	{
		return new File("CHPuzzle4KeySoln.class");
	}

	@Override
	public File getResultsOutputFile()
	{
		File resOutput = new File("CHPuzzle4ResOutput.txt");
		resOutput.deleteOnExit();
		return resOutput;
	}

	@Override
	public File getTemplateFile()
	{
		return new File("CHPuzzle4Template.txt");
	}

	@Override
	public ImageIcon getBackgroundPicture()
	{
		return new ImageIcon(getClass().getResource(
				"/storylines/catchinghope/img/CHPuzzle4Background.jpg"));
	}

	@Override
	public File getSolutionOutputFile()
	{
		File solnOutput = new File("CHPuzzle4SolnOutput.txt");
		solnOutput.deleteOnExit();
		return solnOutput;
	}
	
	@Override
	public String getStorylineName() {
		return "Catching Hope";
	}

	@Override
	public String getPreliminaryCode() {
		return "public static String decrypt (String s){"
				+ "\n	// CODE HERE"
				+ "\n"
				+ "\n}";
	}
}
