package storylines.catchinghope;

/**
 * CHStoryline -	This is the Storyline class for the storyline Catching Hope. It extends Storyline 
 * 					and a number of puzzles are added to it.
 * @author 			Anisa Llaveshi
 * @version 1.00 2014/5/3
 */

import javax.swing.ImageIcon;

import jpgame.storyline.Storyline;

public class CHStoryline extends Storyline {
	private static final long serialVersionUID = -3462966246195898656L;

	public CHStoryline() {
		// Puzzles are added to the storyline
		
		getPuzzles().add(new CHPuzzle1(new CHPuzzle1AdvSetup(), this));
		getPuzzles().add(new CHPuzzle2(new CHPuzzle2AdvSetup(),this));
		getPuzzles().add(new CHPuzzle3(new CHPuzzle3AdvSetup(),this));
		getPuzzles().add(new CHPuzzle4(new CHPuzzle4AdvSetup(),this));

		setName("Catching Hope");
		// Description of the storyline Catching Hope
		setDescription("Do you know your duty? You have to accomplish a very important mission."
				+ "\n You should steal the incredible 'Hope' diamond from the National Museum"
				+ "\n of Natural History in Washington D.C. To achieve this you have to complete"
				+ "\n several puzzles that take place in different place in the museum, in order to "
				+ "\n break into the diamond's room. Once all the puzzles in this storyline are completed"
				+ "\n there will be surprises for you.");

		setPicture(new ImageIcon(getClass().getResource(
				"/storylines/catchinghope/img/CHStoryline.jpg")));
		setDescriptionPicture(new ImageIcon(getClass().getResource(
				"/storylines/catchinghope/img/CHDescription.png")));
		
		setPartner(new CHPartner( this));

		// Conclusion statement
		setConclusion("You made it in the end. You managed to get the most incredible diamond in the world."
				+ "\nThis is the end of a very interesting journey and I hope you have enjoyed it."
				+ "\nWhat you do not know is that you have been a secret double agent. You did not steal"
				+ "\nbut helped the world be a better one by making us possible to catch the terrible "
				+ "\n Fibonacci - the most dangerous thief. Thank you and congratulations!");

	}

	@Override
	public void updatePartner() {
		getPartner().getPartnerView().setPuzzle( this.getCurrentPuzzle());
		getPartner().notifyViews();
	}
}
