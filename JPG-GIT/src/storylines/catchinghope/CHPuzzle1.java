/**
 * @author Ani Kristo
 * @version 1.0
 * 
 * 12 / 05 / 2014
 * 
 * DESCRIPTION: This is the first puzzle to appear in the Catching Hope storyline. It extends jpgame.Puzzle and determines the required
 * 				setup attributes for that particular puzzle. In addition to Puzzle, it overrides the abstract methods such as checkSolution()
 * 				and updateScore() by calculating the results accordingly. 
 * 
 * 				By the setup taken into its constructor, this puzzle will require the user to return the sum of all the integers
 * 				in a int[] array. 
 */

package storylines.catchinghope;

import java.awt.Point;
import java.util.Scanner;

import javax.swing.ImageIcon;

import jpgame.partner.hints.Hint;
import jpgame.puzzle.Puzzle;
import jpgame.puzzle.clues.Clue;
import jpgame.puzzle.setup.AdvancedPuzzleSetup;
import jpgame.puzzle.setup.BasicPuzzleSetup;
import jpgame.storyline.Storyline;

public class CHPuzzle1 extends Puzzle implements CHPuzzle {

	/**
	 * Generated serialVersionUID, which is necessary for serialization
	 */
	private static final long serialVersionUID = 2354023906175837980L;

	// PROPERTIES
	private BasicPuzzleSetup setup;
	static final int COMPILATION_BONUS = 20;
	static final int CORRECT_SOLN_BONUS = 200;

	// CONSTRUCTOR
	public CHPuzzle1(AdvancedPuzzleSetup setup, Storyline s) {
		super(setup, s);
		this.setup = setup;

		// Creating hints
		Hint hint1 = new Hint(this);
		hint1.setText("An array is a collection of elements of the same type.");
		hint1.setTime(10);
		getAllHints().add(hint1);

		Hint hint2 = new Hint(this);
		hint2.setText("To search through an array you can do it iteratively.");
		hint2.setTime(20);
		getAllHints().add(hint2);

		Hint hint3 = new Hint(this);
		hint3.setText("The 'for' loop is a way of iterating through arrays.");
		hint3.setTime(200);
		getAllHints().add(hint3);

		// Creating clues
		Clue clue1 = new Clue(this);
		clue1.setText("Congratulations. This is the first clue for Catching Hope. <br>"
				+ "Strings are a collection of characters. They all have a particular index. <br>"
				+ "Index starts from zero!");
		clue1.setImageIcon(new ImageIcon(getClass().getResource(
				"/storylines/catchinghope/img/CHPuzzle1Clue1.png")));
		clue1.setLocation(new Point(10, 210));
		clue1.setTime(20);
		getAllClues().add(clue1);
	}

	// METHODS
	@Override
	public boolean checkSolution() {
		boolean isCorrect = false;

		compile();
		runKeySoln();
		runInputFile();

		// Waits until the output files are fully written
//		try {
//			Thread.sleep(1000);
//		} catch (InterruptedException e1) {
//		}

		// Reads the output files
		try {

			Scanner readingResOutput = new Scanner(setup.getResultsOutputFile());

			Scanner readingSolnOutput = new Scanner(
					setup.getSolutionOutputFile());
			if (readingResOutput.hasNext())
				isCorrect = true;

			// Compares the outputs
			while (readingResOutput.hasNext() && readingSolnOutput.hasNext()) {
				if (readingResOutput.nextInt() != readingSolnOutput.nextInt()) {
					isCorrect = false;
					updateScore();
				}

			}

			// Updates the puzzle's state
			if (isCorrect) {
				setSolved(true);
				updateScore();
			} else

				readingResOutput.close();
			readingSolnOutput.close();

			return isCorrect;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	// METHODS
	@Override
	// Calculates the scores gained in this puzzle depending on the time spent,
	// difficulty coefficient, number of errors etc.
	public void updateScore() {
		setScore((int) (getScore() - Math.pow(getNumberOfCompilationErrors(), 0.5)));
		setScore((int) (getScore() - Math.pow(getNumberOfRuntimeErrors(), 0.5)));
		setScore((int) (getScore() + getTimeBonus()));
		if (isCompiled()) {
			setScore((int) (getScore() + COMPILATION_BONUS));
		}

		if (isSolved()) {
			setScore((int) (getScore() + CORRECT_SOLN_BONUS));
		}
	}

	// Determines the bonus given in accordance to the time spent for solving
	private int getTimeBonus() {
		if (getTime() <= 60)
			return 100;
		else if (getTime() <= 300)
			return 50;
		else
			return 0;
	}

	@Override
	public int getMaxSuggestedSolvingTime() {
		return 60;
	}

	@Override
	public int getMinSuggestedSolvingTime() {
		return 15;
	}

	@Override
	public int getMaxSuggestedNrOfComplErr() {
		return 7;
	}

	@Override
	public int getMinSuggestedNrOfComplErr() {
		return 0;
	}
}
