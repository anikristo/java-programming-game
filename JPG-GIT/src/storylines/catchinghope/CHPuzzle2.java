package storylines.catchinghope;

import java.awt.Point;
import java.util.Scanner;

import javax.swing.ImageIcon;

import jpgame.partner.hints.Hint;
import jpgame.puzzle.Puzzle;
import jpgame.puzzle.clues.Clue;
import jpgame.puzzle.setup.AdvancedPuzzleSetup;
import jpgame.puzzle.setup.BasicPuzzleSetup;
import jpgame.storyline.Storyline;

/**
 * CHPuzzle2 - This class is puzzle number 2 of Catching Hope storyline
 * 
 * @author Anisa Llaveshi
 * @version 1.00 2014/5/3
 */

public class CHPuzzle2 extends Puzzle implements CHPuzzle {

	private static final long serialVersionUID = 4848522955533700600L;
	// PROPERTIES
	private BasicPuzzleSetup setup;
	static final int COMPILATION_BONUS = 40;
	static final int CORRECT_SOLN_BONUS = 400;

	// CONSTRUCTOR
	public CHPuzzle2(AdvancedPuzzleSetup setup, Storyline s) {
		super(setup, s);
		this.setup = setup;

		// Hints are created and added
		Hint hint1 = new Hint(this);
		hint1.setText("Think about how do you get the elements of an array.");
		hint1.setTime(30);
		getAllHints().add(hint1);
		Hint hint2 = new Hint(this);
		hint2.setText("Think about how do you get the characters in a given index in a String");
		hint2.setTime(60);
		getAllHints().add(hint2);

		// Clues are created and added
		Clue clue1 = new Clue(this);
		clue1.setText("Have you ever needed, thus searched for an element in a set?");
		clue1.setImageIcon(new ImageIcon(getClass().getResource(
				"/storylines/catchinghope/img/CHPuzzle2Clue1.jpg")));
		clue1.setLocation(new Point(380, 60));
		clue1.setTime(20);
		getAllClues().add(clue1);
	}

	// METHODS

	// This method checks the solution of a the puzzle
	@Override
	public boolean checkSolution() {
		boolean isCorrect = false;

		compile();
		runKeySoln();
		runInputFile();

//		try {
//			Thread.sleep(1000);
//		} catch (InterruptedException e1) {
//		}

		try {
			Scanner readingResOutput = new Scanner(setup.getResultsOutputFile());

			Scanner readingSolnOutput = new Scanner(
					setup.getSolutionOutputFile());
			if (readingResOutput.hasNext())
				isCorrect = true;

			while (readingResOutput.hasNext() && readingSolnOutput.hasNext()) {
				if (!readingResOutput.next().equals(readingSolnOutput.next())) {
					isCorrect = false;
					updateScore();
				}

			}
			if (isCorrect) {
				setSolved(true);
				updateScore();
			}

			readingResOutput.close();
			readingSolnOutput.close();

			return isCorrect;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	// This method calculates and updates the score collected depending on the
	// number of errors, difficulty coefficient and time spent on it
	@Override
	public void updateScore() {
		setScore((int) (getScore() - Math.pow(getNumberOfCompilationErrors(), 0.5)));
		setScore((int) (getScore() - Math.pow(getNumberOfRuntimeErrors(), 0.5)));
		setScore((int) (getScore() + getTimeBonus()));
		if (isCompiled()) {
			setScore((int) (getScore() + COMPILATION_BONUS));
		}

		if (isSolved()) {
			setScore((int) (getScore() + CORRECT_SOLN_BONUS));
		}
	}

	// According to the time spent on solving a puzzle a bonus may be awarded
	private int getTimeBonus() {
		if (getTime() <= 60)
			return 150;
		else if (getTime() <= 300)
			return 70;
		else
			return 0;
	}

	@Override
	public int getMaxSuggestedSolvingTime() {
		return 90;
	}

	@Override
	public int getMinSuggestedSolvingTime() {
		return 30;
	}

	@Override
	public int getMaxSuggestedNrOfComplErr() {
		return 10;
	}

	@Override
	public int getMinSuggestedNrOfComplErr() {
		return 2;
	}

}
