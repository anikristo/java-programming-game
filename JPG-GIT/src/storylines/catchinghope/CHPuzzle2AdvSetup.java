package storylines.catchinghope;
import java.io.File;
import java.io.Serializable;

import javax.swing.ImageIcon;

import jpgame.puzzle.setup.AdvancedPuzzleSetup;

/**
 * CHPuzzle2AdvancedSetup	- This is the setup for the second puzzle in Storyline Catching Hope
 * @author Anisa Llaveshi
 * @version 1.00 2014/5/3
 */

public class CHPuzzle2AdvSetup implements AdvancedPuzzleSetup, Serializable{

	private static final long serialVersionUID = 4504509543940183724L;

	@Override
	public double getDifficultyCoefficient() {
		return 3.0;
	}

	@Override
	public String getPuzzleName() {
		return "Second Puzzle";
	}

	@Override
	public String getPuzzleQuestion() {
		return "<html><font face= \"Segoe UI\" >Here you are going on with the puzzles. <br>"
				+ " A new adventure is waiting for you, and a new mission to be accomplished."
				+ " In order to proceed you need to get a code which is a password to enter one of the guardians' computer."
				+ "	In order to do that you should get the first and the last chars of every String in an array of Strings "
				+ "and return it as a String. The method definition should look like that: <b>public static String passCode( String[] x)</b>"
				+ "<br> Good Luck!</html>";
	}

	@Override
	public File getErrorLogFile() {
		File errorLog = new File( "CHPuzzle2ErrLog.log");
		errorLog.deleteOnExit();
		return errorLog;
	}

	@Override
	public File getInputFile() {
		File input = new File( "CHPuzzle2Input.java");
		input.deleteOnExit();
		return input;
	}

	@Override
	public File getKeySolutionFile() {
		return new File( "CHPuzzle2KeySoln.class");
	}

	@Override
	public File getResultsOutputFile() {
		File resOutput = new File( "CHPuzzle2ResOutput.txt");
		resOutput.deleteOnExit();
		return resOutput;
	}

	@Override
	public File getTemplateFile() {
		return new File( "CHPuzzle2Template.txt");
	}

	@Override
	public ImageIcon getBackgroundPicture() {
		return new ImageIcon( getClass().getResource( "/storylines/catchinghope/img/CHPuzzle2Background.jpg"));
	}

	@Override
	public File getSolutionOutputFile() {
		File solnOutput = new File( "CHPuzzle2SolnOutput.txt");
		solnOutput.deleteOnExit();
		return solnOutput;
	}
	
	@Override
	public String getStorylineName() {
		return "Catching Hope";
	}

	@Override
	public String getPreliminaryCode() {
		return "public static String passCode( String[] x){"
				+ "\n	// CODE HERE"
				+ "\n"
				+ "\n}";
	}
}
