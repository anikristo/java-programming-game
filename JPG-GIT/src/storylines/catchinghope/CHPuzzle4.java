package storylines.catchinghope;

/**
 * CHPuzzle4	Puzzle#4 in Storyline Catching Hope
 *				Works almost the same as other puzzles.
 * @author Burcu Canakci
 * @version 1.00 2014/5/5
 */

import java.awt.Point;
import java.util.Scanner;

import javax.swing.ImageIcon;

import jpgame.partner.hints.Hint;
import jpgame.puzzle.Puzzle;
import jpgame.puzzle.clues.Clue;
import jpgame.puzzle.setup.AdvancedPuzzleSetup;
import jpgame.puzzle.setup.BasicPuzzleSetup;
import jpgame.storyline.Storyline;

public class CHPuzzle4 extends Puzzle implements CHPuzzle
{

	private static final long serialVersionUID = 8341183859906269265L;
	// PROPERTIES
	private BasicPuzzleSetup setup;
	static final int COMPILATION_BONUS = 100;
	static final int CORRECT_SOLN_BONUS = 500;

	// CONSTRUCTOR
	public CHPuzzle4(AdvancedPuzzleSetup setup, Storyline s)
	{
		super(setup,s);
		this.setup = setup;

		setHints();
		setClues();
	}

	// METHODS
	@Override
	public boolean checkSolution()
	{
		boolean isCorrect = false;

		compile();

		runInputFile();

		runKeySoln();

//		try
//		{
//			Thread.sleep(1000);
//		} catch (InterruptedException e1)
//		{
//		}

		try
		{
			Scanner readingResOutput = new Scanner(setup.getResultsOutputFile());

			Scanner readingSolnOutput = new Scanner(
					setup.getSolutionOutputFile());

			if (readingResOutput.hasNext())
				isCorrect = true;

			while (readingResOutput.hasNext() && readingSolnOutput.hasNext())
			{
				if (!readingResOutput.next().equals(readingSolnOutput.next()))
				{
					isCorrect = false;
					updateScore();

				}

			}
			if (isCorrect)
			{
				setSolved(true);
				updateScore();
			}

			readingResOutput.close();
			readingSolnOutput.close();

			return isCorrect;

		} catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public void updateScore()
	{
		setScore((int) (getScore() - Math.pow(getNumberOfCompilationErrors(), 0.5)));
		setScore((int) (getScore() - Math.pow(getNumberOfRuntimeErrors(), 0.5)));
		setScore((int) (getScore() + getTimeBonus()));
		if (isCompiled())
		{
			setScore((int) (getScore() + COMPILATION_BONUS));
		}

		if (isSolved())
		{
			setScore((int) (getScore() + CORRECT_SOLN_BONUS));
		}
	}

	private int getTimeBonus()
	{
		if (getTime() <= 60)
			return 1000;
		else if (getTime() <= 500)
			return 500;
		else
			return 0;
	}

	public void setHints()
	{
		Hint hint1 = new Hint(this);
		hint1.setText("Consider loops. You should not require hints my friend, this is almost elementary");
		hint1.setTime(30);
		Hint hint2 = new Hint(this);
		hint2.setText("Consider Substrings.");
		hint2.setTime(60);
		getAllHints().add(hint1);
	}

	public void setClues()
	{
		Clue clue1 = new Clue(this);
		clue1.setText("You have come to the last puzzle. <br>"
				+ "Beware of your actions, your partner is getting suspicious.");
		clue1.setImageIcon(new ImageIcon(getClass().getResource(
				"/storylines/catchinghope/img/CHPuzzle4CluePic.jpg")));
		clue1.setLocation(new Point(250, 250));
		clue1.setTime(50);
		getAllClues().add(clue1);
	}

	@Override
	public int getMaxSuggestedSolvingTime() {
		return 200;
	}

	@Override
	public int getMinSuggestedSolvingTime() {
		return 60;
	}

	@Override
	public int getMaxSuggestedNrOfComplErr() {
		return 15;
	}

	@Override
	public int getMinSuggestedNrOfComplErr() {
		return 7;
	}

}
