/**
 * @author Ani Kristo
 * @version 1.0
 * 06 - 08 - 2014
 * 
 * This interface is to be implemented from every puzzle in CHStoryline.
 * It will grant information needed for customization / enhancments of the CHStoryline
 * 
 */
package storylines.catchinghope;

interface CHPuzzle {
	public int getMaxSuggestedSolvingTime(); // seconds

	public int getMinSuggestedSolvingTime(); // seconds

	public int getMaxSuggestedNrOfComplErr();

	public int getMinSuggestedNrOfComplErr();
}
