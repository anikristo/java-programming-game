package storylines.catchinghope;
import javax.swing.ImageIcon;

import jpgame.partner.Mood;
import jpgame.partner.Moody;
import jpgame.partner.Partner;
import jpgame.puzzle.Puzzle;
import jpgame.storyline.Storyline;

/**
* CHPartner.java
*
* CHPartner application at JavaProgrammingGame
*
* Creates the partner of the storyline Catching Hope with changing moods
*
* Author: Ayse Berceste Dincer
* version 1.00 2014/30/4 
**/

class CHPartner extends Partner implements Moody<ImageIcon>
{	
	//CONSTANTS
	private static final long serialVersionUID = 7873790624245369939L;
	
	//PROPERTIES
	private Puzzle puzzle;
	private Mood mood;
	
	//Expression of the partner according to moods
	private ImageIcon sadMoodExpression;
	private ImageIcon angryMoodExpression;
	private ImageIcon happyMoodExpression;
	private ImageIcon marvelledMoodExpression;
	private ImageIcon dubiousMoodExpression;
	private ImageIcon plainMoodExpression;
	private ImageIcon disappointedMoodExpression;
	
	//Constructor which takes a Puzzle as a parameter
	public CHPartner( Storyline storyline)
	{
		super( "Fibonacci", storyline);

		setPicture(  new ImageIcon( getClass().getResource( "/storylines/catchinghope/img/CHPartner.png")));
		
		this.puzzle = storyline.getCurrentPuzzle();
		
		this.mood = Mood.PLAIN;
		
		sadMoodExpression = new ImageIcon( getClass().getResource("/storylines/catchinghope/img/CHPartner/CHPartnerSad.jpg"));
		angryMoodExpression = new ImageIcon( getClass().getResource("/storylines/catchinghope/img/CHPartner/CHPartnerAngry.jpg"));
		happyMoodExpression  = new ImageIcon( getClass().getResource("/storylines/catchinghope/img/CHPartner/CHPartnerHappy.jpg"));
		marvelledMoodExpression  = new ImageIcon( getClass().getResource( "/storylines/catchinghope/img/CHPartner/CHPartnerMarvelled.jpg"));
		dubiousMoodExpression  = new ImageIcon( getClass().getResource( "/storylines/catchinghope/img/CHPartner/CHPartnerDubious.jpg"));
		plainMoodExpression  = new ImageIcon( getClass().getResource( "/storylines/catchinghope/img/CHPartner/CHPartnerPlain.jpg"));
		disappointedMoodExpression = new ImageIcon( getClass().getResource( "/storylines/catchinghope/img/CHPartner/CHPartnerDisappointed.jpg"));
	}

	//Returns the mood of the partner according to the time spend on the puzzle and the number of errors made
	@Override
	public Mood getMood() {
		updateMood();
		return mood;
	}

	//Returns the mood message according to the mood of the partner
	@Override
	public String getMoodMessage(Mood mood) {
		if( mood.equals( Mood.ANGRY))
			return "I am really angry right now! \n"
					+ "Do you ever think you can finish this puzzle? like... ever?";
		if( mood.equals( Mood.DISAPPOINTED))
			return "You don't seem to be able to solve anything! \n"
					+ "Do you think this is a difficult puzzle???";
		if( mood.equals( Mood.DUBIOUS))
			return "I am not sure if you are really putting enought effort on this!"
					+ "Are you sure you are following my instructions?";
		if( mood.equals( Mood.HAPPY))
			return "Yeah! You are doing fine my dear! I am so happy for you!";
		if( mood.equals( Mood.MARVELLED))
			return "Impressive! Way to go buddy! Keep it up!";
		if( mood.equals( Mood.SAD))
			return "This isn't going fine! At all! \n"
					+ "I don't know if you are the right person for this business... ";
		return "Make sure you follow my instructions correctly and be quick! I want results!"; // Plain
	}

	//Set method for mutating the mood of the partner
	@Override
	public void setMood(Mood mood) {
		this.mood = mood;		
	}

	//Returns the mood expression of the partner
	@Override
	public ImageIcon getMoodExpression(Mood mood) {
		
		if (mood != null)
		{			
			if( mood.equals( Mood.ANGRY))
				return angryMoodExpression;
			if( mood.equals( Mood.SAD))
				return sadMoodExpression;
			if( mood.equals( Mood.DUBIOUS))
				return dubiousMoodExpression;
			if( mood.equals( Mood.HAPPY))
				return happyMoodExpression;
			if( mood.equals( Mood.MARVELLED))
				return marvelledMoodExpression;
			if( mood.equals( Mood.DISAPPOINTED ))
				return disappointedMoodExpression;
			else
				return plainMoodExpression;
		}
		return null;
	}

	@Override
	public ImageIcon getCurrentMoodExpression() {
		updateMood();
		return getMoodExpression( this.mood);
	}

	@Override
	public void updateMood() {
		puzzle = getStoryline().getCurrentPuzzle();
		
		if (puzzle != null && puzzle instanceof CHPuzzle)
		{
			// Compiled, very little time, very few errors
			if( puzzle.isCompiled() && puzzle.getTime() <= ((CHPuzzle) puzzle).getMinSuggestedSolvingTime() && puzzle.getNumberOfCompilationErrors() <= ((CHPuzzle) puzzle).getMinSuggestedNrOfComplErr())
				this.mood = Mood.MARVELLED;
			
			// Not compiled, very little time, very few errors
			else if ( !puzzle.isCompiled() && puzzle.getTime() <= ((CHPuzzle) puzzle).getMinSuggestedSolvingTime() && puzzle.getNumberOfCompilationErrors() <= ((CHPuzzle) puzzle).getMinSuggestedNrOfComplErr())
				this.mood = Mood.HAPPY; 
			
			// Not compiled, a lot of time, very few errors
			else if( !puzzle.isCompiled() && puzzle.getTime() >= ((CHPuzzle) puzzle).getMaxSuggestedSolvingTime() && puzzle.getNumberOfCompilationErrors() <= ((CHPuzzle) puzzle).getMinSuggestedNrOfComplErr())
				this.mood = Mood.DUBIOUS;
			
			// Not compiled, normal amount of time, a lot of errors
			else if( !puzzle.isCompiled() && puzzle.getTime() >= ((CHPuzzle) puzzle).getMinSuggestedSolvingTime() && puzzle.getTime() <= ((CHPuzzle) puzzle).getMaxSuggestedSolvingTime() && puzzle.getNumberOfCompilationErrors() >= ((CHPuzzle) puzzle).getMaxSuggestedNrOfComplErr())
				this.mood = Mood.DISAPPOINTED;
			
			// Not compiled, a lot of time, normal amount of errors
			else if( !puzzle.isCompiled() && puzzle.getTime() >= ((CHPuzzle) puzzle).getMaxSuggestedSolvingTime() && puzzle.getNumberOfCompilationErrors() >= ((CHPuzzle) puzzle).getMinSuggestedNrOfComplErr() && puzzle.getNumberOfCompilationErrors() <= ((CHPuzzle) puzzle).getMaxSuggestedNrOfComplErr())
				this.mood = Mood.SAD;
			
			// Not compiled, a lot of time, a lot of errors
			else if( !puzzle.isCompiled() && puzzle.getTime() >= ((CHPuzzle) puzzle).getMaxSuggestedSolvingTime() && puzzle.getNumberOfCompilationErrors() >= ((CHPuzzle) puzzle).getMaxSuggestedNrOfComplErr())
				this.mood = Mood.ANGRY;
			
			// Not compiled, normal amount of time, normal amount of errors
			else
				this.mood = Mood.PLAIN;
		}
		else
			this.mood = Mood.PLAIN;
	}	
} //End of the CHPartner class

