package storylines.catchinghope;
import java.awt.Point;
import java.util.Scanner;

import javax.swing.ImageIcon;

import jpgame.partner.hints.Hint;
import jpgame.puzzle.Puzzle;
import jpgame.puzzle.clues.Clue;
import jpgame.puzzle.setup.AdvancedPuzzleSetup;
import jpgame.puzzle.setup.BasicPuzzleSetup;
import jpgame.storyline.Storyline;

/**
* CHPuzzle3.java
*
* CHPuzzle3 application at JavaProgrammingGame
*
* Creates the third puzzle of the catching hope storyline
*
* Author: Ayse Berceste Dincer
version 1.00 2014/5/5
**/

public class CHPuzzle3 extends Puzzle implements CHPuzzle {

	private static final long serialVersionUID = -1583100867692190924L;
	
	//CONSTANTS	
	static final int COMPILATION_BONUS = 50;
	static final int CORRECT_SOLN_BONUS = 500;
	
	// PROPERTIES
	private BasicPuzzleSetup setup;
	
	// CONSTRUCTOR
	public CHPuzzle3(AdvancedPuzzleSetup setup, Storyline s) 
	{
		super(setup,s);
		this.setup = setup;
		//Creates the hints of the particular puzzle
		Hint hint1 = new Hint( this);
		Hint hint2 = new Hint( this);
		Hint hint3 = new Hint( this);
		Hint hint4 = new Hint( this);
		Hint hint5 = new Hint( this);
		//Creates the texts of the hints
		hint1.setText( "How do you search for something in real life?");
		hint2.setText( "When you loose something you look at every possible place right?");
		hint3.setText( "In order to find the key you need to search all elements of the array");
		hint3.setText( "What about using a loop?");
		hint3.setText( "Check all elements in the array in the loop whether they have the key or not...");
		//Sets the revealing time for hints						
		hint1.setTime( 30);
		hint2.setTime( 60);
		hint3.setTime( 90);
		hint4.setTime( 120);
		hint5.setTime( 150);
		//Adds the hints to the journal
		getAllHints().add( hint1);
		getAllHints().add( hint2);
		getAllHints().add( hint3);
		getAllHints().add( hint4);
		getAllHints().add( hint5);
		//Creates the clue of the puzzle
    	Clue clue1 = new Clue( this);
		clue1.setImageIcon( new ImageIcon( getClass().getResource( "/storylines/catchinghope/img/CHPuzzle3Clue1.png")));
		clue1.setLocation( new Point( 500, 300));
		clue1.setText( "When your code is a total mess"
			         + "<br> What can be the error, guess"
			         + "<br> Sometimes when you are on the way of success"	
			         + "<br> Clones block your access!");
		clue1.setTime( 30);
		getAllClues().add( clue1);
	}

	//Method for checking the solution of the puzzle
	@Override
	public boolean checkSolution() {
		boolean isCorrect = false;

		compile();
		runKeySoln();
		runInputFile();

//		try {
//			Thread.sleep( 1000);
//		} catch (InterruptedException e1) {	}
		
		try {
			Scanner readingResOutput = new Scanner(setup.getResultsOutputFile());

			Scanner readingSolnOutput = new Scanner(
					setup.getSolutionOutputFile());
			 if(readingResOutput.hasNext())
				 isCorrect= true;

			while (readingResOutput.hasNext() && readingSolnOutput.hasNext()) {
				if (readingResOutput.nextInt() != readingSolnOutput.nextInt()) {
					isCorrect = false;
					updateScore();
				}

			}
			if(isCorrect){
				setSolved( true);
				updateScore();
			}

			readingResOutput.close();
			readingSolnOutput.close();
			

			return isCorrect;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	//Method for updating the score according to the number of errors
	@Override
	public void updateScore() {
		setScore((int) (getScore() - Math.pow(getNumberOfCompilationErrors(), 0.5)));
		setScore((int) (getScore() - Math.pow(getNumberOfRuntimeErrors(), 0.5)));
		setScore((int) (getScore() + getTimeBonus()));
		if (isCompiled()) {
			setScore((int) (getScore() + COMPILATION_BONUS));
		}

		if (isSolved()) {
			setScore((int) (getScore() + CORRECT_SOLN_BONUS));
		}
	}
	
	//Method for returning the bonus point according to the time spend on each puzzle
	private int getTimeBonus() {
		if (getTime() <= 120)
			return 150;
		else if (getTime() <= 300)
			return 50;
		else
			return 0;
	}

	@Override
	public int getMaxSuggestedSolvingTime() {
		return 120;
	}

	@Override
	public int getMinSuggestedSolvingTime() {
		return 40;
	}

	@Override
	public int getMaxSuggestedNrOfComplErr() {
		return 15;
	}

	@Override
	public int getMinSuggestedNrOfComplErr() {
		return 5;
	}

} //End of the CHPuzzle3 class
