package storylines.watsonsjournal;

/**
 * WJPuzzle1AdvSetup  Setup for Puzzle#1 in  Watson's Journal storyline.
 *						
 * @author Burcu Canakci
 * @version 1.00 2014/5/9
 */

import java.io.File;
import java.io.Serializable;

import javax.swing.ImageIcon;

import jpgame.puzzle.setup.AdvancedPuzzleSetup;

public class WJPuzzle1AdvSetup implements AdvancedPuzzleSetup, Serializable
{

	private static final long serialVersionUID = 6566849725406458137L;

	@Override
	public double getDifficultyCoefficient()
	{
		return 1.0;
	}

	@Override
	public String getPuzzleName()
	{
		return "Sorting and Searching Lists";
	}

	@Override
	public String getPuzzleQuestion()
	{
		return "Hello, it is good to have you here. Let's start, shall we?"
				+ "\nI am given three unsorted lists by the police force. The first one is the list of regions of our victims. "
				+ "\nThe second one is the list of ages. The third one is the list of genders"
				+ "\nYou are to write a class of the object unsortedList(String[] regions, int[] ages, char[] genders)"
				+ "\nAlso I require 3 methods. String[] getAlphabeticallySortedRegions() to return the regions listed sorted alphabetically."
				+ "\nint getNumberOfAdults() to return the number of people over the age of 18."
				+ "\nand int getNumberOfFemales() to return the number of females in the list." 
				+ "\nGood Luck!";
	}

	@Override
	public File getErrorLogFile()
	{
		File errorLog = new File("WJPuzzle1ErrLog.log");
		errorLog.deleteOnExit();
		return errorLog;
	}

	@Override
	public File getInputFile()
	{
		File input = new File("WJPuzzle1Input.java");
		input.deleteOnExit();
		return input;
	}

	@Override
	public File getKeySolutionFile()
	{
		return new File("WJPuzzle1KeySoln.class");
	}

	@Override
	public File getResultsOutputFile()
	{
		File resOutput = new File("WJPuzzle1ResOutput.txt");
		resOutput.deleteOnExit();
		return resOutput;
	}

	@Override
	public File getTemplateFile()
	{
		return new File("WJPuzzle1Template.txt");
	}

	@Override
	public ImageIcon getBackgroundPicture()
	{
		return new ImageIcon(getClass().getResource(
				"/storylines/watsonsjournal/img/WJPuzzle1Background.jpg"));
	}

	@Override
	public File getSolutionOutputFile()
	{
		File solnOutput = new File("WJPuzzle1SolnOutput.txt");
		solnOutput.deleteOnExit();
		return solnOutput;
	}
	
	@Override
	public String getStorylineName() {
		return "Watson's Journal";
	}

	@Override
	public String getPreliminaryCode() {
		return "static class UnsortedList{\n\n\n\tpublic UnsortedList(String[] regions, int[] ages, char[] genders){} "
				+ "\n\n\n\tpublic String[] getAlphabeticallySortedRegions(){}"
				+ "\n\tpublic int getNumberOfAdults(){}"
				+ "\n\tint getNumberOfFemales(){}\n}";
	}
}
