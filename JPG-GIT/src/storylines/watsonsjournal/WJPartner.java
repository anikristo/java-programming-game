package storylines.watsonsjournal;

/**
 * WJPartner  The partner for Watson's Journal storyline.
 *						
 * @author Burcu Canakci
 * @version 1.00 2014/5/9
 */

import javax.swing.ImageIcon;

import jpgame.partner.Partner;
import jpgame.storyline.Storyline;

class WJPartner extends Partner
{
	private static final long serialVersionUID = -968283066121266690L;

	public WJPartner( Storyline s)
	{
		super("L", s);
		setPicture( new ImageIcon( getClass().getResource( "/storylines/watsonsjournal/img/WJPartner.jpg")));
	}
}
