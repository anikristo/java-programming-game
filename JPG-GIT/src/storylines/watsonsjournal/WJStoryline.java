package storylines.watsonsjournal;

/**
 * WJStoryline  Watson's Journal storyline.
 *						
 * @author Burcu Canakci
 * @version 1.00 2014/5/9
 */

import javax.swing.ImageIcon;

import jpgame.storyline.Storyline;

public class WJStoryline extends Storyline
{
	private static final long serialVersionUID = 8500960084757338601L;

	public WJStoryline()
	{
		getPuzzles().add(new WJPuzzle1(new WJPuzzle1AdvSetup(), this));

		
		setName("Watson's Journal");
		setDescription("A storyline about the greatest detective of your generation. A universally acknowledged genius, feared and respected by all."
				+ "\nHe solves the hardest of crimes using his incredible deduction and observation skills."
				+ "\nYou? Well, you are just like him, except not quite."
				+ "\nYou are his sidekick, his programming buddy, his key to access almost anywhere. "
				+ "\nCome and help him overcome the challenges and who knows?"
				+ "\nChances are you can solve crimes besides him, before him?");
		
		setPartner(new WJPartner( this));
		
		setPicture(new ImageIcon(getClass().getResource(
				"/storylines/watsonsjournal/img/WJStoryline.jpeg")));
	}

	@Override
	public void updatePartner() {	
	}
}