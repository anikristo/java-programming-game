package storylines.watsonsjournal;

/**
 * WJPuzzle1  Puzzle#1 for Watson's Journal storyline.
 *						
 * @author Burcu Canakci
 * @version 1.00 2014/5/9
 */

import java.util.Scanner;

import jpgame.partner.hints.Hint;
import jpgame.puzzle.Puzzle;
import jpgame.puzzle.setup.AdvancedPuzzleSetup;
import jpgame.puzzle.setup.BasicPuzzleSetup;
import jpgame.storyline.Storyline;

public class WJPuzzle1 extends Puzzle
{

	private static final long serialVersionUID = -798824011767408491L;
	// PROPERTIES
	private BasicPuzzleSetup setup;
	static final int COMPILATION_BONUS = 100;
	static final int CORRECT_SOLN_BONUS = 2000;

	// CONSTRUCTOR
	public WJPuzzle1(AdvancedPuzzleSetup setup, Storyline s)
	{
		super(setup,s);
		this.setup = setup;
		setHints();
	}

	// HINT AND CLUE METHODS
	private void setHints()
	{
		Hint hint = new Hint(this);
		hint.setText("OK, I called Watari, my partner and he checked some site..."
				+ "Anyway, he says you should consider iterating Strings.");
		hint.setTime(30);
		Hint hint2 = new Hint(this);
		hint2.setText("By the way, you were not offended when I regarded Watari as my partner, right? You are my partner too.");
		hint2.setTime(33);
		getAllHints().add(hint);
		getAllHints().add(hint2);
	}

	// METHODS
	@Override
	public boolean checkSolution()
	{
		boolean isCorrect = false;

		compile();
		runKeySoln();
		runInputFile();

		try
		{
			Thread.sleep(1000);
		} catch (InterruptedException e1)
		{
		}

		try
		{
			Scanner readingResOutput = new Scanner(setup.getResultsOutputFile());

			Scanner readingSolnOutput = new Scanner(
					setup.getSolutionOutputFile());

			if (readingResOutput.hasNext() && readingSolnOutput.hasNext())
				isCorrect = true;

			while (readingResOutput.hasNext() && readingSolnOutput.hasNext())
			{
				if (readingResOutput.next().charAt(0) != readingSolnOutput
						.next().charAt(0))
				{
					isCorrect = false;
					updateScore();
				}

			}
			if (isCorrect)
			{
				setSolved(true);
				updateScore();
			}

			readingResOutput.close();
			readingSolnOutput.close();

			return isCorrect;

		} catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public void updateScore()
	{
		setScore((int) (getScore() - Math.pow(getNumberOfCompilationErrors(), 0.5)));
		setScore((int) (getScore() - Math.pow(getNumberOfRuntimeErrors(), 0.5)));
		setScore((int) (getScore() + getTimeBonus()));
		if (isCompiled())
		{
			setScore((int) (getScore() + COMPILATION_BONUS));
		}

		if (isSolved())
		{
			setScore((int) (getScore() + CORRECT_SOLN_BONUS));
		}
	}

	private int getTimeBonus()
	{
		if (getTime() <= 60)
			return 100;
		else if (getTime() <= 300)
			return 50;
		else
			return 0;
	}

}
